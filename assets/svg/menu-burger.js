import React from "react"

export default function menuBurger(hex:string, type:string) {
  const isMobile = type === 'mobile' ? 35 : 43
  return (
    <svg width={isMobile} height="20" viewBox="0 0 43 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 2H42" stroke={hex} strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 10H43" stroke={hex} strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 18H43" stroke={hex} strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
    </svg>
  )
}
