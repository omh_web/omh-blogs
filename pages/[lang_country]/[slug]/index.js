import React from 'react'
import getConfig from 'next/config'
import fetch from 'isomorphic-unfetch'
import Head from 'next/head'
import useMediaQuery from 'react-responsive'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'

const DynamicComponent1 = dynamic(() => import('components/socials'))
const DynamicComponent2 = dynamic(() => import('components/headerBar'))
const DynamicComponent12 = dynamic(() => import('components/error'))
const DynamicComponent13 = dynamic(() => import('components/footer'))

const DynamicComponent3 = dynamic(() => import('modules/post/featuredImage'))
const DynamicComponent4 = dynamic(() => import('modules/post/header'))
const DynamicComponent5 = dynamic(() => import('modules/post/content'), { loading: () => <p>Loading caused by client page transition ...</p> })
const DynamicComponent6 = dynamic(() => import('modules/post/previousPost'))
const DynamicComponent7 = dynamic(() => import('modules/post/nextPost'))
const DynamicComponent8 = dynamic(() => import('modules/post/subscribe'))
const DynamicComponent9 = dynamic(() => import('modules/post/marketing'))
const DynamicComponent10 = dynamic(() => import('modules/post/relatedBlogs'))
const DynamicComponent11 = dynamic(() => import('modules/post/latestBlogs'))

const LargeDesktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 1025 })
  return isDesktop ? children : null
}
const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1024 })
  return isTablet ? children : null
}
const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ maxWidth: 768 })
  return isDesktop ? children : null
}
const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 767 })
  return isMobile ? children : null
}

export const config = { amp : true }
const COUNTRY = {
  sg: 'SGP',
  my: 'MYS',
  ph: 'PHL'
}


function post ({ post, latest, country}) {
  // console.log(post)
  // console.log(country, '.toString().split(/-/)[1] ')
  
  const CountryData = COUNTRY[country] === undefined ? 'SGP' : COUNTRY[country]

  const rootUrl =
  process.env.NODE_ENV === "development"
    ? 'http://localhost:3000'
    : ''

  return (
    <>
      {
        post.error ? 
          <>
          
            <React.Fragment>
              <Head>
                <title>{'Ohmyhome Blog'}</title>
                <meta property="og:type" content="website"/>
                <link rel="shortcut icon" href="https://api.omh.app/store/cms/media/amp/favicon.png" />
              </Head>
              <DynamicComponent12 
                latest={latest} 
                rootUrl={rootUrl}
              /> 
              <DynamicComponent13 country={CountryData}/>
            </React.Fragment>
          </>
          :
          <>
            <Head>
              <title>{post.seo?.title || ''}</title>
              <meta
                name="description"
                content={post.seo?.description || ''}
              />
              <meta charSet="UTF-8" />
              <meta name="viewport" content="width=device-width, initial-scale=1" />
              <meta property="og:url" content={post.metaTag?.canonicalUrl.replace('https://omh.sg','') || post.metaTag?.canonicalUrl.replace('https://ohmyhome.com.my/','')} />
              <meta property="og:title" content={post.seo?.title || ''} />
              <meta property="og:description" content={post.seo?.description || ''} />
              <meta name="keywords" content={post.seo?.keywords || ''} />
              <meta name="twitter:site" content={post.metaTag?.canonicalUrl.replace('https://omh.sg','') || post.metaTag?.canonicalUrl.replace('https://ohmyhome.com.my/','')} />
              <meta name="twitter:card" content="summary_large_image" />
              <meta name="twitter:image" content={post.featuredImages && post.featuredImages[0]?.medium || ''} />
              <meta property="og:image" content={post.featuredImages && post.featuredImages[0]?.medium || ''} />
              <meta property="og:image:width" content="1200" />
              <meta property="og:image:height" content="630" />
              <meta property="og:type" content="website"/>
            </Head>
            <DynamicComponent2 
              post={post}
              rootUrl={rootUrl}
            />
            <div id="post-wrapper">
              <div className="post-container">
                <div className="post-content_wrapper">
                  
                  <div className="content">

                    <DynamicComponent3 post={post} />

                    <div className={`floating ${post && post.marketing &&  post.marketing.isActive === true ? '' : 'social-only'}`}>
                        <DynamicComponent1 post={post}/>
                    </div>
                    
                    <div className={`content-container ${post && post.marketing &&  post.marketing.isActive === true ? '' : 'social-only'}`}>
                      

                      <DynamicComponent4 post={post} />

                      <DynamicComponent5 post={post} />

                    </div>

                    <div className="blogs-prev_next__wrapper">
                      <div className = "previos_wrapper">
                        <DynamicComponent6 
                          post={post} 
                          Desktop={Desktop} 
                          Mobile={Mobile}
                        />
                      </div>
                      <div className="next_wrapper">
                        { post.nextPost === null ? '' :
                          <DynamicComponent7 
                            post={post} 
                            Desktop={Desktop} 
                            Mobile={Mobile}
                          />
                        }
                      </div>
                    </div>

                    <div className="cta_wrapper">
                      { post && post.marketing && post.marketing.isActive === true ?
                        <React.Fragment> 
                          <div className="subscribe-wrapper">
                            <DynamicComponent8 post={post}/>
                          </div>
                          <div className="marketing-wrapper">
                            <DynamicComponent9
                              post={post} 
                            />
                          </div>
                        </React.Fragment> : 
                        <div className="subscribe-wrapper full">
                          <DynamicComponent8 post={post}/>
                        </div>
                      }
                      {/* { post.marketing.isActive === true ?
                        <div className="marketing-wrapper full">
                          <Marketing post={post} />
                        </div> : ''
                      } */}
                    </div>
                  </div>
                </div>
                
                <div className="blog-related-wrapper">
                  { post.relatedBlogs === undefined ? '' :
                    <DynamicComponent10
                      rootUrl={rootUrl}
                      post={post} 
                      title={'Related Blogs'}
                      description={'Resources to help you stay on top of your mortgage game'}
                    />
                  }
                </div>
                <div className="blog-latest-wrapper">
                  <DynamicComponent11 
                    rootUrl={rootUrl}
                    latest={latest} 
                    title={'Latest Blogs'}
                    description={'Read up about all things mortgage and financial planning'}
                  />
                </div>
                
              </div>

              <style jsx>{`
              .content {
                max-width: 100%;
                width: 100%;
              }
              .floating {
                display: none;
              }
              .blogs-prev_next__wrapper {
                margin: 80px 0 80px;
                display: flex;
                justify-content: space-between;
                align-items: flex-start;
                padding: 0 16px;
              }
              .previos_wrapper {
                text-align: left;
                padding-right: 10px;
                position: relative;
                box-sizing: border-box;
                width: 50%;
              }
              .next_wrapper {
                text-align: right;
                padding-left: 10px;
                position: relative;
                box-sizing: border-box;
                width: 50%;
              }
              .cta_wrapper {
                display: flex;
                justify-content: space-between;
                align-items: flex-start;
                padding: 0 16px;
                margin: 80px 0 0px;
                flex-wrap: wrap
              }
              .subscribe-wrapper {
                text-align: left;
                box-sizing: border-box;
                width: 100%;
                margin: 0 0 24px;
                z-index: 3;
              }
              .subscribe-wrapper.full {
                width: 100%;
              }
              .marketing-wrapper.full {
                width: 100%;
              }
              .marketing-wrapper {
                text-align: left;
                position: relative;
                box-sizing: border-box;
                width: 100%;
              }
              .blog-related-wrapper {
                margin: 80px 0 0;
              }
              .mobile-side-nav {
                width: 316px;
                left: -100%;
                transition: all 0.3s cubic-bezier(0.25, 0.1, 0.25, 1);
              }
              .mobile-side-nav.active {
                left: 0;
              }
              @media only screen and (min-width: 768px) {
                .post-container {
                  max-width: 1224px;
                  margin: 0 auto;
                }
                .post-content_wrapper {
                  display: flex;
                  justify-content: center;
                  align-tems:top;
                  min-height: 2500px;
                  position: relative;
                }
                .floating {
                  position: sticky;
                  top: 0;
                  left: 0px;
                  display: block;
                  transform: translate(-25%, 94px);
                  z-index: 2;
                  max-width: 180px;
                  min-height: 400px;
                }
                .content {
                  max-width: 600px;
                  width: 100%;
                }
                .blogs-prev_next__wrapper {
                  padding: 0;
                }
                .cta_wrapper {
                  padding: 0;
                  flex-wrap: nowrap
                }
                .subscribe-wrapper {
                  padding-right: 10px;
                  width: 50%;
                }
                .subscribe-wrapper.full {
                  
                }
                .marketing-wrapper {
                  width: 50%;
                }
                .content-container {
                  margin: -382px 0 0;

                  position: relative;
                }
                .floating.social-only {
                  margin-left: 0;
                }
                div#post-wrapper {
                  padding: 148px 0 0 ;
                }
      
              }
              @media only screen and (min-width: 1025px) { 
                
                .content-container {
                  margin: 120px 0 0;
          
                }
      
                .floating {
                  margin-left: -26%;
                  margin-top: -80%;
                }
                .filler {
                  height: 265px;
                }
              }
              @media only screen and (min-width: 1250px) { 
                .floating {
                  margin-left: -30%;
                  margin-top: -420px;
                  max-width: 300px;
                }
              }
              @media screen and (min-width: 1441px) {
                .content {
                  max-width: 700px;
                }
                .post-container {
                  max-width: 1760px;
                  width: 80%;
                }
              }

              `}</style>    
            </div>
            <DynamicComponent13 country={CountryData}/>
          </>
      }

    </>
    
  )
}

export async function getServerSideProps(context) {
    const { slug } = context.query
    const { omhweb_auth }  = process.env
    const { publicRuntimeConfig } = getConfig()
    // const country  = context.query.lang_country.toString().split(/-/)[1] 
    const country  = context.query && context.query.lang_country && context.query.lang_country.toString().split(/-/)[1] === undefined ? 'sg' : context.query.lang_country.toString().split(/-/)[1]
    const CountryData = COUNTRY[country] === undefined ? 'SGP' : COUNTRY[country]

    const postData = await fetch(`https://cms.staging.ohmyhome.io/api/blog/info/formatted?showNextPrev=1&showRelatedPosts=1&format=amp&slug=${slug}`, 
      {
        params: { slug: slug },
        headers: { 
          'OmhWeb-Auth': `${omhweb_auth}`,
          'Accept-Country': `${COUNTRY[country]}`
        }
      }
    )
    const post = await postData.json()

    const latestBlogsData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?format=simple`, 
      { 
        method: 'GET',
        headers: { 
          'OmhWeb-Auth': `${omhweb_auth}`,
          'Accept-Country': `${CountryData}`
        }
      }
    )
    const latest = await latestBlogsData.json()

    return {
      props: {
        post: post,
        country,
        latest: latest
      }
    }
  
}

export default post 
