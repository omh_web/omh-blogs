import React from 'react'
import getConfig from 'next/config'
import fetch from 'isomorphic-unfetch'
import Head from 'next/head'
import useMediaQuery from 'react-responsive'
import { useRouter, withRouter } from "next/router"
import dynamic from 'next/dynamic'

const DynamicComponent1 = dynamic(() => import('components/headerBar'))
const DynamicComponent5 = dynamic(() => import('components/footer'))

const DynamicComponent2 = dynamic(() => import('modules/category/categoryList'))
const DynamicComponent3 = dynamic(() => import('modules/category/featuredCategory'))
const DynamicComponent4 = dynamic(() => import('components/error'))

const LargeDesktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 1025 })
  return isDesktop ? children : null
}
const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1024 })
  return isTablet ? children : null
}
const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ maxWidth: 768 })
  return isDesktop ? children : null
}
const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 767 })
  return isMobile ? children : null
}

const COUNTRY = {
  sg: 'SGP',
  my: 'MYS',
  ph: 'PHL'
}

export const config = { amp : true }

function category ({ category, info, latest, country }) {

  const CountryData = COUNTRY[country] === undefined ? 'SGP' : COUNTRY[country]
  const router = useRouter()

  console.log(country)
  console.log(CountryData)

  const rootUrl =
  process.env.NODE_ENV === "development"
    ? 'http://localhost:3000'
    : ''
  

  return (
    <>
      {
        category.error  ? 
          <React.Fragment>
            <Head>
              <title>{'Ohmyhome Blog'}</title>
              <meta property="og:type" content="website"/>
              <link rel="shortcut icon" href="https://api.omh.app/store/cms/media/amp/favicon.png" />
            </Head>
            <DynamicComponent4
              latest={latest} 
              rootUrl={rootUrl}
            />
            <DynamicComponent5 country={CountryData}/>
          </React.Fragment>
        :
        <>
          <Head>
            <title>{`Ohmyhome - ${info?.name || ''}`}</title>
            <meta
              name="description"
              content={'Ohmyhome is Singapore’s leading property transaction ecosystem. Find the latest property updates, housing policy changes, enhance grants and other HDB news.'}
            />
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta property="og:url" content={`${rootUrl}/${router.query.lang_country}/category/${info?.slug}`} />
            <meta property="og:title" content={`Ohmyhome - ${info.name || ''}`} />
            <meta property="og:description" content={'Ohmyhome is Singapore’s leading property transaction ecosystem. Find the latest property updates, housing policy changes, enhance grants and other HDB news.'} />
            <meta name="keywords" content={'Property News, HDB News, Housing Policy Updates, Investors Guides, Buyer Guides, Seller Guides, Tenant Guides, Financial Planning, Home Renovation, Home Painting, Mortgage Advisory, Legal Conveyancing, Home Improvement, Interior Design, Property Blogs'} />
            <meta name="twitter:site" content={`${rootUrl}/${router.query.lang_country}/category/${info?.slug}`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:image" content={'https://api.omh.app/store/cms/media/amp/Ohmyhome-blog-property-hdb-news-oak.png'} />
            <meta property="og:image" content={'https://api.omh.app/store/cms/media/amp/Ohmyhome-blog-property-hdb-news-oak.png'} />
            <meta property="og:image:width" content="1200" />
            <meta property="og:image:height" content="630" />
            <meta property="og:type" content="website"/>
          </Head>
          <DynamicComponent1 
            info={info}
            rootUrl={rootUrl}/>
          <div id="category-wrapper">
            <div className="category-container">
              <div className="category-content">
                <div className="category-title">
                  <h1>{info.name}</h1>
                </div>
                <div className="category-description">
                  <p>{info.description}</p>
                </div>
                <div className="category-content_list">
                  <DynamicComponent2 
                    category={category}
                    rootUrl={rootUrl} 
                  />
                </div>
              </div>
              <div className="category-sidebar">
                <div className="category-wrapper">
                  <h2>TOP CATEGORIES</h2>
                  <DynamicComponent3 rootUrl={rootUrl}/>
                </div>
              </div>

              
              
            </div>

            <style jsx>{`
            .category-container {
              max-width: 1016px;
              margin: 16px auto 37px;
            }
      
            h1 {
              font-size: 28px;
              line-height: 33px;  
              color: #1C1C1C;
              margin: 0 0 15px;
              font-family: Tinos;
              font-weight: 600;
            }
            .category-description p {
              font-size: 16px;
              line-height: 24px;
              color: #5F5F5F;
            }
            .category-content {
              max-width: 632px;
              padding: 0 16px;
              width: 100%;
            }
            .category-sidebar {
              display: none;
            }
            @media only screen and (min-width: 768px) {
              .category-container {
                display: flex;
                justify-content: space-between;
                margin: 32px auto 121px;
                height: 100%;
              }
              
              .category-sidebar {
                max-width: 288px;
                padding: 16px;
                display: block;
                width: 100%;
                box-sizing: border-box;
                min-height: 100%;
              }
              .category-description {
                margin: 0 0 40px;
              }
              .category-wrapper {
                position: sticky;
                top: 40px;
              }
              .category-description {
                margin: 0 0 48px;
              }
              h1 {
                font-size: 36px;
                line-height: 43px;
                margin: 0 0 29px;
              }
              h2 {
                color: #5F5F5F;
                font-weight: 600;
                font-size: 13px;
                line-height: 15px;
                padding: 0 16px;
              }
              
            }
            @media only screen and (min-width: 1025px) { 
              div#category-wrapper {
                padding: 198px 0 0 ;
              }
              .category-wrapper {
                top: 100px;
              }
              h1 {
                font-size: 26px;
                line-height: 31px;
                color: #1C1C1C;
                margin: 0 0 8px;
              }
              .category-description p {
                font-weight: 300;
                font-size: 18px;
                line-height: 32px;
              }
            }
            @media only screen and (min-width: 1250px) { 

            }

            `}</style>    
          </div>
          <DynamicComponent5 country={CountryData}/>
        </>
      }
    </>
  )
}

export async function getServerSideProps(context) {
  const { slug } = context.query
  const { omhweb_auth }  = process.env
  const { publicRuntimeConfig } = getConfig()
  // const country  = context.query.lang_country.toString().split(/-/)[1]
  const country  = context.query && context.query.lang_country && context.query.lang_country.toString().split(/-/)[1] === undefined ? 'sg' : context.query.lang_country.toString().split(/-/)[1]
  const CountryData = COUNTRY[country] === undefined ? 'SGP' : COUNTRY[country]

  
  const categoryData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?category=${slug}&perPage=999&format=full`, 
    {
      params: { category: slug },
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': `${COUNTRY[country]}`
      }
    }
  )
  const category = await categoryData.json()

  const categoryInfo = await fetch(`https://cms.staging.ohmyhome.io/api/blog/category?slug=${slug}`, 
    {
      params: { info: slug },
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': `${COUNTRY[country]}`
      }
    }
  )
  const info = await categoryInfo.json()

  const latestBlogsData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?format=simple`, 
      { 
        method: 'GET',
        headers: { 
          'OmhWeb-Auth': `${omhweb_auth}`,
          'Accept-Country': `${CountryData}`
        }
      }
    )
    const latest = await latestBlogsData.json()

  return {
    props: {
      category: category,
      info: info,
      latest: latest,
      country
    }
  }
}

export default category 