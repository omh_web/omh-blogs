import React from 'react'
import dynamic from 'next/dynamic'
import Head from 'next/head'

const DynamicComponent1 = dynamic(() => import('components/notFound'))
const DynamicComponent2 = dynamic(() => import('components/footer'))

export const config = { amp : true }

const rootUrl =
  process.env.NODE_ENV === "development"
    ? 'http://localhost:3000'
    : ''
const CountryData = 'SGP'
const Error = () => {

 

  return (
    <React.Fragment>
      <Head>
        <title>{'Ohmyhome Blog'}</title>
        <meta property="og:type" content="website"/>
        <link rel="shortcut icon" href="https://api.omh.app/store/cms/media/amp/favicon.png" />
      </Head>
       <DynamicComponent1
        rootUrl={rootUrl}
      /> 
      <DynamicComponent2 country={CountryData}/>
    </React.Fragment>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : res ? err.statusCode : 404
  return { statusCode }
}

export default Error
