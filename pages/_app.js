

import NextHead from 'next/head'

import { useRouter, withRouter } from "next/router"
import * as TagManager from 'react-gtm-module'

import { CONFIG } from 'services/gtm'


function MyApp({ Component, pageProps }) {

  return (
    <>
      <NextHead>
        <meta charSet='UTF-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link rel='icon' sizes='192x192' href='/static/touch-icon.png' />
        <link rel='apple-touch-icon' href='/static/touch-icon.png' />
        <link rel='mask-icon' href='/static/favicon-mask.svg' color='#49B882' />
        <link rel='icon' href='/static/favicon.png' />
      </NextHead>

      <amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-NHKS4HL&gtm_auth=F-OYqgmWzQRPjzrC7dQf3Q&gtm_preview=env-3&gtm_cookies_win=x&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>
      
      <Component {...pageProps} />
    
      <style global jsx>
        {`
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/PublicSans-Roman-VF.woff2');
            font-family: 'Public Sans VF';
            font-style: normal;
          }
          
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/PublicSans-Italic-VF.woff2');
            font-family: 'Public Sans VF';
            font-style: italic;
          }
  
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/Tinos-Bold.ttf');
            font-family: 'Tinos';
            font-style: normal;
            font-weight: bold;
          }
          
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/Tinos-BoldItalic.ttf');
            font-family: 'Tinos';
            font-style: italic;
            font-weight: bold;
          }
  
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/Tinos-Italic.ttf');
            font-family: 'Tinos';
            font-style: normal;
            font-style: italic;
          }
  
          @font-face {
            src: url('https://ohmyhome-z.s3-ap-southeast-1.amazonaws.com/media/singapore/fonts/Tinos-Regular.ttf');
            font-family: 'Tinos';
            font-style: normal;
          }
          
          
          html {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            box-sizing: border-box;
            overflow-y: scroll;
          }
          
          body {
            margin: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: hsla(0, 0%, 0%, 0.8);
            font-family: 'Public Sans VF', sans-serif;
            font-variation-settings: 'wght' 350;
            font-size: 16px;
            font-weight: normal;
            word-wrap: break-word;
            font-kerning: normal;
            -moz-font-feature-settings: "kern", "liga", "clig", "calt";
            -ms-font-feature-settings: "kern", "liga", "clig", "calt";
            -webkit-font-feature-settings: "kern", "liga", "clig", "calt";
            font-feature-settings: "kern", "liga", "clig", "calt";
          }
          .latest-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .featured-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .popular-image img {
            object-fit: cover;
            width: 100%;
          }
          .list-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .category-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .related-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .latest-card-image img {
            object-fit: cover;
            width: 100%;
          }
          .custom-heavy {
            font-variation-settings: 'wght' 580;
          }
          
          .custom-bold {
            font-variation-settings: 'wght' 520;
          }
          
          .custom-semibold {
            font-variation-settings: 'wght' 450;
          }
          
          .custom-medium {
            font-variation-settings: 'wght' 350;
          }
          button {
            padding: 15px 25px;
            border-radius: 4px;
            transition: all 100ms ease 0s;
            font-size: 14px;
            cursor: pointer;
            text-rendering: auto;
            letter-spacing: normal;
            word-spacing: normal;
            text-transform: none;
            text-indent: 0px;
            text-shadow: none;
          }
          button.orange {
            border: 1.2px solid #E55710;
            background: #E55710;
            color: #FFF;
          }
          button.orange:hover {
            background: #DD5D24;
          }
          button.orange:active {
            background: #E55710;
            color: #FFF;
          }
          button.transparent-orange {
            border: 1px solid #E55710;
            background-color: #FFF;
            color: #E55710;
          }
          button.transparent-orange:active, button.transparent-orange:hover {
            background: #FEF8F5;
          }
          button.white {
            border: 1px solid #FFF;
            background: #FFF;
            color: #E55710;
          }
          button.white:hover {
            background: #FEF8F5;
          }
          button.transparent-white {
            border: 1px solid #E55710;
            background-color: #FFF;
            color: #E55710;
          }
          button.transparent-white:hover {
            background: #FEF8F5;
          }
          
          button.transparent-white:active {
            background: #FEF8F5;
          }	
          .hidden {
            display: none;
          }	
          article,
          aside,
          details,
          figcaption,
          figure,
          footer,
          header,
          main,
          menu,
          nav,
          section,
          summary, 
          div {
            display: block;
          }
          audio,
          canvas,
          progress,
          video {
            display: inline-block;
          }
          audio:not([controls]) {
            display: none;
            height: 0;
          }
          progress {
            vertical-align: baseline;
          }
          [hidden],
          template {
            display: none;
          }
          a {
            background-color: transparent;
            -webkit-text-decoration-skip: objects;
            transition: all 200ms ease 0s;
            color:  #E55710;
            text-decoration: none;
          }
          a:active,
          a:hover {
            outline-width: 0;
            color:  #DD5D24;
          }
          abbr[title] {
            border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
            cursor: help;
            text-decoration: none;
          }
          b,
          strong {
            font-weight: inherit;
            font-weight: bolder;
          }
          dfn {
            font-style: italic;
          }
          h1 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 2.25rem;
            line-height: 1.1;
          }
          mark {
            background-color: #ff0;
            color: #000;
          }
          small {
            font-size: 80%;
          }
          sub,
          sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline;
          }
          sub {
            bottom: -0.25em;
          }
          sup {
            top: -0.5em;
          }
          img {
            border-style: none;
            max-width: 100%;
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          .content-wrapper amp-img {
            border-radius: 10px;
            background: #FEF4EE;
            padding: 24px;
            border-box: box-sizing;
          }
          .content-wrapper amp-img:before {
            content: " ";
            border: 24px solid #fef4ee;
            z-index: 1;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          svg:not(:root) {
            overflow: hidden;
          }
          code,
          kbd,
          pre,
          samp {
            font-size: 1em;
          }
          figure {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          hr {
            box-sizing: content-box;
            overflow: visible;
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: calc(1.45rem - 1px);
            background: hsla(0, 0%, 0%, 0.2);
            border: none;
            height: 1px;
          }
          input,
          optgroup,
          select,
          textarea {
            font: inherit;
            margin: 0;
          }
          optgroup {
            font-weight: 700;
          }
          fieldset {
            border: 1px solid silver;
            padding: 0.35em 0.625em 0.75em;
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          legend {
            box-sizing: border-box;
            color: inherit;
            display: table;
            max-width: 100%;
            padding: 0;
            white-space: normal;
          }
          textarea {
            overflow: auto;
          }
          [type="checkbox"],
          [type="radio"] {
            box-sizing: border-box;
            padding: 0;
          }
          [type="number"]::-webkit-inner-spin-button,
          [type="number"]::-webkit-outer-spin-button {
            height: auto;
          }
          [type="search"] {
            -webkit-appearance: textfield;
            outline-offset: -2px;
          }
          [type="search"]::-webkit-search-cancel-button,
          [type="search"]::-webkit-search-decoration {
            -webkit-appearance: none;
          }
          ::-webkit-input-placeholder {
            color: inherit;
            opacity: 0.54;
          }
          ::-webkit-file-upload-button {
            -webkit-appearance: button;
            font: inherit;
          }
          * {
            box-sizing: inherit;
          }
          *:before {
            box-sizing: inherit;
          }
          *:after {
            box-sizing: inherit;
          }
          h2 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 1.62671rem;
            line-height: 1.1;
            font-size: 36px;
            line-height: 41px;
            color: #1C1C1C;
  
          }
          h3 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 1.38316rem;
            line-height: 1.1;

          }
          h4 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 1rem;
            line-height: 1.1;
   
          }
          h5 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 0.85028rem;
            line-height: 1.1;
    
          }
          h6 {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: inherit;
            text-rendering: optimizeLegibility;
            font-size: 0.78405rem;
            line-height: 1.1;

          }
          .content-container h2 {
            font-size: 36px;
            line-height: 41px;
            color: #1C1C1C;
            font-family: Tinos;
          }
          .content-container h3 {
            font-size: 28px;
            line-height: 33px;
            color: #1C1C1C;
            font-family: Tinos;
          }
          .content-container h4 {
            font-size: 20px;
            line-height: 25px;
            color: #1C1C1C;
            font-family: Tinos;
          }
          hgroup {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          ul {
            margin-left: 1.45rem;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            list-style-position: outside;
            list-style-image: none;
          }
          ol {
            margin-left: 1.45rem;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            list-style-position: outside;
            list-style-image: none;
          }
          dl {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          dd {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          .content-container th {
            padding: 20px;
            font-weight: 600;
          }
          .content-container td {
            padding: 20px;
          }
          p {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            color: #1C1C1C;
            line-height: 32px;
          }
          .content-container .content-container p {
            font-weight: 300;
            font-size: 15px;
            line-height: 27px;
          }
          pre {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            margin-bottom: 1.45rem;
            font-size: 0.85rem;
            line-height: 1.42;
            background: hsla(0, 0%, 0%, 0.04);
            border-radius: 3px;
            overflow: auto;
            word-wrap: normal;
            padding: 1.45rem;
          }
          table {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
            font-size: 1rem;
            line-height: 1.45rem;
            border-collapse: collapse;
            width: 100%;
          }
          blockquote {
            margin-left: 1.45rem;
            margin-right: 1.45rem;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          form {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          noscript {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          iframe {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          address {
            margin-left: 0;
            margin-right: 0;
            margin-top: 0;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            padding-top: 0;
            margin-bottom: 1.45rem;
          }
          b {
            font-weight: bold;
          }
          strong {
            font-weight: bold;
          }
          dt {
            font-weight: bold;
          }
          th {
            font-weight: bold;
          }
          li {
            margin-bottom: calc(1.45rem / 2);
          }
          ol li {
            padding-left: 0;
          }
          ul li {
            padding-left: 0;
          }
          li > ol {
            margin-left: 1.45rem;
            margin-bottom: calc(1.45rem / 2);
            margin-top: calc(1.45rem / 2);
          }
          li > ul {
            margin-left: 1.45rem;
            margin-bottom: calc(1.45rem / 2);
            margin-top: calc(1.45rem / 2);
          }
          blockquote *:last-child {
            margin-bottom: 0;
          }
          li *:last-child {
            margin-bottom: 0;
          }
          p *:last-child {
            margin-bottom: 0;
          }
          li > p {
            margin-bottom: calc(1.45rem / 2);
          }
          code {
            font-size: 0.85rem;
            line-height: 1.45rem;
          }
          kbd {
            font-size: 0.85rem;
            line-height: 1.45rem;
          }
          samp {
            font-size: 0.85rem;
            line-height: 1.45rem;
          }
          abbr {
            border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
            cursor: help;
          }
          acronym {
            border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
            cursor: help;
          }
          thead {
            text-align: left;
          }
          td,
          th {
            text-align: left;
            border-bottom: 1px solid hsla(0, 0%, 0%, 0.12);
            font-feature-settings: "tnum";
            -moz-font-feature-settings: "tnum";
            -ms-font-feature-settings: "tnum";
            -webkit-font-feature-settings: "tnum";
            padding-left: 0.96667rem;
            padding-right: 0.96667rem;
            padding-top: 0.725rem;
            padding-bottom: calc(0.725rem - 1px);
          }
          
          
          
          .title1 {
            font-size: 36px;
            font-variation-settings: 'wght' 580;
            line-height: 42px;
          }
          
          .title2 {
            font-size: 28px;
            font-variation-settings: 'wght' 580;
            line-height: 100%;
          }
          
          .title3 {
            font-size: 26px;
            font-variation-settings: 'wght' 580;
            line-height: 100%;
          }
          
          .title4 {
            font-size: 24px;
            font-variation-settings: 'wght' 580;
            line-height: 100%;
          }
          
          .title5 {
            font-size: 20px;
            font-variation-settings: 'wght' 580;
            line-height: 100%;
          }
          
          .title6 {
            font-size: 18px;
            font-variation-settings: 'wght' 580;
            line-height: 100%;
          }
          .submit-success {
            display:  none;
          }
          .submit-error {
            display: none;
          }
          .blog-cta_container .amp-form-submit-success .submit-success {
            display: block;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            display: block;
            width: 100%;
            color: #FFF;
          }
          .amp-form-submit-success .subscribe-form {
            display: none;
          }
          .amp-form-submit-success .submit-success {
            display: block;
          }
          .submit-success {
            font-size: 20px;
            text-align: center;
          }
          .submit-success .success-image_wrapper {
            width: 100%;
            margin: 50px auto 0;
          }
          .header-option {
            list-style: none;
            margin: 0;
            padding: 0 5px;
            display: flex;
            position: absolute;
            bottom: 0;
            right: 0;
            left: 0;
            z-index: 4;
            justify-content: space-between;
          }
          .header-option li {
            background: #FFF;
            text-indent: -9999px;
            width: 24%;
            height: 5px;
            cursor: pointer;
            z-index: 5;
            outline: none;
            position: relative;
          }
          .header-option li.react-tabs__tab--selected {
            background: rgba(255, 255, 255, 0.2)
          }
          .header-option li:before {
            content: "1";
            position: absolute;
            left: 0;
            right: 0;
            top: -10px;
            bottom: 0;
            padding: 14px 0;
            z-index: 999;
          }
          .popular-content amp-carousel > div > div:last-child {
            bottom: auto;
            cursor: pointer;
          }
          .popular-content amp-carousel > div > div:last-child > div{
            z-index: 1;
          }
          blockquote {
            background: #FEF4EE;
            border-radius: 10px;
            padding: 32px 40px;
            font-size: 22px;
            line-height: 33px;
            color: #1C1C1C;
            text-align: center;
            margin: 0 0 40px;
            display: block;
            font-family: Tinos;
            font-weight: 600;
          }
          blockquote:before {
            content: '“';
            margin-right: 5px;
          }
          blockquote:after {
            content: '“';
            margin-left: 5px;
          }
          .header-image-wrapper a {
            padding: 44px 36px 31px;
            border-bottom: 1px solid rgba(224, 224, 224, 0.7);
            display: block;
          }
          .header-navigation-wrapper a{
            font-size: 14px;
            line-height: 14px;
            color: rgb(28, 28, 28);
            padding: 21px 36px;
            display: block;
            font-variation-settings: "wght" 520;
          }
          .header-navigation-wrapper {
            border-bottom: 1px solid rgba(224, 224, 224, 0.7);
            display: block;
          }
          
          @media only screen and (max-width: 480px) {
            html {
              font-size: 100%;
            }
          }
          @media only screen and (min-width: 768px) {
            .content-container .content-container p {
              font-size: 18px;
              line-height: 32px;
            }
          }
          @media only screen and (min-width: 768px) and (max-width: 1024px) {
            .title1XL {
              font-size: 40px;
              line-height: 47px;
            }
            
            .title1 {
              font-size: 40px;
              line-height: 47px;
            }
            
            .title2 {
              font-size: 36px;
            }
            
            .title3 {
              font-size: 28px;
            }
            
            .title4 {
              font-size: 26px;
            }
            
            .title5 {
              font-size: 24px;
            }
            
            .title6 {
              font-size: 20px;
            }
          }
          @media only screen and (min-width: 1025px) {
            .title1XL {
              font-size: 65px;
              line-height: 72px;
            }
            .title1 {
              font-size: 52px;
            }
            .title2 {
              font-size: 40px;
            }
            .title3 {
              font-size: 36px;
            }
            .title4 {
              font-size: 28px;
            }
            .title5 {
              font-size: 26px;
            }
            .title6 {
              font-size: 24px;
            }
            .header-navigation-wrapper {
              display: flex;
              justify-content: flex-start;
              border: 0px solid;
            }
            .header-image-wrapper a {
              padding: 0;
              border: 0px solid;
            }
            .header-navigation-wrapper a {         
              color: #1C1C1C;
              font-size: 14px;
              line-height: 14px;
              text-decoration: none;
              padding: 30px 15px;
              transition: all .2s ease-out .1s;
              display: block;
            } 
            .header-navigation-wrapper .navigation-item {         
              transition: all .2s ease-out .1s;
              border-bottom: 5px solid #FFF;
            } 
            .header-navigation-wrapper .navigation-item:hover {         
              border-bottom: 5px solid #D8571D;
            }
            .header-navigation-wrapper .active {         
              border-bottom: 5px solid #D8571D;
            } 
          }
        `}
      </style>
    </>
  )
}

export default MyApp
