import Document, { Html, Head, Main, NextScript } from 'next/document'
import {Layout} from "react-amp-components";


class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {

    
    return (
      <Html>
        <Head></Head>
        <body>
          <Main />
          <NextScript />
          {/*<Layout>
            <script type="application/ld+json">{`
              {
                "vars" : {
                  "gtmId": "GTM-TCPHJ4X",
                  "auth": "uMLVPpLT-8XI7CGVyZ2ZLg",
                  "preview": "env-6",
                }
              }
            `}</script> 
           <amp-analytics type="gtag" data-credentials="include">
              <script type="application/json">
                {`
                  "vars" : {
                    "gtmId": "GTM-TCPHJ4X",
                    "auth": "uMLVPpLT-8XI7CGVyZ2ZLg",
                  }
                `}
              </script>
            </amp-analytics>
          </Layout> */}
        </body>
      </Html>
    )
  }
}

export default MyDocument