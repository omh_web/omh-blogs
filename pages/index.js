import React from 'react'
import Head from 'next/head'
import fetch from 'isomorphic-unfetch'
import dynamic from 'next/dynamic'

const DynamicComponent1 = dynamic(() => import('components/headerBar'))
const DynamicComponent9 = dynamic(() => import('components/footer'))

const DynamicComponent2 = dynamic(() => import('modules/landing/latestBlogs'))
const DynamicComponent3 = dynamic(() => import('modules/landing/featuredBlogs'))
const DynamicComponent4 = dynamic(() => import('modules/landing/popularBlogs'))
const DynamicComponent5 = dynamic(() => import('modules/landing/featuredCategory'))
const DynamicComponent6 = dynamic(() => import('modules/landing/form'))
const DynamicComponent7 = dynamic(() => import('modules/landing/listBlogs'))
const DynamicComponent8 = dynamic(() => import('modules/landing/categoryList'))

export async function getStaticProps() {
  const { omhweb_auth }  = process.env

  const latestBlogsData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?format=simple&perPage=7`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'SGP'
      }
    }
  )
  const latest = await latestBlogsData.json()

  const featuredBlogsData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/list/formatted?code=sgp-featured-blogs&format=simple`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'SGP'
      }
    }
  )
  const featured = await featuredBlogsData.json()

  const popularBlogsData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/list/formatted?code=sgp-popular-blogs&format=simple`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'SGP'
      }
    }
  )
  const popular = await popularBlogsData.json()

  const categoryOneSectionData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?perPage=4&format=simple`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'SGP'
      }
    }
  )
  const categoryOneSection = await categoryOneSectionData.json()

  const categoryTwoSectionData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?perPage=4&format=simple`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'PHL'
      }
    }
  )
  const categoryTwoSection = await categoryTwoSectionData.json()

  const categoryThreeSectionData = await fetch(`https://cms.staging.ohmyhome.io/api/blogs/formatted?perPage=4&format=simple`, 
    { 
      method: 'GET',
      headers: { 
        'OmhWeb-Auth': `${omhweb_auth}`,
        'Accept-Country': 'MYS'
      }
    }
  )
  const categoryThreeSection = await categoryThreeSectionData.json()

  return {
    props: {
      latest: latest,
      featured: featured,
      popular: popular,
      categoryOneSection: categoryOneSection,
      categoryTwoSection: categoryTwoSection,
      categoryThreeSection: categoryThreeSection,
      // country: country
    },
    revalidate: 10,
  }
}

export const config = { amp : true }

function blog ({latest, featured, popular, categoryOneSection, categoryTwoSection, categoryThreeSection}) {

  const CountryData = 'SGP'

  const CATEGORYONEINFO = {
    data: {
      title: 'Singapore Latest Blogs',
      description: 'Catch the latest property news in Singapore.',
      slug: 'en-sg' 
    },
  }
  const CATEGORYTHREEINFO = {
    data: {
      title: 'Malaysia Latest Blogs',
      description: 'Catch the latest property news in Philippines.',
      slug: 'en-my' 
    },
  }
  const CATEGORYTWOINFO = {
    data: {
      title: 'Philippines Latest Blogs',
      description: 'Catch the latest property news in Malaysia.',
      slug: 'en-ph' 
    },
  }
  return (
    <>
      <Head>
        <title>{'Ohmyhome Blog'}</title>
        <meta
          name="description"
          content={'Ohmyhome is the leading property transaction ecosystem. Find the latest Singapore property updates, HDB news, housing policies, CPF housing grants, and how to sell your property fast at affordable agent fees. Buy your property in Singapore with Ohmyhome`s VIP real estate services'}
        />
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:url" content={'https://blog.ohmyhome.com'} />
        <meta property="og:title" content={'Blog Title | Ohmyhome'} />
        <meta property="og:description" content={'Ohmyhome is the leading property transaction ecosystem. Find the latest Singapore property updates, HDB news, housing policies, CPF housing grants, and how to sell your property fast at affordable agent fees. Buy your property in Singapore with Ohmyhome`s VIP real estate services'} />
        <meta name="keywords" content={'Property News, HDB News, Housing Policy Updates, Investors Guides, Buyer Guides, Seller Guides, Tenant Guides, Financial Planning, Home Renovation, Home Painting, Mortgage Advisory, Legal Conveyancing, Home Improvement, Interior Design, Property Blogs, Property Singapore, Sell Property in Singapore, Buy Property in Singapore'} />
        <meta name="twitter:site" content={'https://z.omh.sg/media/amp/landing-hero.jpg'} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:image" content={'https://z.omh.sg/media/amp/landing-hero.jpg'} />
        <meta property="og:image" content={'https://z.omh.sg/media/amp/landing-hero.jpg'} />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta property="og:type" content="website"/>
        <link rel="shortcut icon" href="https://api.omh.app/store/cms/media/amp/favicon.png" />
      </Head>
      <DynamicComponent1/>
      <div className="hidden for-seo">
        <h1>Ohmyhome Real Estate Blogs</h1>
      </div>
      <div id="blog-wrapper">
        <div className="blog-container">

          <div className="blog-content_wrapper">
            <div className="blog-content_container">
              <div className="latest-wrapper">
                <h2>Latest</h2>
                <DynamicComponent2 
                  latest={latest} 
                />
              </div>
              <div className="featured-wrapper">
                <h2>Featured</h2>
                <DynamicComponent3 
                  featured={featured} 
                />
              </div>
              <div className="popular-wrapper">
                <h2>Popular</h2>
                <DynamicComponent4 
                  popular={popular} 
                />
                {/* <div className="featured-category">
                  <FeaturedCategory />
                </div> */}
              </div>
            </div>
          </div>

          <div className="blog-cta_wrapper">
            <div className="blog-cta_container">
              <DynamicComponent6 />
            </div>
          </div>

          <div className="list-content_wrapper">
            <div className="list-content-container">
              <DynamicComponent7 
                list={latest} 
              />
            </div>  
          </div>

           <div className="blog-category_wrapper">
            <div className="blog-category_container">
              <DynamicComponent8 
                category={categoryOneSection} 
                title={`${CATEGORYONEINFO.data.title}`}
                description={`${CATEGORYONEINFO.data.description}`}
                slug={`${CATEGORYONEINFO.data.slug}`}
              />
            </div>  
          </div>

          <div className="blog-category_wrapper">
            <div className="blog-category_container">
              <DynamicComponent8 
                category={categoryTwoSection} 
                title={`${CATEGORYTWOINFO.data.title}`}
                description={`${CATEGORYTWOINFO.data.description}`}
                slug={`${CATEGORYTWOINFO.data.slug}`}
              />
            </div>  
          </div>

          <div className="blog-category_wrapper">
            <div className="blog-category_container">
              <DynamicComponent8 
                category={categoryThreeSection} 
                title={`${CATEGORYTHREEINFO.data.title}`}
                description={`${CATEGORYTHREEINFO.data.description}`}
                slug={`${CATEGORYTHREEINFO.data.slug}`}
              />
            </div>  
          </div>
          
        </div>

        <style jsx>{`
        .blog-content_wrapper {
          margin: 0 auto 32px; 
          max-width: 1186px;
          padding: 0 16px;
          flex: 1 1 auto;
          display: flex;
          flex-flow: column;
        }
        #blog-wrapper {
          margin: 16px 0 0;
        }
        .blog-content_container {
          display: flex;
          flex-wrap: wrap;
          flex-direction: row;
        }
        .latest-wrapper {
          box-sizing: border-box;
          position: relative;
          width: 100%;
          order: 2;
          margin: 0 0 32px;
        }
        .featured-wrapper {
          box-sizing: border-box;
          position: relative;
          width: 100%;
          order: 1;
          margin: 0 0 32px;
        }
        .popular-wrapper {
          width: 100%;
          box-sizing: border-box;
          position: relative;
          order: 3
        }
        
        .blog-content_container h2 {
          font-size: 24px;
          line-height: 28px;
          margin: 0 0 38px;
          color: #1C1C1C;
          margin: 0 0 28px 0;
        }
        .blog-cta_wrapper {
          position: relative;
          margin: 0 0 48px;
        }
        .blog-category_container {
          max-width: 1224px;
          margin: 0 auto;
        }
        .blog-cta_container {
          max-width: 1224px;
          margin: 0 auto;
        }
        .list-content-container {
          max-width: 1224px;
          margin: 0 auto;
        }
        @media only screen and (min-width: 768px) {
          div#blog-wrapper {
            padding: 108px 0 0 ;
          }
          .blog-content_container {
            display: flex;
          }
          .blog-content_wrapper {
            padding: 0 24px;
            box-sizing: content-box;
            margin-bottom: 27px;
          }
          .blog-content_container h2 {
            font-size: 24px;
            line-height: 28px;
            margin: 0 0 18px 14px;
          }
          .latest-wrapper {
            flex: 0 0 100%;
            margin: 40px 0 0;
            order: 3;
          }
          .featured-wrapper {
            flex: 0 0 69%;
            order: 1;
          }
          .popular-wrapper {
            flex: 0 0 30%;
            order: 2;
          }
          .list-content_wrapper {
            margin: 0 0 40px;
          }
          .blog-cta_wrapper {
            margin: 0 0 40px;
          }
          .blog-category_wrapper {
            margin: 0 0 40px;
          }
          .featured-category {
            padding: 0 28px;
          }
        }
        @media only screen and (min-width: 1025px) {
          
          .blog-content_wrapper {
            margin-bottom: 142px;
          } 
          .blog-content_container {
            display: flex;
            flex-wrap: wrap;
          }
          .blog-content_container h2 {
            margin: 0 0 38px 32px;
          }
          .latest-wrapper {
            flex: 0 0 25%;
            padding-right: 12px;
            margin: 0px 0 0;
            order: 1;
          }
          .latest-wrapper:after {
            content: " ";
            position: absolute;
            opacity: .2;
            right: 0;
            background: #000;
            width: 1px;
            height: 1020px;
            top: 63px;
          }
          .featured-wrapper {
            flex: 0 0 50%;
            padding-right: 15px;
            padding-left: 15px;
            order: 2;
          }
          .popular-wrapper {
            flex: 0 0 25%;
            order: 3;
          }
          .popular-wrapper:before {
            content: " ";
            position: absolute;
            opacity: .2;
            left: 0;
            background: #000;
            width: 1px;
            height: 1020px;
            top: 63px;
          }
          
          .list-content_wrapper {
            margin: 0 0 80px;
          }
          .blog-cta_wrapper {
            margin: 0 0 80px;
          }
          .blog-category_wrapper {
            margin: 0 0 80px;
          }
        }
        @media screen and (min-width: 1441px) {
          .blog-content_wrapper {
            max-width: 1760px;
            width: 80%;
          }
          .blog-cta_container {
            max-width: 1760px;
            width: 80%;
          }
          .list-content-container {
            max-width: 1760px;
            width: 80%;
          }
          .blog-category_container {
            max-width: 1760px;
            width: 80%;
          }
        }

        `}</style>    
      </div>
      <DynamicComponent9 country={CountryData}/>
    </>
    
  )
}

export default blog 
