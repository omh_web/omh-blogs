export const CONFIG = {
  'QA': {
    'sg': {
      gtmId: 'GTM-TCPHJ4X',
      auth: 'uMLVPpLT-8XI7CGVyZ2ZLg',
      preview: 'env-6'
    },
    'my': {
      gtmId: 'GTM-53DWVTF',
      auth: 'ZjaJKf0FHHtP1ZkTLJtRwg',
      preview: 'env-2'
    },
    'ph': {
      gtmId: 'GTM-P2DF5HL',
      auth: 'wU__RbNs3ZfdvXwm9oakkQ',
      preview: 'env-3'
    }
  },
  'PROD': {
    'sg': {
      gtmId: 'GTM-TCPHJ4X',
      auth: 'yYthFGS5OlfDoYHliv4hiw',
      preview: 'env-2'
    },
    'my': {
      gtmId: 'GTM-53DWVTF',
      auth: '1rV8ZYC44iNu1xhqYU8UPw',
      preview: 'env-1'
    },
    'ph': {
      gtmId: 'GTM-P2DF5HL',
      auth: 'V-KCzrqzlPMxplRw9UG6sQ',
      preview: 'env-1'
    }
  }
}