import React from 'react'

import MobileFooter from './mobile'
import DesktopFooter from './desktop'


export default function FooterSeo({country}) {
  return (
    <div
      className="md:py-10 lg:py-10 xl:py-10 footer_wrapper"
    >
      <div className="desktop">
        <DesktopFooter country={country}/>
      </div>
      <div className="mobile">
        <MobileFooter country={country}/>
      </div>

        

      <style jsx>{`
        .footer_wrapper {
          border-top: 1px solid #E0E0E0;
          z-index: 5;
          position: relative;
        }
        .desktop {
          display: none;
        }
        .mobile { 
          display: block;
        }
        @media screen and (min-width: 1025px) {
          .desktop {
            display: block;
          }
          .mobile {
            display: none;
          }
        }
      `}</style>
    </div>
  )
}
