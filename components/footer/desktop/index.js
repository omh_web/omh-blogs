import React from 'react'
import MediaQuery from 'react-responsive'
import { useRouter, withRouter } from "next/router"

function DesktopFooter({country}) {

  const router = useRouter()

  const OhmyhomeLogo = (
    <svg width="143" height="28" viewBox="0 0 143 28" fill="none" xmlns="http://www.w3.org/2000/svg">
    <title>Buy Sell Rent Real Estate</title>
    <path fillRule="evenodd" clipRule="evenodd" d="M7.40508 22.1897C11.5983 22.1897 14.8102 18.9673 14.8102 14.456C14.8102 9.85263 11.5983 6.72232 7.40508 6.72232C3.21184 6.72232 0 10.0368 0 14.456C0 18.8753 3.21184 22.1897 7.40508 22.1897ZM7.40626 10.3118C9.28011 10.3118 10.9755 11.6928 10.9755 14.4548C10.9755 17.2169 9.28011 18.5979 7.40626 18.5979C5.53241 18.5979 3.83702 17.2169 3.83702 14.4548C3.83702 11.6928 5.53241 10.3118 7.40626 10.3118Z" fill="#EE620F"/>
    <path d="M20.8773 21.7283H17.1302V0.184387H20.9666V8.28635C21.8587 7.18154 23.4647 6.62913 24.8029 6.62913C28.3716 6.62913 30.0668 9.20703 30.0668 12.4294V21.6362H26.2304V13.0739C26.2304 11.4167 25.4274 10.1277 23.6431 10.1277C22.0372 10.1277 21.0558 11.4167 20.9666 12.9818L20.8773 21.7283Z" fill="#EE620F"/>
    <path d="M33.1004 21.7291V7.09037H36.7583V8.83966C37.5613 7.36658 39.3457 6.63004 40.8624 6.63004C42.8252 6.63004 44.3419 7.45865 45.0556 9.0238C46.2154 7.27451 47.6429 6.63004 49.5165 6.63004C52.1038 6.63004 54.6019 8.28725 54.6019 12.1541V21.6371H50.944V12.9827C50.944 11.4176 50.2302 10.2207 48.4459 10.2207C46.84 10.2207 45.8586 11.5096 45.8586 13.0748V21.6371H42.1114V12.9827C42.1114 11.4176 41.3084 10.2207 39.6133 10.2207C37.9182 10.2207 36.9368 11.5096 36.9368 13.0748V21.6371H33.1004V21.7291Z" fill="#EE620F"/>
    <path d="M58.3489 27.5294L61.65 19.8878L55.6724 7.09033H59.9548L63.702 15.6526L67.1815 7.09033H71.1963L62.3637 27.5294H58.3489Z" fill="#EE620F"/>
    <path d="M76.6384 21.7283H72.802V0.184387H76.6384V8.28635C77.5305 7.18154 79.1365 6.62913 80.4747 6.62913C84.0434 6.62913 85.7386 9.20703 85.7386 12.4294V21.6362H81.9022V13.0739C81.9022 11.4167 81.0993 10.1277 79.3149 10.1277C77.709 10.1277 76.7276 11.4167 76.6384 12.9818V21.7283Z" fill="#EE620F"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M95.196 22.1896C99.3892 22.1896 102.601 18.9672 102.601 14.4559C102.601 9.85247 99.3892 6.6301 95.196 6.72217C91.0027 6.72217 87.7909 10.0366 87.7909 14.4559C87.7909 18.8751 91.0027 22.1896 95.196 22.1896ZM95.1972 10.3118C97.071 10.3118 98.7664 11.6928 98.7664 14.4548C98.7664 17.2169 97.071 18.5979 95.1972 18.5979C93.3233 18.5979 91.6279 17.2169 91.6279 14.4548C91.6279 11.6928 93.3233 10.3118 95.1972 10.3118Z" fill="#EE620F"/>
    <path d="M104.831 21.7291V7.09037H108.489V8.83966C109.292 7.36658 111.076 6.63004 112.593 6.63004C114.556 6.63004 116.072 7.45865 116.786 9.0238C117.946 7.27451 119.374 6.63004 121.247 6.63004C123.834 6.63004 126.332 8.28725 126.332 12.1541V21.6371H122.585V12.9827C122.585 11.4176 121.872 10.2207 120.087 10.2207C118.481 10.2207 117.5 11.5096 117.5 13.0748V21.6371H113.753V12.9827C113.753 11.4176 112.95 10.2207 111.255 10.2207C109.56 10.2207 108.578 11.5096 108.578 13.0748V21.6371H104.831V21.7291Z" fill="#EE620F"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M135.699 22.2818C139.179 22.2818 141.409 20.0722 142.123 17.5863L139.001 16.5736C138.554 17.8625 137.573 18.7832 135.789 18.7832C133.915 18.7832 132.309 17.4022 132.22 15.4688H142.212C142.212 15.4688 142.302 14.8243 142.302 14.2719C142.302 9.57642 139.714 6.72232 135.343 6.72232C131.685 6.72232 128.384 9.76056 128.384 14.456C128.384 19.4277 131.863 22.2818 135.699 22.2818ZM135.433 9.94356C137.575 9.94356 138.467 11.3248 138.556 12.7981H132.31C132.4 11.1406 133.827 9.85148 135.433 9.94356Z" fill="#EE620F"/>
    <path d="M2.85522 24.9511L9.99371 21.7282H5.26446L2.85522 24.9511Z" fill="#EE620F"/>
    <path d="M14.8109 3.59064L7.40581 0L0.000732422 3.59064V7.27335L7.40581 3.68271L14.8109 7.27335V3.59064Z" fill="#EE620F"/>
    </svg>

  )
  const phone = (
    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.6125 8.94451C10.4095 8.82551 10.16 8.82801 9.958 8.94801L8.935 9.55751C8.706 9.69401 8.42 9.67801 8.21 9.51401C7.847 9.23051 7.2625 8.75351 6.754 8.24501C6.2455 7.73651 5.7685 7.15201 5.485 6.78901C5.321 6.57901 5.305 6.29301 5.4415 6.06401L6.051 5.04101C6.1715 4.83901 6.1725 4.58751 6.0535 4.38451L4.5525 1.82051C4.407 1.57251 4.118 1.45051 3.8385 1.51901C3.567 1.58501 3.2145 1.74601 2.845 2.11601C1.688 3.27301 1.0735 5.22451 5.4245 9.57551C9.7755 13.9265 11.7265 13.3125 12.884 12.155C13.2545 11.7845 13.415 11.4315 13.4815 11.1595C13.549 10.8805 13.429 10.5935 13.1815 10.4485C12.5635 10.087 11.2305 9.30651 10.6125 8.94451Z" fill="#343434"/>
    </svg>
  )
  const twirly = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M4.02539 2.99805C4.08189 3.00045 4.13808 3.00764 4.19336 3.01953C4.19336 3.01953 6.82279 3.53144 9.48047 4.99024C9.7277 5.12594 9.97047 5.32146 10.2188 5.47656C11.8905 4.62484 13.7711 4 16 4C19.7576 4 22.5673 5.38842 24.373 7.34766C26.1788 9.30689 27 11.7822 27 14C27 15.9014 26.5034 17.584 25.3418 19.3516C24.5333 20.5818 23.3953 21.8952 21.8887 23.4746L23.707 25.293C24.098 25.684 24.098 26.317 23.707 26.707C23.512 26.902 23.256 27 23 27H18C17.448 27 17 26.552 17 26V21C17 20.744 17.098 20.488 17.293 20.293C17.684 19.902 18.317 19.902 18.707 20.293L20.4727 22.0586C21.9198 20.535 22.9788 19.3035 23.6699 18.252C24.6713 16.7282 25 15.5856 25 14C25 12.2848 24.3378 10.2606 22.9023 8.70313C21.4668 7.14561 19.2774 6 16 6C14.4495 6 13.113 6.2995 11.9238 6.75C13.6247 8.24828 15 10.2688 15 13C15 16.817 12.4291 19 10 19C7.55077 19 5 16.7681 5 13C5 10.5149 6.19496 8.25021 8.19336 6.60742C5.97206 5.46304 3.80664 4.98047 3.80664 4.98047C3.55885 4.93391 3.33778 4.79548 3.1877 4.59289C3.03761 4.39029 2.96959 4.13849 2.99724 3.88788C3.02489 3.63727 3.14618 3.40635 3.33683 3.24135C3.52748 3.07636 3.77341 2.98946 4.02539 2.99805ZM9.91602 7.84961C8.08807 9.16622 7 11.0077 7 13C7 15.9099 8.74723 17 10 17C11.2729 17 13 15.973 13 13C13 10.6432 11.5882 9.09063 9.91602 7.84961Z" fill="#E86225"/>
    </svg>
  )
  const email = (
    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2.5 2.5C1.80937 2.5 1.25 3.05937 1.25 3.75V11.25C1.25 11.9406 1.80937 12.5 2.5 12.5H12.5C13.1906 12.5 13.75 11.9406 13.75 11.25V3.75C13.75 3.05937 13.1906 2.5 12.5 2.5H2.5ZM2.5 3.75H12.5V4.37622L7.5 7.5L2.5 4.37622V3.75ZM2.5 5.62622L7.5 8.75L12.5 5.62622V11.25H2.5V5.62622Z" fill="#1C1C1C"/>
    </svg>
  )
  const facebook = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Facebook</title>
      <path d="M15.075 3.75C8.82051 3.75 3.75 8.82051 3.75 15.075C3.75 20.7529 7.93253 25.441 13.3824 26.26V18.0766H10.5805V15.0997H13.3824V13.1189C13.3824 9.83925 14.9803 8.39943 17.706 8.39943C19.0115 8.39943 19.7018 8.4962 20.0287 8.54047V11.139H18.1693C17.0121 11.139 16.608 12.236 16.608 13.4725V15.0997H19.9993L19.5391 18.0766H16.608V26.2842C22.1356 25.5342 26.4 20.808 26.4 15.075C26.4 8.82051 21.3295 3.75 15.075 3.75Z" fill="#3578EA"/>
    </svg>
  )
  const instagram = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Instagram</title>
      <path d="M21.0938 26.7188H8.90625C6.32812 26.7188 4.21875 24.6094 4.21875 22.0312V9.84375C4.21875 7.26562 6.32812 5.15625 8.90625 5.15625H21.0938C23.6719 5.15625 25.7812 7.26562 25.7812 9.84375V22.0312C25.7812 24.6094 23.6719 26.7188 21.0938 26.7188Z" fill="url(#paint0_radial)"/>
      <path d="M15 9.375C17.1562 9.375 17.3906 9.375 18.2344 9.42188C19.0313 9.46875 19.4531 9.60938 19.7344 9.70313C20.1094 9.84375 20.3906 10.0312 20.6719 10.3125C20.9531 10.5938 21.1406 10.875 21.2813 11.25C21.375 11.5313 21.5156 11.9531 21.5625 12.75C21.5625 13.5469 21.5625 13.7813 21.5625 15.9375C21.5625 18.0937 21.5625 18.3281 21.5156 19.1719C21.4688 19.9688 21.3281 20.3906 21.2344 20.6719C21.0937 21.0469 20.9063 21.3281 20.625 21.6094C20.3437 21.8906 20.0625 22.0781 19.6875 22.2188C19.4062 22.3125 18.9844 22.4531 18.1875 22.5C17.3906 22.5 17.1562 22.5 15 22.5C12.8438 22.5 12.6094 22.5 11.7656 22.4531C10.9688 22.4063 10.5469 22.2656 10.2656 22.1719C9.89062 22.0312 9.60937 21.8438 9.32812 21.5625C9.04687 21.2812 8.85938 21 8.71875 20.625C8.625 20.3437 8.48438 19.9219 8.4375 19.125C8.4375 18.3281 8.4375 18.0937 8.4375 15.9375C8.4375 13.7813 8.4375 13.5469 8.48438 12.7031C8.53125 11.9063 8.67188 11.4844 8.76563 11.2031C8.90625 10.8281 9.09375 10.5469 9.375 10.2656C9.65625 9.98437 9.9375 9.79688 10.3125 9.65625C10.5938 9.5625 11.0156 9.42188 11.8125 9.375C12.6094 9.375 12.8438 9.375 15 9.375ZM15 7.96875C12.8438 7.96875 12.5625 7.96875 11.7188 8.01563C10.875 8.0625 10.3125 8.20312 9.79687 8.39062C9.28125 8.57812 8.8125 8.85937 8.39062 9.32812C7.96875 9.79687 7.6875 10.2187 7.45312 10.7344C7.26562 11.25 7.125 11.8125 7.07813 12.6562C7.03125 13.5 7.03125 13.7813 7.03125 15.9375C7.03125 18.0937 7.03125 18.375 7.07813 19.2188C7.125 20.0625 7.26562 20.625 7.45312 21.1406C7.64062 21.6562 7.92187 22.125 8.39062 22.5469C8.8125 22.9687 9.28125 23.25 9.79687 23.4844C10.3125 23.6719 10.875 23.8125 11.7188 23.8594C12.5625 23.9063 12.8438 23.9062 15 23.9062C17.1562 23.9062 17.4375 23.9063 18.2812 23.8594C19.125 23.8125 19.6875 23.6719 20.2031 23.4844C20.7187 23.2969 21.1875 23.0156 21.6094 22.5469C22.0312 22.125 22.3125 21.6562 22.5469 21.1406C22.7344 20.625 22.875 20.0625 22.9219 19.2188C22.9688 18.375 22.9688 18.0937 22.9688 15.9375C22.9688 13.7813 22.9688 13.5 22.9219 12.6562C22.875 11.8125 22.7344 11.25 22.5469 10.7344C22.3594 10.2187 22.0781 9.75 21.6094 9.32812C21.1406 8.90625 20.7187 8.625 20.2031 8.39062C19.6875 8.20312 19.125 8.0625 18.2812 8.01563C17.4375 7.96875 17.1562 7.96875 15 7.96875Z" fill="white"/>
      <path d="M15 11.7188C12.6562 11.7188 10.7812 13.5938 10.7812 15.9375C10.7812 18.2812 12.6562 20.1562 15 20.1562C17.3438 20.1562 19.2188 18.2812 19.2188 15.9375C19.2188 13.5938 17.3438 11.7188 15 11.7188ZM15 18.75C13.4531 18.75 12.1875 17.4844 12.1875 15.9375C12.1875 14.3906 13.4531 13.125 15 13.125C16.5469 13.125 17.8125 14.3906 17.8125 15.9375C17.8125 17.4844 16.5469 18.75 15 18.75Z" fill="white"/>
      <path d="M19.2188 12.6562C19.7365 12.6562 20.1562 12.2365 20.1562 11.7188C20.1562 11.201 19.7365 10.7812 19.2188 10.7812C18.701 10.7812 18.2812 11.201 18.2812 11.7188C18.2812 12.2365 18.701 12.6562 19.2188 12.6562Z" fill="white"/>
      <defs>
      <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.67606 31.0761) rotate(-49.945) scale(32.6463 21.5657)">
      <stop offset="0.073" stopColor="#EACC7B"/>
      <stop offset="0.184" stopColor="#ECAA59"/>
      <stop offset="0.307" stopColor="#EF802E"/>
      <stop offset="0.358" stopColor="#EF6D3A"/>
      <stop offset="0.46" stopColor="#F04B50"/>
      <stop offset="0.516" stopColor="#F03E58"/>
      <stop offset="0.689" stopColor="#DB359E"/>
      <stop offset="0.724" stopColor="#CE37A4"/>
      <stop offset="0.789" stopColor="#AC3CB4"/>
      <stop offset="0.877" stopColor="#7544CF"/>
      <stop offset="0.98" stopColor="#2B4FF2"/>
      </radialGradient>
      </defs>
    </svg>
  )
  const youtube = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Youtube</title>
      <path d="M27 21.1875C26.75 22.5 25.6875 23.5 24.375 23.6875C22.3125 24 18.875 24.375 15 24.375C11.1875 24.375 7.75 24 5.625 23.6875C4.3125 23.5 3.25 22.5 3 21.1875C2.75 19.75 2.5 17.625 2.5 15C2.5 12.375 2.75 10.25 3 8.8125C3.25 7.5 4.3125 6.5 5.625 6.3125C7.6875 6 11.125 5.625 15 5.625C18.875 5.625 22.25 6 24.375 6.3125C25.6875 6.5 26.75 7.5 27 8.8125C27.25 10.25 27.5625 12.375 27.5625 15C27.5 17.625 27.25 19.75 27 21.1875Z" fill="#FF3D00"/>
      <path d="M12.5 19.375V10.625L20 15L12.5 19.375Z" fill="white"/>
    </svg>
  )

  return (
    <div className="footer-wrapper">
      <div className="footer-top">
        <div>
          { country === 'SGP' ?
            <div className="address-box">
              <a href="https://omh.sg">{OhmyhomeLogo}</a>
              <div className="custom-bold">Singapore</div>
              <p>11 Lorong 3 Toa Payoh, Jackson Square Proptech Innovation Centre Block B, #04-17, Singapore 319579</p>
              <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:6597272131">+65 9727 2131</a> </p>
              <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:help@ohmyhome.com">help@ohmyhome.com</a> </p>
            </div> : ''
          }
          { country === 'MYS' ?
            <div className="address-box">
              <a href="https://ohmyhome.com.my">{OhmyhomeLogo}</a>
              <div className="custom-bold">Malaysia</div>
              <p>Bumi, No. 8-1, Jalan Jalil 1, 217, Bukit Jalil, 57000 Puchong, Federal Territory of Kuala Lumpur, Malaysia</p>
              <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:60192665593">+60 19-266 5593</a> </p>
              <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:help@ohmyhome.com.my">help@ohmyhome.com.my</a> </p>
            </div> : ''
          }
          { country === 'PHL' ?
            <div className="address-box">
              <a href="https://ohmyhome.com/en-ph/">{OhmyhomeLogo}</a>
              <div className="custom-bold">Philippines</div>
              <p>4th Floor, Billions Building (PGHI Hotel), 1235 E. Rodriguez Ave. New Manila, Quezon City</p>
              <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:639177006497">+63 917 700-6497</a> </p>
              <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:hi@ohmyhome.com">hi@ohmyhome.com</a> </p>
            </div> : ''
          }
        </div>
        <div>
          { country === 'SGP' ?
            <React.Fragment> 
              <div className="menu-header"><a href="https://omh.sg/about-us">About us</a></div>
              <ul className="menu-wrapper">
                <li><a href="https://ohmyhome.com/en-sg/about-us">Company</a></li>   
                <li><a href="https://omh.sg/careers">Careers</a></li>
                <li><a href="https://omh.sg/about-us/news">Press</a></li>
                <li><a href="https://omh.sg/contact-us">Contact us</a></li>
                <li><a href="https://omh.sg/blog">Blog</a></li>
              </ul>
            </React.Fragment> : ''
          }
          { country === 'MYS' ?
            <React.Fragment> 
              <div className="menu-header"><a href="https://ohmyhome.com.my/about-us">About us</a></div>
              <ul className="menu-wrapper">
                <li><a href="https://ohmyhome.com.my/about-us">Company</a></li>   
                <li><a href="https://ohmyhome.com.my/careers">Careers</a></li>
                <li><a href="https://ohmyhome.com.my/about-us/news">Press</a></li>
                <li><a href="https://ohmyhome.com.my/contact-us">Contact us</a></li>
                <li><a href="https://ohmyhome.com.my/blog">Blog</a></li>
              </ul>
            </React.Fragment> : ''
          }
          { country === 'PHL' ?
            <React.Fragment> 
              <div className="menu-header"><a href="https://ohmyhome.com/en-ph/about-us">About us</a></div>
              <ul className="menu-wrapper">
                <li><a href="https://ohmyhome.com/en-ph/about-us">Company</a></li>   
                <li><a href="https://omh.sg/careers">Careers</a></li>
                <li><a href="https://omh.sg/about-us/news">Press</a></li>
                <li><a href="https://omh.sg/contact-us">Contact us</a></li>
                <li><a href="https://omh.sg/blog">Blog</a></li>
              </ul>
            </React.Fragment> : ''
          }
        </div>
        <div>
          { country === 'SGP' ?
            <React.Fragment> 
              <div className="menu-header"><a href="https://omh.sg/faqs">Guides</a></div>
              <ul className="menu-wrapper">
                <li><a href="https://omh.sg/about-us">HDB Process</a></li>   
                <li><a href="https://omh.sg/landlords-guide">Landlords</a></li>
                <li><a href="https://omh.sg/buyers-guide">Buyers</a></li>
                <li><a href="https://omh.sg/sellers-guide">Sellers</a></li>
              </ul>
            </React.Fragment> : ''
          }
          { country === 'MYS' ?
            <React.Fragment> 
              <div className="menu-header"><a href="https://ohmyhome.com.my/blog">Blogs</a></div>
              <ul className="menu-wrapper">
                <li><a href="https://ohmyhome.com.my/blog/chinese">Chinse</a></li>   
                <li><a href="https://ohmyhome.com.my/blog">English</a></li>
                <li><a href="https://ohmyhome.com.my/faqs">FaQs</a></li>
              </ul>
            </React.Fragment> : ''
          }
          { country === 'PHL' ?
            <React.Fragment> 
              <div className="menu-header">Buy a home</div>
              <ul className="menu-wrapper">
                <li><a href="https://ohmyhome.com/en-ph/property-investments">New Properties</a></li>
                <li><a href="https://ohmyhome.com/en-ph/resale-properties">Resale Properties</a></li>
                {/* <li><a href="#">SMDC Properties</a></li>
                <li><a href="#">Ayala Properties</a></li>
                <li><a href="#">Makati Properties</a></li> */}
              </ul>
            </React.Fragment> : ''
          }
        </div>
        <div>
          <div className="menu-header">Countries</div>
          <ul className="menu-wrapper">
            <li><a href="https://omh.sg/">Singapore</a></li>
            <li><a href="https://ohmyhome.com.my/">Malaysia</a></li>
            <li><a href="https://ohmyhome.com/en-ph/">Philippines</a></li>
          </ul>
        </div>
        <div>
          <div className="download-app_wrapper">
            <div className="title-download">Download the app <MediaQuery minDeviceWidth={1025}><span>{twirly}</span></MediaQuery></div>
            <div className="download-platform">
              
              <amp-img 
                src="https://api.omh.app/store/cms/media/philippines/resale-properties/content/Ohmyhome-Philippines-google.png" 
                alt="Android" 
                width="152" 
                height="45"
                layout="responsive"> 
              </amp-img>           
              <a href="https://play.google.com/store/apps/details?id=com.ohmyhome.sg&amp;hl=en" target="_blank" className="block"><span className="hidden">Android</span></a>
            </div>
            <div className="download-platform">
            
              <amp-img 
                src="https://api.omh.app/store/cms/media/philippines/resale-properties/content/Ohmyhome-Philippines-apple.png" 
                alt="IOS" 
                width="152" 
                height="45"
                layout="responsive"> 
              </amp-img>           
              <a href="https://apps.apple.com/sg/app/ohmyhome/id1112125972" target="_blank" className="block"><span className="hidden">IOS</span></a>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        { country === 'SGP' ? 
            <React.Fragment> 
              <div className="footer-copyright">
                © 2020 Ohmyhome  •  <a href="https://omh.sg/privacy-policy">Policies</a>   •  <a href="https://omh.sg/terms-of-use">Terms</a> 
              </div>
              <div className="social-footer">
                Follow us on &nbsp; &nbsp;
                <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block pr-10">{instagram}</a>
                <a href="https://www.facebook.com/ohmyhomeph/" target="_blank" id="pl-fb" className="block pr-10">{facebook}</a>            
              </div>
            </React.Fragment> : ''
        }
        { country === 'MYS' ?
            <React.Fragment> 
              <div className="footer-copyright">
                © 2020 Ohmyhome  •  <a href="https://ohmyhome.com.my/privacy-policy">Policies</a>   •  <a href="https://ohmyhome.com.my/terms-of-use">Terms</a> 
              </div>
              <div className="social-footer">
                Follow us on &nbsp; &nbsp;
                <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block pr-10">{instagram}</a>
                <a href="https://www.facebook.com/ohmyhomemy/" target="_blank" id="pl-fb" className="block pr-10">{facebook}</a>     
              </div>
            </React.Fragment> : ''
        }
        { country === 'PHL' ?
            <React.Fragment> 
              <div className="footer-copyright">
                © 2020 Ohmyhome  •  <a href="https://omh.sg/privacy-policy">Policies</a>   •  <a href="https://omh.sg/terms-of-use">Terms</a> 
              </div>
              <div className="social-footer">
                Follow us on &nbsp; &nbsp;
                <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block pr-10">{instagram}</a>
                <a href="https://www.facebook.com/ohmyhomesg/" target="_blank" id="pl-fb" className="block pr-10">{facebook}</a>     
              </div>
            </React.Fragment> : ''
        }
      </div>
      <style jsx>{`
        .footer-wrapper {
          padding: 0 40px;
        }
        .footer-top {
          max-width: 1224px;
          padding: 50px 0 40px;
          border-bottom: 1px solid #E0E0E0;
          display: flex;
          justify-content: space-between;
          margin: 0 auto;
        }
        .address-box {
          max-width: 210px;
          padding: 0;
        }
        .footer-bottom {
          max-width: 1224px;
          padding: 19px 0 0;
          margin: 0 auto;
          display: flex;
          justify-content: space-between;
          vertical-align: middle;
          align-items: center;
        }
        .address-box div {
          font-size: 16.0024px;
          line-height: 16px;
          color: #484848;
          margin: 19px 0 18px;
        }
        p {
          font-size: 14.5px;
          line-height: 22px;
          color: #343434;
          margin: 0 0 18px;
        }
        a {
          text-decoration: none;
        }
        .menu-header {
          font-size: 16.0024px;
          line-height: 16px;
          color: #484848;
          margin: 0 0 18px;
          font-variation-settings: "wght" 580;
        }
        .menu-header a {
          color: #484848;
        }
        .menu-wrapper {
          list-style: none;
          margin: 0;
        }
        .menu-wrapper a {
          font-size: 15.0022px;
          line-height: 16px;
          color: #7E7E7E;
          margin: 0 0 18px;
          font-variation-settings: "wght" 520;
          display: block;
        }
        .download-app_wrapper {
          background: #FEF4EE;
          border-radius: 16px;
          padding: 16px;
          color: #E86225;        
        }
        .download-platform {
          margin: 0 0 10px;
          position: relative;
        }
        .download-platform a { 
          position: absolute;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
        }
        .footer-copyright {
          font-size: 14.5px;
          line-height: 22px;
          color: #5F5F5F;
        }
        .social-footer {
          font-size: 14.5px;
          line-height: 22px;
          color: #5F5F5F;
          display: flex;
          justify-content: flex-start;
          align-items: center;
          vertical-align: middle;
          font-variation-settings: "wght" 520;
        }
        .social-footer a {

        }
       
        @media screen and (min-width: 768px) {
          .footer-wrapper {
            padding: 0 16px;
          }
          .footer-top {
            flex-flow: row wrap;
          }
          .download-app_wrapper {
            display: flex;
            max-width: 352px;
            font-size: 14px;
            line-height: 14px;
          }
          .download-app_wrapper a {
            margin: 0 0 0 5px;
          }
          .title-download {
            margin: 0 17px 0 0;
            display: flex;
            justify-content: flex-start;
            font-variation-settings: "wght" 520;
            align-items: bottom;
            position: relative;
          }
          .footer-top > div {
            min-width: 117px;
            padding: 0 20px;
            display: block;
            margin: 10px 0 0;
            flex: 0;
          }
          .footer-top > div:first-child {
            padding: 0;
            margin: 0;
          }
          .footer-top > div:last-child {
            padding: 0;
            margin: 20px 0 0;
            flex: 1 100%;
            
          }
        }
        @media screen and (min-width: 835px) {
          .footer-top > div {
            flex: auto;
          }
        }
        @media screen and (min-width: 1025px) {
          
          .footer-top {
            flex-flow: initial;
          }
          .footer-top > div {
            min-width: 117px;
            padding: 0 20px;
            display: block;
            margin: 10px 0 0;
          }
          .download-app_wrapper {
            display: block;
            max-width: 184px;
            font-size: 22px;
            line-height: 22px;
            max-height: 210px;
          }
          .download-app_wrapper a {
            margin: 13px 0 0;
          }
          .title-download {
            margin: 0 0 20px;
            padding-right: 50px;
          }
          .title-download span {
            position: absolute;
            right: 40px;
            bottom: -20px;
          }
          .footer-top > div:last-child {
            padding: 0;
            margin: 0;
            flex: auto;
            display: flex;
            justify-content: flex-end;
          }
          .footer-top {
            padding-left: 16px;
            padding-right: 16px;
          }
          .footer-bottom {
            padding-left: 16px;
            padding-right: 16px;
          }
        }
        @media screen and (min-width: 1441px) {
          .footer-wrapper {
            padding-left: 0;
            padding-right: 0;
          }
          .footer-top {
            max-width: 1760px;
            width: 80%;
          }
          .footer-bottom {
            max-width: 1760px;
            width: 80%;
          }
        }
      `}</style>
    </div>
  )
}

export default DesktopFooter
