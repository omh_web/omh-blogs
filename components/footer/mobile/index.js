import React from 'react'
import { useRouter, withRouter } from "next/router"

function MobileFooter({country}) {
  const router = useRouter()
  const OhmyhomeLogo = (
    <div>
      <svg width="134" height="27" viewBox="0 0 134 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Buy Sell Rent Real Estate</title>
      <path fillRule="evenodd" clipRule="evenodd" d="M6.97308 21.6982C10.9217 21.6982 13.9462 18.6638 13.9462 14.4157C13.9462 10.0809 10.9217 7.13318 6.97308 7.13318C3.02447 7.13318 0 10.2543 0 14.4157C0 18.5771 3.02447 21.6982 6.97308 21.6982ZM6.97421 10.5133C8.73874 10.5133 10.3352 11.8137 10.3352 14.4146C10.3352 17.0155 8.73874 18.316 6.97421 18.316C5.20967 18.316 3.61319 17.0155 3.61319 14.4146C3.61319 11.8137 5.20967 10.5133 6.97421 10.5133Z" fill="#EE620F"/>
      <path d="M19.6595 21.2637H16.1309V0.976624H19.7435V8.60594C20.5836 7.56557 22.0958 7.04539 23.356 7.04539C26.7166 7.04539 28.3128 9.4729 28.3128 12.5073V21.177H24.7002V13.1142C24.7002 11.5536 23.9441 10.3399 22.2639 10.3399C20.7516 10.3399 19.8275 11.5536 19.7435 13.0275L19.6595 21.2637Z" fill="#EE620F"/>
      <path d="M31.1693 21.2645V7.47975H34.6138V9.12699C35.37 7.73984 37.0502 7.04626 38.4784 7.04626C40.3267 7.04626 41.755 7.82653 42.4271 9.30038C43.5192 7.65314 44.8634 7.04626 46.6277 7.04626C49.0641 7.04626 51.4165 8.60681 51.4165 12.2481V21.1778H47.9719V13.0283C47.9719 11.5545 47.2998 10.4274 45.6196 10.4274C44.1073 10.4274 43.1832 11.6412 43.1832 13.115V21.1778H39.6546V13.0283C39.6546 11.5545 38.8985 10.4274 37.3023 10.4274C35.706 10.4274 34.7819 11.6412 34.7819 13.115V21.1778H31.1693V21.2645Z" fill="#EE620F"/>
      <path d="M54.945 26.7264L58.0534 19.5305L52.4246 7.47968H56.4572L59.9857 15.5425L63.2622 7.47968H67.0428L58.7255 26.7264H54.945Z" fill="#EE620F"/>
      <path d="M72.1674 21.2637H68.5549V0.976624H72.1674V8.60594C73.0076 7.56557 74.5198 7.04539 75.78 7.04539C79.1405 7.04539 80.7368 9.4729 80.7368 12.5073V21.177H77.1242V13.1142C77.1242 11.5536 76.3681 10.3399 74.6878 10.3399C73.1756 10.3399 72.2514 11.5536 72.1674 13.0275V21.2637Z" fill="#EE620F"/>
      <path fillRule="evenodd" clipRule="evenodd" d="M89.6424 21.698C93.591 21.698 96.6155 18.6636 96.6155 14.4155C96.6155 10.0807 93.591 7.04628 89.6424 7.13298C85.6938 7.13298 82.6693 10.2541 82.6693 14.4155C82.6693 18.5769 85.6938 21.698 89.6424 21.698ZM89.6435 10.5132C91.4081 10.5132 93.0046 11.8137 93.0046 14.4146C93.0046 17.0155 91.4081 18.3159 89.6435 18.3159C87.879 18.3159 86.2825 17.0155 86.2825 14.4146C86.2825 11.8137 87.879 10.5132 89.6435 10.5132Z" fill="#EE620F"/>
      <path d="M98.7154 21.2645V7.47975H102.16V9.12699C102.916 7.73984 104.596 7.04626 106.025 7.04626C107.873 7.04626 109.301 7.82653 109.973 9.30038C111.065 7.65314 112.41 7.04626 114.174 7.04626C116.61 7.04626 118.963 8.60681 118.963 12.2481V21.1778H115.434V13.0283C115.434 11.5545 114.762 10.4274 113.082 10.4274C111.569 10.4274 110.645 11.6412 110.645 13.115V21.1778H107.117V13.0283C107.117 11.5545 106.361 10.4274 104.764 10.4274C103.168 10.4274 102.244 11.6412 102.244 13.115V21.1778H98.7154V21.2645Z" fill="#EE620F"/>
      <path fillRule="evenodd" clipRule="evenodd" d="M127.783 21.7849C131.06 21.7849 133.16 19.7042 133.832 17.3634L130.892 16.4097C130.471 17.6235 129.547 18.4905 127.867 18.4905C126.103 18.4905 124.591 17.19 124.507 15.3694H133.916C133.916 15.3694 134 14.7625 134 14.2423C134 9.82078 131.564 7.13318 127.447 7.13318C124.002 7.13318 120.894 9.99417 120.894 14.4157C120.894 19.0973 124.17 21.7849 127.783 21.7849ZM127.532 10.1665C129.549 10.1665 130.389 11.4672 130.473 12.8545H124.592C124.676 11.2937 126.02 10.0798 127.532 10.1665Z" fill="#EE620F"/>
      <path d="M2.68866 24.2984L9.4107 21.2636H4.95735L2.68866 24.2984Z" fill="#EE620F"/>
      <path d="M13.9468 4.18415L6.97375 0.802979L0.000671387 4.18415V7.65202L6.97375 4.27085L13.9468 7.65202V4.18415Z" fill="#EE620F"/>
      </svg>
    </div>
  )
  const phone = (
    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.6125 8.94451C10.4095 8.82551 10.16 8.82801 9.958 8.94801L8.935 9.55751C8.706 9.69401 8.42 9.67801 8.21 9.51401C7.847 9.23051 7.2625 8.75351 6.754 8.24501C6.2455 7.73651 5.7685 7.15201 5.485 6.78901C5.321 6.57901 5.305 6.29301 5.4415 6.06401L6.051 5.04101C6.1715 4.83901 6.1725 4.58751 6.0535 4.38451L4.5525 1.82051C4.407 1.57251 4.118 1.45051 3.8385 1.51901C3.567 1.58501 3.2145 1.74601 2.845 2.11601C1.688 3.27301 1.0735 5.22451 5.4245 9.57551C9.7755 13.9265 11.7265 13.3125 12.884 12.155C13.2545 11.7845 13.415 11.4315 13.4815 11.1595C13.549 10.8805 13.429 10.5935 13.1815 10.4485C12.5635 10.087 11.2305 9.30651 10.6125 8.94451Z" fill="#343434"/>
    </svg>
  )
  const email = (
    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2.5 2.5C1.80937 2.5 1.25 3.05937 1.25 3.75V11.25C1.25 11.9406 1.80937 12.5 2.5 12.5H12.5C13.1906 12.5 13.75 11.9406 13.75 11.25V3.75C13.75 3.05937 13.1906 2.5 12.5 2.5H2.5ZM2.5 3.75H12.5V4.37622L7.5 7.5L2.5 4.37622V3.75ZM2.5 5.62622L7.5 8.75L12.5 5.62622V11.25H2.5V5.62622Z" fill="#1C1C1C"/>
    </svg>
  )
  const facebook = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Facebook</title>
      <path d="M15.075 3.75C8.82051 3.75 3.75 8.82051 3.75 15.075C3.75 20.7529 7.93253 25.441 13.3824 26.26V18.0766H10.5805V15.0997H13.3824V13.1189C13.3824 9.83925 14.9803 8.39943 17.706 8.39943C19.0115 8.39943 19.7018 8.4962 20.0287 8.54047V11.139H18.1693C17.0121 11.139 16.608 12.236 16.608 13.4725V15.0997H19.9993L19.5391 18.0766H16.608V26.2842C22.1356 25.5342 26.4 20.808 26.4 15.075C26.4 8.82051 21.3295 3.75 15.075 3.75Z" fill="#3578EA"/>
    </svg>
  )
  const instagram = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Instagram</title>
      <path d="M21.0938 26.7188H8.90625C6.32812 26.7188 4.21875 24.6094 4.21875 22.0312V9.84375C4.21875 7.26562 6.32812 5.15625 8.90625 5.15625H21.0938C23.6719 5.15625 25.7812 7.26562 25.7812 9.84375V22.0312C25.7812 24.6094 23.6719 26.7188 21.0938 26.7188Z" fill="url(#paint0_radial)"/>
      <path d="M15 9.375C17.1562 9.375 17.3906 9.375 18.2344 9.42188C19.0313 9.46875 19.4531 9.60938 19.7344 9.70313C20.1094 9.84375 20.3906 10.0312 20.6719 10.3125C20.9531 10.5938 21.1406 10.875 21.2813 11.25C21.375 11.5313 21.5156 11.9531 21.5625 12.75C21.5625 13.5469 21.5625 13.7813 21.5625 15.9375C21.5625 18.0937 21.5625 18.3281 21.5156 19.1719C21.4688 19.9688 21.3281 20.3906 21.2344 20.6719C21.0937 21.0469 20.9063 21.3281 20.625 21.6094C20.3437 21.8906 20.0625 22.0781 19.6875 22.2188C19.4062 22.3125 18.9844 22.4531 18.1875 22.5C17.3906 22.5 17.1562 22.5 15 22.5C12.8438 22.5 12.6094 22.5 11.7656 22.4531C10.9688 22.4063 10.5469 22.2656 10.2656 22.1719C9.89062 22.0312 9.60937 21.8438 9.32812 21.5625C9.04687 21.2812 8.85938 21 8.71875 20.625C8.625 20.3437 8.48438 19.9219 8.4375 19.125C8.4375 18.3281 8.4375 18.0937 8.4375 15.9375C8.4375 13.7813 8.4375 13.5469 8.48438 12.7031C8.53125 11.9063 8.67188 11.4844 8.76563 11.2031C8.90625 10.8281 9.09375 10.5469 9.375 10.2656C9.65625 9.98437 9.9375 9.79688 10.3125 9.65625C10.5938 9.5625 11.0156 9.42188 11.8125 9.375C12.6094 9.375 12.8438 9.375 15 9.375ZM15 7.96875C12.8438 7.96875 12.5625 7.96875 11.7188 8.01563C10.875 8.0625 10.3125 8.20312 9.79687 8.39062C9.28125 8.57812 8.8125 8.85937 8.39062 9.32812C7.96875 9.79687 7.6875 10.2187 7.45312 10.7344C7.26562 11.25 7.125 11.8125 7.07813 12.6562C7.03125 13.5 7.03125 13.7813 7.03125 15.9375C7.03125 18.0937 7.03125 18.375 7.07813 19.2188C7.125 20.0625 7.26562 20.625 7.45312 21.1406C7.64062 21.6562 7.92187 22.125 8.39062 22.5469C8.8125 22.9687 9.28125 23.25 9.79687 23.4844C10.3125 23.6719 10.875 23.8125 11.7188 23.8594C12.5625 23.9063 12.8438 23.9062 15 23.9062C17.1562 23.9062 17.4375 23.9063 18.2812 23.8594C19.125 23.8125 19.6875 23.6719 20.2031 23.4844C20.7187 23.2969 21.1875 23.0156 21.6094 22.5469C22.0312 22.125 22.3125 21.6562 22.5469 21.1406C22.7344 20.625 22.875 20.0625 22.9219 19.2188C22.9688 18.375 22.9688 18.0937 22.9688 15.9375C22.9688 13.7813 22.9688 13.5 22.9219 12.6562C22.875 11.8125 22.7344 11.25 22.5469 10.7344C22.3594 10.2187 22.0781 9.75 21.6094 9.32812C21.1406 8.90625 20.7187 8.625 20.2031 8.39062C19.6875 8.20312 19.125 8.0625 18.2812 8.01563C17.4375 7.96875 17.1562 7.96875 15 7.96875Z" fill="white"/>
      <path d="M15 11.7188C12.6562 11.7188 10.7812 13.5938 10.7812 15.9375C10.7812 18.2812 12.6562 20.1562 15 20.1562C17.3438 20.1562 19.2188 18.2812 19.2188 15.9375C19.2188 13.5938 17.3438 11.7188 15 11.7188ZM15 18.75C13.4531 18.75 12.1875 17.4844 12.1875 15.9375C12.1875 14.3906 13.4531 13.125 15 13.125C16.5469 13.125 17.8125 14.3906 17.8125 15.9375C17.8125 17.4844 16.5469 18.75 15 18.75Z" fill="white"/>
      <path d="M19.2188 12.6562C19.7365 12.6562 20.1562 12.2365 20.1562 11.7188C20.1562 11.201 19.7365 10.7812 19.2188 10.7812C18.701 10.7812 18.2812 11.201 18.2812 11.7188C18.2812 12.2365 18.701 12.6562 19.2188 12.6562Z" fill="white"/>
      <defs>
      <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.67606 31.0761) rotate(-49.945) scale(32.6463 21.5657)">
      <stop offset="0.073" stopColor="#EACC7B"/>
      <stop offset="0.184" stopColor="#ECAA59"/>
      <stop offset="0.307" stopColor="#EF802E"/>
      <stop offset="0.358" stopColor="#EF6D3A"/>
      <stop offset="0.46" stopColor="#F04B50"/>
      <stop offset="0.516" stopColor="#F03E58"/>
      <stop offset="0.689" stopColor="#DB359E"/>
      <stop offset="0.724" stopColor="#CE37A4"/>
      <stop offset="0.789" stopColor="#AC3CB4"/>
      <stop offset="0.877" stopColor="#7544CF"/>
      <stop offset="0.98" stopColor="#2B4FF2"/>
      </radialGradient>
      </defs>
    </svg>
  )
  const youtube = (
    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Youtube</title>
      <path d="M27 21.1875C26.75 22.5 25.6875 23.5 24.375 23.6875C22.3125 24 18.875 24.375 15 24.375C11.1875 24.375 7.75 24 5.625 23.6875C4.3125 23.5 3.25 22.5 3 21.1875C2.75 19.75 2.5 17.625 2.5 15C2.5 12.375 2.75 10.25 3 8.8125C3.25 7.5 4.3125 6.5 5.625 6.3125C7.6875 6 11.125 5.625 15 5.625C18.875 5.625 22.25 6 24.375 6.3125C25.6875 6.5 26.75 7.5 27 8.8125C27.25 10.25 27.5625 12.375 27.5625 15C27.5 17.625 27.25 19.75 27 21.1875Z" fill="#FF3D00"/>
      <path d="M12.5 19.375V10.625L20 15L12.5 19.375Z" fill="white"/>
    </svg>
  )
  return (
      <div className="footer-wrapper">
        <div className="footer-top">
          { country === 'SGP' ?
              <React.Fragment> 
                <a href="https://omh.sg">{OhmyhomeLogo}</a>
                <div className="social-footer">
                  <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                  <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block">{instagram}</a>
                  <a href="https://www.facebook.com/ohmyhomeph/" target="_blank" id="pl-fb" className="block">{facebook}</a>   
                </div>
              </React.Fragment> : ''
          }
          { country === 'MYS' ?
              <React.Fragment> 
                <a href="https://ohmyhome.com.my">{OhmyhomeLogo}</a>
                <div className="social-footer">
                  <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                  <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block">{instagram}</a>
                  <a href="https://www.facebook.com/ohmyhomemy/" target="_blank" id="pl-fb" className="block">{facebook}</a>    
                </div>
              </React.Fragment> : ''
          }
          { country === 'PHL' ?
              <React.Fragment> 
                 <a href="https://ohmyhome.com/en-ph/">{OhmyhomeLogo}</a>
                <div className="social-footer">
                  <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank" className="block">{youtube}</a>
                  <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank" id="pl-insta" className="block">{instagram}</a>
                  <a href="https://www.facebook.com/ohmyhomesg/" target="_blank" id="pl-fb" className="block">{facebook}</a> 
                </div>
              </React.Fragment> : ''
          }
        </div>
        { country === 'SGP' ?
          <div className="address-box">
            <div className="custom-bold">Singapore</div>
            <p>11 Lorong 3 Toa Payoh, Jackson Square Proptech Innovation Centre Block B, #04-17, Singapore 319579</p>
            <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:6597272131">+65 9727 2131</a> </p>
            <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:help@ohmyhome.com">help@ohmyhome.com</a> </p>
          </div> : ''
        }
        { country === 'MYS' ?
          <div className="address-box">
            <div className="custom-bold">Malaysia</div>
            <p>Bumi, No. 8-1, Jalan Jalil 1, 217, Bukit Jalil, 57000 Puchong, Federal Territory of Kuala Lumpur, Malaysia</p>
            <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:60192665593">+60 19-266 5593</a> </p>
            <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:help@ohmyhome.com.my">help@ohmyhome.com.my</a> </p>
          </div> : ''
        }
        { country === 'PHL' ?
          <div className="address-box">
            <div className="custom-bold">Philippines</div>
            <p>4th Floor, Billions Building (PGHI Hotel), 1235 E. Rodriguez Ave. New Manila, Quezon City</p>
            <p className="flex m-0 align-middle items-center justify-start mb-10">{phone} &nbsp; <a href="tel:639177006497">+63 917 700-6497</a> </p>
            <p className="flex m-0 align-middle items-center justify-start">{email} &nbsp; <a href="mailto:hi@ohmyhome.com">hi@ohmyhome.com</a> </p>
          </div> : ''
        }
        <div className="row menu-wrapper">
          <div className="col">
            <div className="tabs">
              { country === 'SGP' ?
                <React.Fragment> 
                  <div className="tab">
                    <input type="checkbox" id="chck1" />
                    <label className="tab-label" for="chck1">About us</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://ohmyhome.com/en-sg/about-us">Company</a></li>   
                        <li><a href="https://omh.sg/careers">Careers</a></li>
                        <li><a href="https://omh.sg/about-us/news">Press</a></li>
                        <li><a href="https://omh.sg/contact-us">Contact us</a></li>
                        <li><a href="https://omh.sg/blog">Blog</a></li>
                      </ul>
                    </div>
                  </div> 
                  <div className="tab">
                    <input type="checkbox" id="chck2" />
                    <label className="tab-label" for="chck2">Guides</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://omh.sg/about-us">HDB Process</a></li>   
                        <li><a href="https://omh.sg/landlords-guide">Landlords</a></li>
                        <li><a href="https://omh.sg/buyers-guide">Buyers</a></li>
                        <li><a href="https://omh.sg/sellers-guide">Sellers</a></li>
                      </ul>
                    </div>
                  </div>
                  <div className="tab">
                    <input type="checkbox" id="chck3" />
                    <label className="tab-label" for="chck3">Blogs</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://omh.sg/blog/DIY-guides">HDB DIY</a></li>
                        <li><a href="https://omh.sg/blog/financial-planning">Financial Planning</a></li>
                        <li><a href="https://omh.sg/blog/integrated-housing-needs">Integrated Housing Needs</a></li>
                        <li><a href="https://omh.sg/blog/home-rental">Home Rental</a></li>
                      </ul>
                    </div>
                  </div>
                </React.Fragment> : ''
              }
              { country === 'MYS' ?
                <React.Fragment> 
                  <div className="tab">
                    <input type="checkbox" id="chck1" />
                    <label className="tab-label" for="chck1">About us</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://ohmyhome.com.my/about-us">Company</a></li>   
                        <li><a href="https://ohmyhome.com.my/careers">Careers</a></li>
                        <li><a href="https://ohmyhome.com.my/about-us/news">Press</a></li>
                        <li><a href="https://ohmyhome.com.my/contact-us">Contact us</a></li>
                        <li><a href="https://ohmyhome.com.my/blog">Blog</a></li>
                      </ul>
                    </div>
                  </div> 
                  <div className="tab">
                    <input type="checkbox" id="chck2" />
                    <label className="tab-label" for="chck2">Blogs</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://ohmyhome.com.my/blog/chinese">Chinse</a></li>   
                        <li><a href="https://ohmyhome.com.my/blog">English</a></li>
                        <li><a href="https://ohmyhome.com.my/faqs">FaQs</a></li>
                      </ul>
                    </div>
                  </div>
                </React.Fragment> : ''
              }
              { country === 'PHL' ?
                <React.Fragment> 
                  <div className="tab">
                    <input type="checkbox" id="chck1" />
                    <label className="tab-label" for="chck1">About us</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://ohmyhome.com/en-ph/about-us">Company</a></li>   
                        <li><a href="https://omh.sg/careers">Careers</a></li>
                        <li><a href="https://omh.sg/about-us/news">Press</a></li>
                        <li><a href="https://omh.sg/contact-us">Contact us</a></li>
                        <li><a href="https://omh.sg/blog">Blog</a></li>
                      </ul>
                    </div>
                  </div> 
                  <div className="tab">
                    <input type="checkbox" id="chck2" />
                    <label className="tab-label" for="chck2">Buy a home</label>
                    <div className="tab-content">
                      <ul>
                        <li><a href="https://ohmyhome.com/en-ph/property-investments">New Properties</a></li>
                        <li><a href="https://ohmyhome.com/en-ph/resale-properties">Resale Properties</a></li>
                      </ul>
                    </div>
                  </div>
                </React.Fragment> : ''
              }
              
              <div className="tab">
                <input type="checkbox" id="chck4" />
                <label className="tab-label" for="chck4">Countries</label>
                <div className="tab-content">
                  <ul>
                    <li><a className="block" href="https://omh.sg/">Singapore</a></li>
                    <li><a className="block" href="https://ohmyhome.com.my/">Malaysia</a></li>
                    <li><a className="block" href="https://ohmyhome.com/en-ph/">Philippines</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        { country === 'SGP' ?
            <div className="footer-copyright"> 
              © 2020 Ohmyhome  •  <a href="https://omh.sg/privacy-policy">Policies</a>   •  <a href="https://omh.sg/terms-of-use"> Terms </a> 
            </div> : ''
        }
        { country === 'MYS' ?
            <div className="footer-copyright"> 
              © 2020 Ohmyhome  •  <a href="https://ohmyhome.com.my/privacy-policy">Policies</a>   •  <a href="https://ohmyhome.com.my/terms-of-use"> Terms </a> 
            </div> : ''
        }
        { country === 'PHL' ?
            <div className="footer-copyright"> 
              © 2020 Ohmyhome  •  <a href="https://omh.sg/privacy-policy">Policies</a>   •  <a href="https://omh.sg/terms-of-use"> Terms </a> 
            </div> : ''
        }
        <div className="download-app_wrapper">
          <div className="title-download custom-bold">Download the app</div>
          <a href="https://play.google.com/store/apps/details?id=com.ohmyhome.sg&amp;hl=en" target="_blank" className="inline-block">
            <span className="hidden">Android</span>
            <amp-img 
              src="https://api.omh.app/store/cms/media/philippines/resale-properties/content/Ohmyhome-Philippines-google.png" 
              alt="Android" 
              width="152" 
              height="45"
              layout="fixed">  
            </amp-img>           
          </a>
          <a href="https://apps.apple.com/sg/app/ohmyhome/id1112125972" target="_blank" className="inline-block">
            <span className="hidden">IOS</span>
            <amp-img 
              src="https://api.omh.app/store/cms/media/philippines/resale-properties/content/Ohmyhome-Philippines-apple.png" 
              alt="IOS" 
              width="152" 
              height="45"
              layout="fixed"> 
            </amp-img>  
          </a>
        </div>
      <style jsx>{`
        .footer-wrapper {
          padding: 20px 0 0;
          background: #FFF;
        }
        .footer-top {
          margin: 0 28px 23px;
          display: flex;
          align-items: center;
          justify-content: sapce-between;
          vertical-align: middle;
        }
        .address-box {
          margin: 0px 16px 23px;
          border-bottom: 1px solid #E0E0E0;
          padding: 0 12px 24px;
        }
        .menu-wrapper {
          margin: 0px 16px 23px;
          border-bottom: 1px solid #E0E0E0;
          padding: 0 12px 8px;
        }
        .footer-copyright {
          margin: 0 28px 16px;
          font-size: 11px;
          line-height: 16px;
          color: #5F5F5F;
        }
        h6 {
          font-size: 14px;
          line-height: 16px;
          color: #1C1C1C;
          margin: 0 0 8px;
        }
        p {
          font-size: 13px;
          line-height: 140%;
          color: #343434;
          margin: 0 0 16px;
        }
        .address-box p {
          display: flex;
          vertical-align: middle;
          align-items: center;
          justify-content: flex-start;
          margin: 0 0 10px;
        }
        .phone {
          font-size: 13px;
          line-height: 15px;
          color: #343434;
        }
        .title-download {
          margin: 0 0 17px;
          text-align: center;
        }
        .social-footer {
          display: flex;
          justify-content: flex-start;
          align-items: center;
          vertical-align: middle;
          margin-left: 10px;
        }
        .social-footer a {
          display: block;
        }
        .download-app_wrapper {
          background: #FEF4EE;
          padding: 16px 28px 16px;
          color: #E86225;    
          text-align: center;
          font-size: 16px;
          line-height: 16px;    
          align-items: center;
        }
        .download-app_wrapper a {
          margin: 0 0 13px;
          display: block;
        }
        .download-app_wrapper img {
          max-width: 108px;   
          margin: 0 auto;
        }
        input {
          position: absolute;
          opacity: 0;
          z-index: 1;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          display: block;
          width: 100%;
          height: 20px;
        }
        
        .row {
          display: -webkit-box;
          display: flex;
        }
        .row .col {
          -webkit-box-flex: 1;
                  flex: 1;
        }
        .tab-content ul {
          list-style: none;
          margin: 0;
          padding: 0;
        }
        .tab {
          width: 100%;
          color: white;
          overflow: hidden;
          position: relative;
        }
        .tab-label {
          font-size: 13px;
          line-height: 14px;
          color: #1C1C1C;
          display: flex;
          justify-content: space-between;
          padding: 0 0 16px;
          position: relative;
        }
        
        .tab-label::before, .tab-label::after {
          position: absolute;
          display: block;
          content: "";
          border: 4px solid transparent;
          transition: all .35s;
          right: 0;
        }
        .tab-label::after {
          top: -2px;
          border-top-color: #fff;
        }
        .tab-label::before {
          top: 0px;
          border-top-color: #A8A8A8;
        }
        .tab-content {
          max-height: 0;
          padding: 0 1em;
          background: white;
          -webkit-transition: all .35s;
          -webkit-transition: all .35s;
          transition: all .35s;
          font-size: 12px;
          line-height: 16px;
          color: #5F5F5F;
          opacity: 0;
        }
        .tab-close {
          display: -webkit-box;
          display: flex;
          -webkit-box-pack: end;
                  justify-content: flex-end;
          padding: 1em;
          font-size: 0.75em;
          background: #2c3e50;
          cursor: pointer;
        }
        .tab-close:hover {
          background: #1a252f;
        }
        input:checked + .tab-label::after, input:checked + .tab-label::before {
          -webkit-transform: rotate(180deg);
                  transform: rotate(180deg);
        }
        input:checked + .tab-label::after {
          top: 2px;
          border-top-color: #FFF;
        }
        input:checked ~ .tab-content {
          max-height: 100vh;
          padding: 0 16px 16px;
          opacity: 1;
        }
        .tab:last-child input:checked ~ .tab-content {
          padding: 0 16px 0;
        }
        .menu-wrapper a {
          font-size: 13px;
          line-height: 17px;
          color: #5F5F5F;
          margin: 0 0 8px;
        }
        .menu-wrapper .tab-content ul li:last-child a {
          margin: 0;
        }
       

        @media screen and (min-width: 480px) {
          .download-app_wrapper {
            display: flex;
          }
          .title-download {
            margin: 0 17px 0 0;
            text-align: left;
          }
          .download-app_wrapper a {
            margin: 0 13px 0 0;
          }
        }
      `}</style>
    </div>
  )
}

export default MobileFooter
