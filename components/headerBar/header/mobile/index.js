import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"
import CountryPicker from '../../countryPicker'

import {
  BrowserView,
  MobileView,
  isAndroid,
  isBrowser,
  isIOS,
  CustomView
} from "react-device-detect"


function Header({post, info, rootUrl}) {

  const Phone = (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1" width="26" height="26" rx="3.54545" stroke="#EE620F" />
      <path d="M10.0001 13.1035C11.0716 15.2094 12.7942 16.932 14.9038 18.0035L16.5408 16.3627C16.7455 16.1581 17.0394 16.0986 17.2961 16.1804C18.1295 16.4557 19.0262 16.6046 19.9526 16.6046C20.3655 16.6046 20.6967 16.9357 20.6967 17.3487V19.9531C20.6967 20.366 20.3655 20.6972 19.9526 20.6972C12.9654 20.6972 7.30273 15.0345 7.30273 8.04733C7.30273 7.63435 7.63758 7.30322 8.04684 7.30322H10.6512C11.0642 7.30322 11.3953 7.63435 11.3953 8.04733C11.3953 8.97374 11.5441 9.87039 11.8195 10.7038C11.9013 10.9605 11.8418 11.2544 11.6372 11.4591L10.0001 13.1035Z" fill="#EE620F" />
    </svg>
  )
  const Menu = (
    <svg width="30" height="20" viewBox="0 0 30 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 2H42" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 10H43" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 18H43" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
    </svg>
  )
  const Logo = (
    <svg width="112" height="22" viewBox="0 0 112 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Buy Sell Rent Real Estate</title>
      <path fillRule="evenodd" clipRule="evenodd" d="M5.82822 17.4561C9.12854 17.4561 11.6564 14.921 11.6564 11.3718C11.6564 7.75027 9.12854 5.2876 5.82822 5.2876C2.5279 5.2876 0 7.89513 0 11.3718C0 14.8486 2.5279 17.4561 5.82822 17.4561ZM5.8282 8.11227C7.30281 8.11227 8.63698 9.19875 8.63698 11.3717C8.63698 13.5446 7.30281 14.6311 5.8282 14.6311C4.35359 14.6311 3.01942 13.5446 3.01942 11.3717C3.01942 9.19875 4.35359 8.11227 5.8282 8.11227Z" fill="#EE620F" />
      <path d="M16.4312 17.094H13.4819V0.14502H16.5014V6.51899C17.2036 5.64981 18.4675 5.21522 19.5208 5.21522C22.3296 5.21522 23.6638 7.24331 23.6638 9.77841V17.0216H20.6443V10.2854C20.6443 8.98166 20.0123 7.96762 18.608 7.96762C17.344 7.96762 16.5716 8.98166 16.5014 10.213L16.4312 17.094Z" fill="#EE620F" />
      <path d="M26.0513 17.0941V5.57749H28.9303V6.95369C29.5622 5.79478 30.9666 5.21533 32.1604 5.21533C33.7052 5.21533 34.8989 5.86722 35.4607 7.09855C36.3735 5.72235 37.497 5.21533 38.9717 5.21533C41.008 5.21533 42.9742 6.5191 42.9742 9.56122V17.0217H40.0952V10.2131C40.0952 8.98177 39.5334 8.04016 38.129 8.04016C36.8651 8.04016 36.0927 9.0542 36.0927 10.2855V17.0217H33.1434V10.2131C33.1434 8.98177 32.5115 8.04016 31.1773 8.04016C29.8431 8.04016 29.0707 9.0542 29.0707 10.2855V17.0217H26.0513V17.0941Z" fill="#EE620F" />
      <path d="M45.9235 21.6569L48.5216 15.6451L43.8169 5.57715H47.1874L50.1366 12.3133L52.8752 5.57715H56.0351L49.0834 21.6569H45.9235Z" fill="#EE620F "/>
      <path d="M60.3188 17.094H57.2993V0.14502H60.3188V6.51899C61.0209 5.64981 62.2849 5.21522 63.3382 5.21522C66.147 5.21522 67.4811 7.24331 67.4811 9.77841V17.0216H64.4617V10.2854C64.4617 8.98166 63.8297 7.96762 62.4253 7.96762C61.1614 7.96762 60.389 8.98166 60.3188 10.213V17.094Z" fill="#EE620F" />
      <path fillRule="evenodd" clipRule="evenodd" d="M74.9244 17.4561C78.2247 17.4561 80.7526 14.921 80.7526 11.3719C80.7526 7.75032 78.2247 5.21521 74.9244 5.28765C71.6241 5.28765 69.0962 7.89518 69.0962 11.3719C69.0962 14.8486 71.6241 17.4561 74.9244 17.4561ZM74.9244 8.11227C76.3991 8.11227 77.7332 9.19875 77.7332 11.3717C77.7332 13.5446 76.3991 14.6311 74.9244 14.6311C73.4498 14.6311 72.1157 13.5446 72.1157 11.3717C72.1157 9.19875 73.4498 8.11227 74.9244 8.11227Z" fill="#EE620F" />
      <path d="M82.5078 17.0941V5.57749H85.3868V6.95369C86.0188 5.79478 87.4232 5.21533 88.6169 5.21533C90.1617 5.21533 91.3555 5.86722 91.9172 7.09855C92.8301 5.72235 93.9536 5.21533 95.4282 5.21533C97.4646 5.21533 99.4307 6.5191 99.4307 9.56122V17.0217H96.4815V10.2131C96.4815 8.98177 95.9197 8.04016 94.5153 8.04016C93.2514 8.04016 92.479 9.0542 92.479 10.2855V17.0217H89.5298V10.2131C89.5298 8.98177 88.8978 8.04016 87.5636 8.04016C86.2294 8.04016 85.457 9.0542 85.457 10.2855V17.0217H82.5078V17.0941Z" fill="#EE620F" />
      <path fillRule="evenodd" clipRule="evenodd" d="M106.804 17.5285C109.542 17.5285 111.298 15.7902 111.86 13.8345L109.402 13.0378C109.051 14.0518 108.278 14.7761 106.874 14.7761C105.4 14.7761 104.136 13.6897 104.065 12.1686H111.93C111.93 12.1686 112 11.6616 112 11.227C112 7.53297 109.964 5.2876 106.523 5.2876C103.644 5.2876 101.046 7.67784 101.046 11.3718C101.046 15.2831 103.784 17.5285 106.804 17.5285ZM106.593 7.82237C108.279 7.82237 108.981 8.90884 109.051 10.0677H104.136C104.206 8.76398 105.329 7.74993 106.593 7.82237Z" fill="#EE620F" />
      <path d="M2.24707 19.6291L7.86463 17.094H4.143L2.24707 19.6291Z" fill="#EE620F" />
      <path d="M11.6564 2.82483L5.82822 0L0 2.82483V5.72209L5.82822 2.89726L11.6564 5.72209V2.82483Z" fill="#EE620F" />
    </svg>
  )
  const LogoMobile = (
    <svg width="136" height="35" viewBox="0 0 136 35" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.25436 20.9571C11.2148 20.9571 14.2483 17.9136 14.2483 13.6527C14.2483 9.30491 11.2148 6.34839 7.25436 6.34839C3.29391 6.34839 0.260376 9.47883 0.260376 13.6527C0.260376 17.8267 3.29391 20.9571 7.25436 20.9571ZM7.25549 9.73949C9.02531 9.73949 10.6266 11.0438 10.6266 13.6525C10.6266 16.2612 9.02531 17.5656 7.25549 17.5656C5.48567 17.5656 3.8844 16.2612 3.8844 13.6525C3.8844 11.0438 5.48567 9.73949 7.25549 9.73949Z" fill="#EE620F"/>
    <path d="M19.9767 20.5222H16.4376V0.174316H20.061V7.82649C20.9037 6.78302 22.4204 6.26128 23.6844 6.26128C27.055 6.26128 28.656 8.69606 28.656 11.7395V20.4352H25.0326V12.3482C25.0326 10.783 24.2743 9.56563 22.589 9.56563C21.0722 9.56563 20.1453 10.783 20.061 12.2613L19.9767 20.5222Z" fill="#EE620F"/>
    <path d="M31.5214 20.5231V6.69699H34.9762V8.34916C35.7346 6.95786 37.4199 6.26221 38.8524 6.26221C40.7062 6.26221 42.1387 7.04482 42.8129 8.52308C43.9083 6.8709 45.2565 6.26221 47.0261 6.26221C49.4698 6.26221 51.8292 7.82743 51.8292 11.4796V20.4361H48.3743V12.2622C48.3743 10.7839 47.7002 9.65351 46.0149 9.65351C44.4982 9.65351 43.5712 10.8709 43.5712 12.3492V20.4361H40.0321V12.2622C40.0321 10.7839 39.2737 9.65351 37.6727 9.65351C36.0717 9.65351 35.1448 10.8709 35.1448 12.3492V20.4361H31.5214V20.5231Z" fill="#EE620F"/>
    <path d="M55.37 26.0004L58.4878 18.783L52.842 6.69604H56.8868L60.4259 14.783L63.7122 6.69604H67.5041L59.1619 26.0004H55.37Z" fill="#EE620F"/>
    <path d="M72.6441 20.5222H69.0208V0.174316H72.6441V7.82649C73.4868 6.78302 75.0036 6.26128 76.2675 6.26128C79.6381 6.26128 81.2392 8.69606 81.2392 11.7395V20.4352H77.6158V12.3482C77.6158 10.783 76.8574 9.56563 75.1721 9.56563C73.6553 9.56563 72.7284 10.783 72.6441 12.2613V20.5222Z" fill="#EE620F"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M90.1715 20.957C94.1319 20.957 97.1654 17.9135 97.1654 13.6526C97.1654 9.30478 94.1319 6.2613 90.1715 6.34825C86.211 6.34825 83.1775 9.47869 83.1775 13.6526C83.1775 17.8265 86.211 20.957 90.1715 20.957ZM90.1736 9.73999C91.9434 9.73999 93.5447 11.0443 93.5447 13.653C93.5447 16.2617 91.9434 17.5661 90.1736 17.5661C88.4038 17.5661 86.8025 16.2617 86.8025 13.653C86.8025 11.0443 88.4038 9.73999 90.1736 9.73999Z" fill="#EE620F"/>
    <path d="M99.2732 20.5231V6.69699H102.728V8.34916C103.486 6.95786 105.172 6.26221 106.604 6.26221C108.458 6.26221 109.891 7.04482 110.565 8.52308C111.66 6.8709 113.008 6.26221 114.778 6.26221C117.222 6.26221 119.581 7.82743 119.581 11.4796V20.4361H116.042V12.2622C116.042 10.7839 115.368 9.65351 113.682 9.65351C112.166 9.65351 111.239 10.8709 111.239 12.3492V20.4361H107.7V12.2622C107.7 10.7839 106.941 9.65351 105.34 9.65351C103.739 9.65351 102.812 10.8709 102.812 12.3492V20.4361H99.2732V20.5231Z" fill="#EE620F"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M128.427 21.0441C131.713 21.0441 133.819 18.9571 134.494 16.6093L131.544 15.6527C131.123 16.8701 130.196 17.7397 128.511 17.7397C126.741 17.7397 125.224 16.4354 125.14 14.6093H134.578C134.578 14.6093 134.662 14.0006 134.662 13.4788C134.662 9.04404 132.218 6.34839 128.089 6.34839C124.635 6.34839 121.517 9.21796 121.517 13.6527C121.517 18.3484 124.803 21.0441 128.427 21.0441ZM128.174 9.3922C130.196 9.3922 131.039 10.6967 131.123 12.0883H125.224C125.308 10.5228 126.657 9.30523 128.174 9.3922Z" fill="#EE620F"/>
    <path d="M2.95569 23.5659L9.69788 20.522H5.23118L2.95569 23.5659Z" fill="#EE620F"/>
    <path d="M14.2491 3.39155L7.25509 0.000244141L0.261108 3.39155V6.86982L7.25509 3.47851L14.2491 6.86982V3.39155Z" fill="#EE620F"/>
    <path d="M73.486 26.6977V26.9586C73.486 27.0456 73.4017 27.2195 73.2331 27.2195H71.7162V32.0898C71.7162 32.1768 71.6319 32.3507 71.4633 32.3507H71.2105C71.1262 32.3507 70.9577 32.2637 70.9577 32.0898V27.2195H69.4407C69.3564 27.2195 69.1879 27.1325 69.1879 26.9586V26.6977C69.1879 26.6107 69.2721 26.4368 69.4407 26.4368H73.2331C73.4017 26.4368 73.486 26.6107 73.486 26.6977Z" fill="#151515"/>
    <path d="M78.0382 32.3493H77.7011C77.6168 32.3493 77.4483 32.2623 77.4483 32.0884V29.9141C77.4483 29.0444 77.0269 28.6096 76.3527 28.6096C75.6785 28.6096 75.2571 29.0444 75.2571 29.9141V32.0884C75.2571 32.1753 75.1728 32.3493 75.0042 32.3493H74.6671C74.5829 32.3493 74.4143 32.2623 74.4143 32.0884V26.6093C74.4143 26.5223 74.4986 26.3484 74.6671 26.3484H75.0042C75.0885 26.3484 75.2571 26.4354 75.2571 26.6093V28.4357C75.4256 28.1747 75.847 27.8269 76.5212 27.8269C77.5326 27.8269 78.2911 28.5226 78.2911 29.7402V32.0884C78.2911 32.2623 78.2068 32.3493 78.0382 32.3493Z" fill="#151515"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M82.2499 29.4793V29.5663L80.9857 29.6533C80.0587 29.8272 79.3002 30.262 79.3002 31.1317C79.3002 31.8275 79.8901 32.3493 80.7329 32.3493C81.2386 32.3493 81.997 32.0884 82.1656 31.6536V32.0014C82.1656 32.1754 82.3342 32.2623 82.4184 32.2623H82.7555C82.9241 32.2623 83.0084 32.0884 83.0084 32.0014V29.4793C83.0084 28.4357 82.3342 27.8269 81.3228 27.8269C80.4801 27.8269 79.7216 28.3487 79.7216 28.8705C79.7216 29.0445 79.8901 29.1314 79.9744 29.1314H80.2272C80.3115 29.1314 80.3958 29.1314 80.4801 29.0445C80.6486 28.7836 80.8172 28.6096 81.3228 28.6096C81.8285 28.6096 82.2499 28.8705 82.2499 29.4793ZM81.3216 30.4357L82.2486 30.2617H82.3329V30.5226C82.3329 31.2184 81.7429 31.7402 80.9844 31.7402C80.3945 31.7402 80.226 31.4793 80.226 31.2184C80.226 30.7835 80.6473 30.5226 81.3216 30.4357Z" fill="#151515"/>
    <path d="M86.2111 31.6535H86.6325C86.7168 31.6535 86.8854 31.7405 86.8854 31.9144V32.1753C86.8854 32.2623 86.8011 32.4363 86.6325 32.4363H86.1269C85.1998 32.4363 84.7784 31.9144 84.7784 30.9578V28.7835H84.1885C84.1042 28.7835 83.9357 28.6966 83.9357 28.5226V28.2617C83.9357 28.1747 84.0199 28.0008 84.1885 28.0008H84.7784V26.6093C84.7784 26.5223 84.8627 26.3484 85.0313 26.3484H85.3684C85.4527 26.3484 85.6212 26.4354 85.6212 26.6093V28.0008H86.6325C86.7168 28.0008 86.8854 28.0878 86.8854 28.2617V28.5226C86.8854 28.6096 86.8011 28.7835 86.6325 28.7835H85.6212V30.9578C85.4527 31.3056 85.6212 31.6535 86.2111 31.6535Z" fill="#151515"/>
    <path d="M94.9737 28.1747C94.9737 28.0877 95.0579 28.0007 95.2265 28.0007H95.4793C95.5636 28.0007 95.6479 28.0877 95.6479 28.1747C95.6479 28.1747 95.6479 28.1747 95.6479 28.2616L94.3837 32.0883C94.3837 32.2622 94.2994 32.3492 94.1309 32.3492H93.9623C93.7938 32.3492 93.7938 32.2622 93.7095 32.0883L92.7825 29.3053L91.8554 32.0883C91.7711 32.1753 91.7711 32.3492 91.6026 32.3492H91.3497C91.1812 32.3492 91.1812 32.2622 91.0969 32.0883L89.8328 28.2616V28.1747C89.8328 28.0877 89.917 28.0007 90.0013 28.0007H90.2541C90.4227 28.0007 90.507 28.0877 90.507 28.1747L91.434 30.9577L92.4453 28.1747C92.4453 28.0877 92.5296 28.0007 92.6982 28.0007H92.8667C93.0353 28.0007 93.1196 28.0877 93.1196 28.1747L94.1309 30.9577L94.9737 28.1747Z" fill="#151515"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M99.3562 29.4793V29.5663L98.092 29.6533C97.165 29.8272 96.4065 30.262 96.4065 31.1317C96.4065 31.8275 96.9964 32.3493 97.8392 32.3493C98.3449 32.3493 99.1034 32.0884 99.2719 31.6536V32.0014C99.2719 32.1754 99.4405 32.2623 99.5248 32.2623H99.8619C100.03 32.2623 100.115 32.0884 100.115 32.0014V29.4793C100.115 28.4357 99.4405 27.8269 98.4292 27.8269C97.5864 27.8269 96.8279 28.3487 96.8279 28.8705C96.8279 29.0445 96.9964 29.1314 97.0807 29.1314H97.3335C97.4178 29.1314 97.5021 29.1314 97.5864 29.0445C97.7549 28.7836 97.9235 28.6096 98.4292 28.6096C98.9348 28.6096 99.3562 28.8705 99.3562 29.4793ZM98.4279 30.4357L99.3549 30.2617H99.4392V30.5226C99.4392 31.2184 98.8493 31.7402 98.0908 31.7402C97.5008 31.7402 97.3323 31.4793 97.3323 31.2184C97.3323 30.7835 97.7537 30.5226 98.4279 30.4357Z" fill="#151515"/>
    <path d="M101.377 29.1314C101.377 28.4356 101.883 27.9138 102.979 27.9138C104.074 27.9138 104.58 28.4356 104.58 28.8705C104.58 28.9575 104.496 29.1314 104.327 29.1314H104.074C103.99 29.1314 103.906 29.1314 103.822 29.0444C103.653 28.8705 103.4 28.6965 102.894 28.6965C102.389 28.6965 102.052 28.8705 102.052 29.2184C102.052 29.6532 102.389 29.6532 103.316 29.9141C104.327 30.175 104.664 30.6099 104.664 31.2187C104.664 31.9144 103.99 32.5232 102.979 32.5232C101.883 32.5232 101.293 31.9144 101.293 31.5665C101.293 31.4796 101.377 31.3056 101.546 31.3056H101.799C101.883 31.3056 101.967 31.3056 102.052 31.3926C102.22 31.6535 102.473 31.7405 102.979 31.7405C103.484 31.7405 103.906 31.4796 103.906 31.1317C103.906 30.6968 103.484 30.6099 102.557 30.4359C101.715 30.175 101.377 29.7402 101.377 29.1314Z" fill="#151515"/>
    <path d="M107.949 29.1314C107.949 28.4356 108.455 27.9138 109.551 27.9138C110.646 27.9138 111.152 28.4356 111.152 28.8705C111.152 28.9575 111.068 29.1314 110.899 29.1314H110.646C110.562 29.1314 110.478 29.1314 110.393 29.0444C110.225 28.8705 109.972 28.6965 109.466 28.6965C108.961 28.6965 108.624 28.8705 108.624 29.2184C108.624 29.6532 108.961 29.6532 109.888 29.9141C110.899 30.175 111.236 30.6099 111.236 31.2187C111.236 31.9144 110.562 32.5232 109.551 32.5232C108.455 32.5232 107.865 31.9144 107.865 31.5665C107.865 31.4796 107.949 31.3056 108.118 31.3056H108.371C108.455 31.3056 108.539 31.3056 108.624 31.3926C108.792 31.6535 109.045 31.7405 109.551 31.7405C110.056 31.7405 110.478 31.4796 110.478 31.1317C110.478 30.6968 110.056 30.6099 109.129 30.4359C108.286 30.175 107.949 29.7402 107.949 29.1314Z" fill="#151515"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M112.668 27.3051H113.09C113.174 27.3051 113.343 27.2181 113.343 27.0441V26.6093C113.343 26.4354 113.174 26.3484 113.09 26.3484H112.668C112.499 26.3484 112.415 26.5223 112.415 26.6093V27.0441C112.415 27.2181 112.584 27.3051 112.668 27.3051ZM112.668 32.35H113.005C113.09 32.35 113.258 32.263 113.258 32.0891V28.1754C113.258 28.0015 113.09 27.9145 113.005 27.9145H112.668C112.499 27.9145 112.415 28.0885 112.415 28.1754V32.0891C112.415 32.263 112.584 32.35 112.668 32.35Z" fill="#151515"/>
    <path d="M116.546 27.9138C117.22 27.9138 117.641 28.1747 117.894 28.6096C118.147 28.1747 118.484 27.9138 119.243 27.9138C120.338 27.9138 120.844 28.6096 120.844 29.8271V32.1753C120.844 32.2623 120.76 32.4362 120.591 32.4362H120.254C120.17 32.4362 120.001 32.3493 120.001 32.1753V29.9141C120.001 29.0444 119.664 28.6965 119.074 28.6965C118.484 28.6965 118.147 29.0444 118.147 29.9141V32.1753C118.147 32.2623 118.063 32.4362 117.894 32.4362H117.557C117.473 32.4362 117.304 32.3493 117.304 32.1753V29.9141C117.304 29.0444 116.967 28.6965 116.377 28.6965C115.787 28.6965 115.45 29.0444 115.45 29.9141V32.1753C115.45 32.2623 115.366 32.4362 115.197 32.4362H114.86C114.776 32.4362 114.607 32.3493 114.607 32.1753V28.2617C114.607 28.1747 114.692 28.0008 114.86 28.0008H115.197C115.282 28.0008 115.45 28.0878 115.45 28.2617V28.5226C115.619 28.1747 115.872 27.9138 116.546 27.9138Z" fill="#151515"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M122.61 34.0017C122.778 34.0017 122.863 33.8277 122.863 33.7408V31.8274C123.031 32.1753 123.453 32.4362 124.127 32.4362C125.138 32.4362 125.897 31.6535 125.981 30.4359V29.9141C125.897 28.6965 125.138 27.9138 124.127 27.9138C123.453 27.9138 123.115 28.2617 122.863 28.5226V28.2617C122.863 28.0878 122.694 28.0008 122.61 28.0008H122.273C122.104 28.0008 122.02 28.1747 122.02 28.2617V33.7408C122.02 33.9147 122.188 34.0017 122.273 34.0017H122.61ZM125.223 30.4364C125.223 31.2192 124.718 31.741 124.043 31.741C123.369 31.741 122.948 31.2192 122.864 30.6104V29.9146C122.864 29.1319 123.369 28.697 124.043 28.697C124.718 28.697 125.223 29.2189 125.223 30.0016V30.4364Z" fill="#151515"/>
    <path d="M127.754 32.3493H127.417C127.332 32.3493 127.164 32.2623 127.164 32.0884V26.6093C127.164 26.5223 127.248 26.3484 127.417 26.3484H127.754C127.838 26.3484 128.007 26.4354 128.007 26.6093V32.0884C128.007 32.2623 127.923 32.3493 127.754 32.3493Z" fill="#151515"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M132.975 30.2618V30.0009C132.975 28.6963 132.301 27.9136 131.037 28.0875C129.941 28.0875 129.267 28.8703 129.183 29.9139V30.6966C129.267 31.8272 129.941 32.6099 131.037 32.6099C132.217 32.6099 132.723 32.0012 132.807 31.5663C132.807 31.3924 132.638 31.3054 132.554 31.3054H132.301C132.217 31.3054 132.133 31.3054 132.048 31.3924C131.88 31.6533 131.627 31.8272 131.037 31.8272C130.447 31.8272 129.941 31.3924 129.941 30.6096V30.5227H132.723C132.891 30.5227 132.975 30.3487 132.975 30.2618ZM131.122 28.6092C131.88 28.6092 132.302 29.044 132.302 29.7398H130.026C130.026 29.044 130.532 28.6092 131.122 28.6092Z" fill="#151515"/>
    <path d="M135.085 31.5668V32.0886C135.085 32.1756 135 32.3495 134.832 32.3495H134.326C134.242 32.3495 134.073 32.2626 134.073 32.0886V31.5668C134.073 31.4798 134.157 31.3059 134.326 31.3059H134.832C135 31.3059 135.085 31.3929 135.085 31.5668Z" fill="#151515"/>
    </svg>

  )
  const MYS = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 10H46V37H2V10Z" fill="#D50000"/>
      <path d="M2 35.9999H46V37.9999H2V35.9999ZM2 31.9999H46V33.9999H2V31.9999ZM2 27.9999H46V29.9999H2V27.9999ZM2 23.9999H46V25.9999H2V23.9999ZM2 19.9999H46V21.9999H2V19.9999ZM2 15.9999H46V17.9999H2V15.9999ZM2 12.1429H46V13.9999H2V12.1429Z" fill="#ECEFF1"/>
      <path d="M2 10H28V26H2V10Z" fill="#311B92"/>
      <path d="M12.02 12.071C8.697 12.071 6 14.759 6 18.071C6 21.383 8.697 24.071 12.02 24.071C13.221 24.071 14.339 23.72 15.279 23.115C14.732 23.304 14.145 23.407 13.534 23.407C10.587 23.408 8 20.937 8 18C8 15.063 10.587 12.767 13.533 12.767C14.184 12.767 14.808 12.883 15.385 13.096C14.424 12.45 13.266 12.071 12.02 12.071Z" fill="#FFD600"/>
      <path d="M23.003 21.123L20.468 19.8L21.218 22.508L19.522 20.243L18.992 23L18.472 20.242L16.769 22.502L17.527 19.797L14.987 21.112L16.874 18.995L14 19.105L16.642 17.996L14.004 16.879L16.877 16.998L14.997 14.876L17.533 16.199L16.783 13.491L18.479 15.756L19.008 13L19.528 15.758L21.231 13.498L20.473 16.203L23.013 14.888L21.126 17.005L24 16.895L21.358 18.004L23.996 19.121L21.123 19.002L23.003 21.123Z" fill="#FFD600"/>
    </svg>
  )
  const PHL = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 24H46V39H2V24Z" fill="#FF3D00"/>
      <path d="M2 9H46V24H2V9Z" fill="#3F51B5"/>
      <path d="M2 9V39L25 24.002L2 9Z" fill="#ECEFF1"/>
      <path d="M4.5 13L4.963 13.988L6 14.146L5.25 14.916L5.428 16L4.5 15.488L3.572 16L3.75 14.916L3 14.146L4.037 13.988L4.5 13ZM4.787 32.072L5.25 33.061L6.287 33.218L5.537 33.988L5.715 35.072L4.787 34.561L3.859 35.072L4.037 33.988L3.287 33.218L4.324 33.061L4.787 32.072ZM19.463 22.324L19.926 23.313L20.963 23.47L20.213 24.24L20.391 25.324L19.463 24.813L18.535 25.324L18.713 24.24L17.963 23.47L19 23.313L19.463 22.324ZM10 21C9.20435 21 8.44129 21.3161 7.87868 21.8787C7.31607 22.4413 7 23.2044 7 24C7 24.7956 7.31607 25.5587 7.87868 26.1213C8.44129 26.6839 9.20435 27 10 27C10.7956 27 11.5587 26.6839 12.1213 26.1213C12.6839 25.5587 13 24.7956 13 24C13 23.2044 12.6839 22.4413 12.1213 21.8787C11.5587 21.3161 10.7956 21 10 21Z" fill="#FFA000"/>
      <path d="M11 29C11 29.553 10.553 30 10 30C9.447 30 9 29.553 9 29L10 27.002V21L9 19C9 18.447 9.447 18 10 18C10.553 18 11 18.447 11 19L10 21V27.002L11 29Z" fill="#FFA000"/>
      <path d="M15 23C15.553 23 16 23.447 16 24C16 24.553 15.553 25 15 25L13.002 24H7L5 25C4.447 25 4 24.553 4 24C4 23.447 4.447 23 5 23L7 24H13.002L15 23Z" fill="#FFA000"/>
      <path d="M5.75701 21.173C5.36501 20.781 5.36501 20.15 5.75701 19.758C6.14801 19.367 6.78001 19.367 7.17101 19.758L7.87601 21.878L12.12 26.122L14.241 26.829C14.632 27.22 14.632 27.852 14.241 28.243C13.85 28.634 13.218 28.634 12.827 28.243L12.12 26.122L7.87601 21.878L5.75701 21.173Z" fill="#FFA000"/>
      <path d="M12.828 19.758C13.22 19.366 13.851 19.366 14.243 19.758C14.634 20.149 14.634 20.781 14.243 21.172L12.123 21.877L7.87897 26.121L7.17197 28.242C6.78097 28.633 6.14897 28.633 5.75797 28.242C5.36697 27.851 5.36697 27.219 5.75797 26.828L7.87897 26.121L12.123 21.877L12.828 19.758Z" fill="#FFA000"/>
    </svg>
  )
  const SGP = (
    <svg width="32" height="32"viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 9H46V24H2V9Z" fill="#FF3D00"/>
      <path d="M2 24H46V39H2V24ZM13.621 21.688C13.023 21.893 12.385 22 11.716 22C8.564 22 6 19.543 6 16.5C6 13.457 8.564 11 11.716 11C12.385 11 13.023 11.107 13.621 11.312C10.733 11.409 8.432 13.701 8.432 16.5C8.432 19.299 10.733 21.59 13.621 21.688Z" fill="#ECEFF1"/>
      <path d="M12.427 16.6329L11.65 16.0599L10.874 16.6329L11.173 15.7089L10.394 15.1389L11.355 15.1409L11.65 14.2149L11.945 15.1409L12.906 15.1389L12.127 15.7089L12.427 16.6329ZM18.52 16.6329L17.743 16.0599L16.967 16.6329L17.266 15.7089L16.487 15.1389L17.448 15.1409L17.743 14.2149L18.038 15.1409L19 15.1379L18.221 15.7079L18.52 16.6329ZM15.477 14.4569L14.7 13.8839L13.924 14.4569L14.223 13.5329L13.444 12.9629L14.405 12.9649L14.7 12.0389L14.995 12.9649L15.956 12.9629L15.177 13.5329L15.477 14.4569ZM13.567 20.1139L12.79 19.5409L12.014 20.1139L12.313 19.1899L11.534 18.6199L12.495 18.6219L12.79 17.6959L13.085 18.6219L14.046 18.6199L13.267 19.1899L13.567 20.1139ZM17.38 20.1139L16.603 19.5409L15.827 20.1139L16.126 19.1899L15.347 18.6199L16.308 18.6219L16.603 17.6959L16.898 18.6219L17.859 18.6199L17.08 19.1899L17.38 20.1139Z" fill="#ECEFF1"/>
    </svg>
  )
  const close = (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14 2.33337C7.54831 2.33337 2.33331 7.54837 2.33331 14C2.33331 20.4517 7.54831 25.6667 14 25.6667C20.4516 25.6667 25.6666 20.4517 25.6666 14C25.6666 7.54837 20.4516 2.33337 14 2.33337ZM19.8333 18.1884L18.1883 19.8334L14 15.645L9.81165 19.8334L8.16665 18.1884L12.355 14L8.16665 9.81171L9.81165 8.16671L14 12.355L18.1883 8.16671L19.8333 9.81171L15.645 14L19.8333 18.1884Z" fill="#5F5F5F"/>
    </svg>
  )

  const mainNavigation =
    {
      "sg":[
        {
          title: 'Find a Home',
          url: 'https://omh.sg',
          code: 'menu-1'
        },
        {
          title: 'Post a Property',
          url: 'https://omh.sg/post-a-property',
          code: 'menu-2'
        },
        {
          title: 'Agent Service',
          url: 'https://ohmyhome.com/en-sg/property-agent-services',
          code: 'menu-4'
        },
        {
          title: 'Renovation',
          url: 'https://renovation.ohmyhome.com/en-sg',
          code: 'menu-5'
        },
        {
          title: 'Home Services',
          url: 'https://omh.sg/services',
          code: 'menu-6'
        },
      ],
      "my": [
        {
          title: 'New Launches',
          url: 'https://ohmyhome.com.my/new-launches',
          code: 'menu-1'
        },
        {
          title: 'Find a Home',
          url: 'https://ohmyhome.com.my/find-a-home',
          code: 'menu-2'
        },
        {
          title: 'Post a Property',
          url: 'https://ohmyhome.com.my/post-a-property',
          code: 'menu-4'
        },
        {
          title: 'Get an Agent',
          url: 'https://ohmyhome.com.my/get-an-agent',
          code: 'menu-5'
        },
      ],
      "ph": [
        {
          title: 'New Properties',
          url: 'https://ohmyhome.com/en-ph/property-investments',
          code: 'menu-1'
        },
        {
          title: 'Agent Services',
          url: 'https://omh.sg/blog/condo',
          code: 'menu-2'
        },
        {
          title: 'Resale Properties',
          url: 'https://ohmyhome.com/en-ph/resale-properties',
          code: 'menu-3'
        },
        {
          title: 'About Us',
          url: 'https://ohmyhome.com/en-ph/about-us',
          code: 'menu-4'
        },
        {
          title: 'Blogs',
          url: '/en-ph/',
          code: 'menu-5'
        },
      ]
    }

  const navigationSG = [,
    {
      title: 'Find a Home',
      url: 'https://omh.sg',
      code: 'menu-1'
    },
    {
      title: 'Post a Property',
      url: 'https://omh.sg/post-a-property',
      code: 'menu-2'
    },
    {
      title: 'Agent Service',
      url: 'https://ohmyhome.com/en-sg/property-agent-services',
      code: 'menu-4'
    },
    {
      title: 'Renovation',
      url: 'https://renovation.ohmyhome.com/en-sg',
      code: 'menu-5'
    },
    {
      title: 'Home Services',
      url: 'https://omh.sg/services',
      code: 'menu-6'
    },
  ]

  const navigationPH = [,
    {
      title: 'New Properties',
      url: 'https://ohmyhome.com/en-ph/property-investments',
      code: 'menu-1'
    },
    {
      title: 'Agent Services',
      url: 'https://omh.sg/blog/condo',
      code: 'menu-2'
    },
    {
      title: 'Resale Properties',
      url: 'https://ohmyhome.com/en-ph/resale-properties',
      code: 'menu-3'
    },
    {
      title: 'About Us',
      url: 'https://ohmyhome.com/en-ph/about-us',
      code: 'menu-4'
    },
    {
      title: 'Blogs',
      url: '/en-ph/',
      code: 'menu-5'
    },
  ]

  const navigationMY = [,
    {
      title: 'New Launches',
      url: 'https://ohmyhome.com.my/new-launches',
      code: 'menu-1'
    },
    {
      title: 'Find a Home',
      url: 'https://ohmyhome.com.my/find-a-home',
      code: 'menu-2'
    },
    {
      title: 'Post a Property',
      url: 'https://ohmyhome.com.my/post-a-property',
      code: 'menu-4'
    },
    {
      title: 'Get an Agent',
      url: 'https://ohmyhome.com.my/get-an-agent',
      code: 'menu-5'
    },
  ]

  const categoryLinksSG = [,
    {
      title: 'Featured',
      url: '/en-sg/category/featured',
      code: 'menu-1'
    },
    {
      title: 'HDB',
      url: '/en-sg/category/hdb-blog',
      code: 'menu-2'
    },
    {
      title: 'Condo',
      url: '/en-sg/category/condo',
      code: 'menu-3'
    },
    {
      title: 'Financing',
      url: '/en-sg/category/financing',
      code: 'menu-4'
    },
    {
      title: 'Town',
      url: '/en-sg/category/hot-towns',
      code: 'menu-5'
    },
    {
      title: 'Tips & Guide',
      url: '/en-sg/category/hdb-tips-and-tricks',
      code: 'menu-6'
    },
    {
      title: 'Home Renovation',
      url: '/en-sg/category/home-renovation',
      code: 'menu-7'
    }
  ]

  const categoryLinksMY = [,
    {
      title: 'Featured',
      url: '/en-my/category/featured',
      code: 'menu-1'
    },
    {
      title: 'Investor Guides',
      url: '/en-my/category/investor-guides',
      code: 'menu-2'
    },
    {
      title: 'Property Guides',
      url: '/en-my/category/property-guides',
      code: 'menu-3'
    },
    {
      title: 'User Guides',
      url: '/en-my/category/user-guides',
      code: 'menu-4'
    },
    {
      title: 'Financial',
      url: '/en-my/category/financial-planning-MY',
      code: 'menu-5'
    },
    {
      title: 'Lifestyle',
      url: '/en-my/category/lifestyle',
      code: 'menu-7'
    }
  ]
  const categoryLinksPH = [,
    {
      title: 'Property Guides',
      url: '/en-ph/category/property-guides-Philippines',
      code: 'menu-1'
    },
    {
      title: 'Lifestyle',
      url: '/en-ph/category/lifestyle-Philippines',
      code: 'menu-2'
    },
    {
      title: 'Featured',
      url: '/en-ph/category/featured-Philippines',
      code: 'menu-3'
    },
    {
      title: 'Investing',
      url: '/en-ph/category/finance-and-investments',
      code: 'menu-4'
    },
    {
      title: 'Location Guides',
      url: '/en-ph/category/location-guides',
      code: 'menu-5'
    },
    {
      title: 'OMH News',
      url: '/en-ph/category/Philippines-property-news',
      code: 'menu-6'
    },
  ]

  const router = useRouter()
  const country = router && router.query && router.query.lang_country && router.query.lang_country.split(/-/)[1] || "sg"

  let navigationLogo
  if (post) {
    navigationLogo = 
    <React.Fragment>
      { router.query.lang_country === 'en-sg' ? <a href={`https://omh.sg?utm_source=blog&utm_medium=${post.slug}`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-my' ? <a href={`https://ohmyhome.com.my?utm_source=blog&utm_medium=${post.slug}`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-ph' ? <a href={`https://ohmyhome.com/en-ph?utm_source=blog&utm_medium=${post.slug}`}>{LogoMobile}</a> : '' }
    </React.Fragment> 
  } else if (info) {
    navigationLogo = 
    <React.Fragment>
      { router.query.lang_country === 'en-sg' ? <a href={`https://omh.sg?utm_source=blog&utm_medium=${info.slug}`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-my' ? <a href={`https://ohmyhome.com.my?utm_source=blog&utm_medium=${info.slug}`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-ph' ? <a href={`https://ohmyhome.com/en-ph?utm_source=blog&utm_medium=${info.slug}`}>{LogoMobile}</a> : '' }
    </React.Fragment> 
  } else {
    navigationLogo = 
    <React.Fragment>
      { router.query.lang_country === 'en-sg' ? <a href={`https://omh.sg?utm_source=blog&utm_medium=home_page`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-my' ? <a href={`https://ohmyhome.com.my?utm_source=blog&utm_medium=home_page`}>{LogoMobile}</a> : '' }
      { router.query.lang_country === 'en-ph' ? <a href={`https://ohmyhome.com/en-ph?utm_source=blog&utm_medium=home_page`}>{LogoMobile}</a> : '' }
      { router.route === '/' ?  <a href={`https://omh.sg?utm_source=blog&utm_medium=home_page`}>{LogoMobile}</a> : '' }
    </React.Fragment> 
  }

  let navigationItems
  if (post) {
    navigationItems = 
    <React.Fragment>
      <div className="header-navigation-wrapper">
        {
          mainNavigation && mainNavigation[country].map((item) => {
            return (
              <React.Fragment key={item.code}>
                <span className={`${router.asPath === item.url || router.asPath === item.code ? `active` : null} navigation-item`}>
                  
                  <Link prefetch={false} href={`${item.url}?utm_source=blog&utm_medium=${post.slug}`}>
                    <a className={`link-item-container h-full relative block  ${item.code}`}>
                      <span className="flex items-center px-headerNav h-full link-item">{item.title}</span>
                      <span className="border-item" />
                    </a>
                  </Link>
                </span>
              </React.Fragment>
            )
          })
        }
      </div>  
    </React.Fragment>
  } else if (info) {
    navigationItems = 
    <React.Fragment>
      <div className="header-navigation-wrapper">
        {
          mainNavigation && mainNavigation[country].map((item) => {
            return (
              <React.Fragment key={item.code}>
                <span className={`${router.asPath === item.url || router.asPath === item.code ? `active` : null} navigation-item`}>
                  
                  <Link prefetch={false} href={`${item.url}?utm_source=blog&utm_medium=${info.slug}`}>
                    <a className={`link-item-container h-full relative block  ${item.code}`}>
                      <span className="flex items-center px-headerNav h-full link-item">{item.title}</span>
                      <span className="border-item" />
                    </a>
                  </Link>
                </span>
              </React.Fragment>
            )
          })
        }
      </div>
    </React.Fragment>
  } else {
    navigationItems = 
    <React.Fragment>
      <div className="header-navigation-wrapper">
        {
          mainNavigation && mainNavigation[country].map((item) => {
            return(
              <React.Fragment key={item.code}>
                <span className={`${router.asPath === item.url || router.asPath === item.code ? `active` : null} navigation-item`}>
                  
                  <Link prefetch={false} href={`${item.url}?utm_source=blog&utm_medium=home_page`}>
                    <a className={`link-item-container h-full relative block  ${item.code}`}>
                      <span className="flex items-center px-headerNav h-full link-item">{item.title}</span>
                      <span className="border-item" />
                    </a>
                  </Link>
                </span>
              </React.Fragment>
            )
          })
        }
      </div> 
    </React.Fragment>
  }

  return (
    <div className="mobile-wrapper">
      <div className="tabs">
        <div className="tab">
          <input type="radio" id="mobile-sidebar_wrapper" name="mobile-navigation" />
          <label className="tab-label">
            <label htmlFor="mobile-sidebar_wrapper" className="tab-open_mobile">
              {Menu}
            </label>
            <div className="logo-wrapper">
              {navigationLogo}
            </div>
            <div className="phone-wrapper">
              <a href={'tel:6329677933'}>
                {Phone}
              </a>
            </div>
          </label>
          
          <div className="tab-content_mobile">
            <div className="tab tab-close_wrapper">
              <input type="radio" id="mobile-content" name="mobile-navigation" />
              <label htmlFor="mobile-content" className="tab-close">
                {close}
              </label>
            </div>
            <div className="tab-content_mobile_wrapper">
              <div className="header-container_middle"> 
                <div className="header-image-wrapper">
                  {navigationLogo}
                </div>
                <div className="header-navigation-wrapper">
                  {navigationItems}
                </div> 
              </div>
            <div className="country-mobile_wrapper">
              <CountryPicker />
            </div>
            </div>
            
          </div>
        </div>
      </div>
      <div className="header-bottom">
        { router.query.lang_country === 'en-sg' ?
          <div className="header-bottom-container">
            <div className="header-container-title">
              <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
            </div>
            <div className="header-container_bottom">
              {
                categoryLinksSG && categoryLinksSG.map( item => {
                  const { title, url, code } = item
                  return(
                    <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                      <Link prefetch={false} href={url}>
                        <a className={`link-item-container h-full relative block  ${code}`}>
                          <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                          <span className="border-item" />
                        </a>
                      </Link>
                    </span>
                  )
                })
              }
            </div>
          </div> : ''
        }
        { router.query.lang_country === 'en-my' ?
          <div className="header-bottom-container">
            <div className="header-container-title">
              <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
            </div>
            <div className="header-container_bottom">
              {
                categoryLinksMY && categoryLinksMY.map( item => {
                  const { title, url, code } = item
                  return(
                    <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                      <Link prefetch={false} href={url}>
                        <a className={`link-item-container h-full relative block  ${code}`}>
                          <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                          <span className="border-item" />
                        </a>
                      </Link>
                    </span>
                  )
                })
              }
            </div>
          </div> : ''
        }
        { router.query.lang_country === 'en-ph' ?
          <div className="header-bottom-container">
            <div className="header-container-title">
              <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
            </div>
            <div className="header-container_bottom">
              {
                categoryLinksPH && categoryLinksPH.map( item => {
                  const { title, url, code } = item
                  return(
                    <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                      <Link prefetch={false} href={url}>
                        <a className={`link-item-container h-full relative block  ${code}`}>
                          <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                          <span className="border-item" />
                        </a>
                      </Link>
                    </span>
                  )
                })
              }
            </div>
          </div> : ''
        }
        </div> 

      <style jsx>{`
        .mobile-wrapper {
          height: 100%;
        }
        label.tab-label {
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 0 15px;
          height: 56px;
          border-bottom: 1px solid #EBEBEB;
        }

        input#mobile-sidebar_wrapper, input#mobile-content {
          position: absolute;
          opacity: 0;
          z-index: -1;
        }
        .tab-open_mobile {
          font-size: 14px;
          line-height: 14px;
          color: #E86225;
          border-radius: 5px;
          text-decoration: none;
          display: inline-block;
          font-variation-settings: "wght" 550;
          transition: all 100ms ease 0s;
          cursor: pointer;
        }
        .tab-open_mobile:hover {
          border: 1px solid rgba(232, 98, 37, 0.6);
        }
        input#mobile-sidebar_wrapper:checked + .tab-open_mobile {
          position: absolute;
          left:0;
          right: 0;
          top: 0;
          z-index: -1;
        } 
        .tab-close_wrapper {
          border-bottom:1px solid #E0E0E0;
        }
        .tab-close {
          display: -webkit-box;
          display: flex;
          justify-content: flex-end;
          padding: 13px 19px 17px;
          font-size: 0.75em;
          cursor: pointer;
          color: #1C1C1C;
          background: #FFF;
        }
        .tab-content_mobile {
          left: -100%;
          background: white;
          position: fixed;
          height: 100vh;
          width: 100%;
          max-width: 400px;
          top: 0;
          bottom: 0;
          background: #FFF;
          border-right: 1px solid #E0E0E0;
          z-index: 2;
          transition: all 100ms ease 0s;
          box-shadow: 1px 0px 3px rgba(0, 0, 0, 0.2);
        }
        input#mobile-sidebar_wrapper:checked ~ .tab-content_mobile {
          left: 0;
          z-index: 10;
        }
        .tab-content_mobile_wrapper {
          height: 100%;
          background: #FFF;
          overflow: auto;
          padding-bottom: 40px;
        }
        input#mobile-sidebar_wrapper:checked ~ .tab-content_mobile .tab-close:after {
          content: " ";
          position: fixed;
          left: 0;
          right: 0;
          background: rgba(0,0,0,0.4);
          top: 0;
          bottom: 0;
          z-index: -999;
          text-indent: -9999px;
          overflow: hidden;
        }
        .header-image-wrapper a {
          padding: 44px 36px 31px;
          border-bottom: 1px solid rgba(224, 224, 224, 0.7);
          display: block;
        }
        .header-navigation-wrapper a{
          font-size: 14px;
          line-height: 14px;
          color: rgb(28, 28, 28);
          padding: 21px 36px;
          display: block;
          font-variation-settings: "wght" 520;
        }
        .header-navigation-wrapper {
          border-bottom: 1px solid rgba(224, 224, 224, 0.7);
          display: block;
        }
        .country-mobile_wrapper {
          padding: 24px 36px;
        }
        .header-bottom {
          display: none;
        }
        @media screen and (min-width: 768px) {
          .header-bottom {
            background: #FFF;
            border-bottom: 1px solid #F2F2F2
            box-shadow: 0px 4px 4px #F7F6F7;
            display: block;
          }
          .header-bottom-container {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin: 0 auto;
            max-width: 1224px;
          }
          .header-container_bottom {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            width: 100%;
            text-align: center;
          }
          .header-container-title a {
            padding: 24px;
            color: #1C1C1C;
            font-size: 18px;
            line-height: 26px;
            letter-spacing: -0.250037px;
            font-weight: 700;
            margin-right: 0px;
            display: block;
          }
          .header-container-title a:hover {
            color: #D8571D;
            background-color: #FEF4EE;
          }
          .header-container_bottom a {         
            color: #1C1C1C;
            font-weight: 600;
            font-size: 14px;
            line-height: 14px;
            text-decoration: none;
            padding: 14px 20px;
            transition: all .2s ease-out .1s;
            display: block;
            border-radius: 5px;
            margin: 0;
          }
          .header-container_bottom a:hover, .header-container_bottom a:active {         
            color: #D8571D;
            background-color: #FEF4EE;
          }
          .header-container_bottom a:hover {         
            color: #EC6423;
          }
          .header-container_bottom a {         
            padding: 14px 20px;
          }
          .header-bottom {
            position: sticky;
            top: 0;
            z-index: 8;
          } 
        }

        
      `}</style>
    </div>
  )
}

export default Header