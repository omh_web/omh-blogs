import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"

function  countryPicker() {

  // Flags
  const SGP = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2 9H46V24H2V9Z" fill="#FF3D00"/>
    <path d="M2 24H46V39H2V24ZM13.621 21.688C13.023 21.893 12.385 22 11.716 22C8.564 22 6 19.543 6 16.5C6 13.457 8.564 11 11.716 11C12.385 11 13.023 11.107 13.621 11.312C10.733 11.409 8.432 13.701 8.432 16.5C8.432 19.299 10.733 21.59 13.621 21.688Z" fill="#ECEFF1"/>
    <path d="M12.427 16.6329L11.65 16.0599L10.874 16.6329L11.173 15.7089L10.394 15.1389L11.355 15.1409L11.65 14.2149L11.945 15.1409L12.906 15.1389L12.127 15.7089L12.427 16.6329ZM18.52 16.6329L17.743 16.0599L16.967 16.6329L17.266 15.7089L16.487 15.1389L17.448 15.1409L17.743 14.2149L18.038 15.1409L19 15.1379L18.221 15.7079L18.52 16.6329ZM15.477 14.4569L14.7 13.8839L13.924 14.4569L14.223 13.5329L13.444 12.9629L14.405 12.9649L14.7 12.0389L14.995 12.9649L15.956 12.9629L15.177 13.5329L15.477 14.4569ZM13.567 20.1139L12.79 19.5409L12.014 20.1139L12.313 19.1899L11.534 18.6199L12.495 18.6219L12.79 17.6959L13.085 18.6219L14.046 18.6199L13.267 19.1899L13.567 20.1139ZM17.38 20.1139L16.603 19.5409L15.827 20.1139L16.126 19.1899L15.347 18.6199L16.308 18.6219L16.603 17.6959L16.898 18.6219L17.859 18.6199L17.08 19.1899L17.38 20.1139Z" fill="#ECEFF1"/>
    </svg>
  )
  const MYS = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2 10H46V37H2V10Z" fill="#D50000"/>
    <path d="M2 35.9999H46V37.9999H2V35.9999ZM2 31.9999H46V33.9999H2V31.9999ZM2 27.9999H46V29.9999H2V27.9999ZM2 23.9999H46V25.9999H2V23.9999ZM2 19.9999H46V21.9999H2V19.9999ZM2 15.9999H46V17.9999H2V15.9999ZM2 12.1429H46V13.9999H2V12.1429Z" fill="#ECEFF1"/>
    <path d="M2 10H28V26H2V10Z" fill="#311B92"/>
    <path d="M12.02 12.071C8.697 12.071 6 14.759 6 18.071C6 21.383 8.697 24.071 12.02 24.071C13.221 24.071 14.339 23.72 15.279 23.115C14.732 23.304 14.145 23.407 13.534 23.407C10.587 23.408 8 20.937 8 18C8 15.063 10.587 12.767 13.533 12.767C14.184 12.767 14.808 12.883 15.385 13.096C14.424 12.45 13.266 12.071 12.02 12.071Z" fill="#FFD600"/>
    <path d="M23.003 21.123L20.468 19.8L21.218 22.508L19.522 20.243L18.992 23L18.472 20.242L16.769 22.502L17.527 19.797L14.987 21.112L16.874 18.995L14 19.105L16.642 17.996L14.004 16.879L16.877 16.998L14.997 14.876L17.533 16.199L16.783 13.491L18.479 15.756L19.008 13L19.528 15.758L21.231 13.498L20.473 16.203L23.013 14.888L21.126 17.005L24 16.895L21.358 18.004L23.996 19.121L21.123 19.002L23.003 21.123Z" fill="#FFD600"/>
    </svg>
  )
  const PHL = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2 24H46V39H2V24Z" fill="#FF3D00"/>
    <path d="M2 9H46V24H2V9Z" fill="#3F51B5"/>
    <path d="M2 9V39L25 24.002L2 9Z" fill="#ECEFF1"/>
    <path d="M4.5 13L4.963 13.988L6 14.146L5.25 14.916L5.428 16L4.5 15.488L3.572 16L3.75 14.916L3 14.146L4.037 13.988L4.5 13ZM4.787 32.072L5.25 33.061L6.287 33.218L5.537 33.988L5.715 35.072L4.787 34.561L3.859 35.072L4.037 33.988L3.287 33.218L4.324 33.061L4.787 32.072ZM19.463 22.324L19.926 23.313L20.963 23.47L20.213 24.24L20.391 25.324L19.463 24.813L18.535 25.324L18.713 24.24L17.963 23.47L19 23.313L19.463 22.324ZM10 21C9.20435 21 8.44129 21.3161 7.87868 21.8787C7.31607 22.4413 7 23.2044 7 24C7 24.7956 7.31607 25.5587 7.87868 26.1213C8.44129 26.6839 9.20435 27 10 27C10.7956 27 11.5587 26.6839 12.1213 26.1213C12.6839 25.5587 13 24.7956 13 24C13 23.2044 12.6839 22.4413 12.1213 21.8787C11.5587 21.3161 10.7956 21 10 21Z" fill="#FFA000"/>
    <path d="M11 29C11 29.553 10.553 30 10 30C9.447 30 9 29.553 9 29L10 27.002V21L9 19C9 18.447 9.447 18 10 18C10.553 18 11 18.447 11 19L10 21V27.002L11 29Z" fill="#FFA000"/>
    <path d="M15 23C15.553 23 16 23.447 16 24C16 24.553 15.553 25 15 25L13.002 24H7L5 25C4.447 25 4 24.553 4 24C4 23.447 4.447 23 5 23L7 24H13.002L15 23Z" fill="#FFA000"/>
    <path d="M5.75701 21.173C5.36501 20.781 5.36501 20.15 5.75701 19.758C6.14801 19.367 6.78001 19.367 7.17101 19.758L7.87601 21.878L12.12 26.122L14.241 26.829C14.632 27.22 14.632 27.852 14.241 28.243C13.85 28.634 13.218 28.634 12.827 28.243L12.12 26.122L7.87601 21.878L5.75701 21.173Z" fill="#FFA000"/>
    <path d="M12.828 19.758C13.22 19.366 13.851 19.366 14.243 19.758C14.634 20.149 14.634 20.781 14.243 21.172L12.123 21.877L7.87897 26.121L7.17197 28.242C6.78097 28.633 6.14897 28.633 5.75797 28.242C5.36697 27.851 5.36697 27.219 5.75797 26.828L7.87897 26.121L12.123 21.877L12.828 19.758Z" fill="#FFA000"/>
    </svg>
  )

  const router = useRouter()

  return (
    <div>
      <div data-header="user-block">
        <div className=""
        >
          <span
            className="country-active"
          >
            { router.route === '/' ?
              <React.Fragment>
                <span>{SGP}</span>
                <span className="mobile-title">
                  Singapore
                </span>
              </React.Fragment> : ''
            }
            { router.query.lang_country === 'en-sg' ?
              <React.Fragment>
                <span>{SGP}</span>
                <span className="mobile-title">
                  Singapore
                </span>
              </React.Fragment> : ''
            }
            { router.query.lang_country === 'en-ph' ?
              <React.Fragment>
                <span>{PHL}</span>
                <span className="mobile-title">
                  Phillipines
                </span>
              </React.Fragment> : ''
            }
            { router.query.lang_country === 'en-my' ?
              <React.Fragment>
                <span>{MYS}</span>
                <span className="mobile-title">
                  Malaysia
                </span>
              </React.Fragment> : ''
            }
          </span>
        </div>

        <div data-header="user-block-options" className="dropdown-flags">
          <Link
            href="/en-sg/"
          >
            <a
              className=""
            >
              <span>{SGP} &nbsp;&nbsp; Singapore</span>
            </a>
          </Link>
          <Link
            href="/en-my/"
          >
            <a
              className=""
            >
              <span>{MYS} &nbsp;&nbsp; Malaysia</span>
            </a>
          </Link>
          <Link
            href="/en-ph/"
          >
            <a
              className=""
            >
              <span>{PHL} &nbsp;&nbsp; Philippines</span>
            </a>
          </Link>
        </div>

      </div>
      <style jsx>{`

        
        .country-active span {
          width: auto;
          height: auto;
        }
        span {
          display: block;
          width: 100%;
          height: 32px;
          display: flex;
          align-items: center;
          justify-content: flex-start;
        }
        .dropdown-flags {
          position: relative;
          background: transparent;
          width: 100%;
          box-shadow: 0 0 0;
          padding: 20px;
          display: none;
        }
        a {
          text-decoration: none;
          background: #FFF;
          display: block;
          padding: 10px;
          font-size: 15px;
          color: #484848
        }
        .country-active {
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }
        .mobile-title {
          display: block;
          margin-left: 8px;
          font-size: 15px;
          line-height: 14px;
          color: rgb(28, 28, 28);
          font-variation-settings: "wght" 520;
          position: relative;
          vertical-align: middle;
        }
        .mobile-title:after, .mobile-title:before {
          position: absolute;
          display: block;
          content: "";
          border: 5px solid transparent;
          transition: all 0.35s ease 0s;
          left: 120px;
          transform: translate(0%, -50%);
          top: 70%;
        }
        .mobile-title:before {
          border-top-color: rgb(168, 168, 168);
          top: 70%;
        }
        .mobile-title:after {
          border-top-color: rgb(255, 255, 255);
          top: 60%;
        }
        [data-header="user-block"]:hover > [data-header="user-block-options"] {
          display: block;
          z-index: 12;
        }
        @media screen and (min-width: 1025px) {
          .mobile-title {
            display: none;
          }
          .dropdown-flags {
            width: 209px;
            overflow: hidden;
            position:absolute;
            display: none;
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06);
            padding: 0px;
          }
        }
      `}</style>
    </div>
  )
}

export default countryPicker
