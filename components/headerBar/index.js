import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"

import HeaderDesktop from './header/desktop'
import HeaderMobile from './header/mobile'

function HeaderBar({post, info, rootUrl}) {

  const router = useRouter()

  return (
    <React.Fragment>
     
      <header>
        <div className="desktop">
          <HeaderDesktop 
            info={info}
            post={post} 
            rootUrl={rootUrl}
          />
        </div>
        <div className="mobile">
          <HeaderMobile
            info={info}
            post={post} 
            rootUrl={rootUrl}
          />
        </div>
        <style jsx>{`
          .desktop {
            display: none;
            height: 100%;
          }
          .mobile {
            display: block;
            height: 100%;
          }
          @media screen and (min-width: 768px) {
            header {
              position: absolute;
              left: 0;
              right: 0;
              top: 0;
              bottom: 0;
              min-height: 198px;
            }
          }
          @media screen and (min-width: 1025px) {
            .desktop {
              display: block;
            }
            .mobile {
              display: none;
              
            }
            
          }
        `}</style>    
      </header>
    </React.Fragment>
    
  )
}

export default HeaderBar
