import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"
import CountryPicker from '../../countryPicker'

import {
  BrowserView,
  MobileView,
  isAndroid,
  isBrowser,
  isIOS,
  CustomView
} from "react-device-detect"


function Header({info}) {

  const Phone = (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1" width="26" height="26" rx="3.54545" stroke="#EE620F" />
      <path d="M10.0001 13.1035C11.0716 15.2094 12.7942 16.932 14.9038 18.0035L16.5408 16.3627C16.7455 16.1581 17.0394 16.0986 17.2961 16.1804C18.1295 16.4557 19.0262 16.6046 19.9526 16.6046C20.3655 16.6046 20.6967 16.9357 20.6967 17.3487V19.9531C20.6967 20.366 20.3655 20.6972 19.9526 20.6972C12.9654 20.6972 7.30273 15.0345 7.30273 8.04733C7.30273 7.63435 7.63758 7.30322 8.04684 7.30322H10.6512C11.0642 7.30322 11.3953 7.63435 11.3953 8.04733C11.3953 8.97374 11.5441 9.87039 11.8195 10.7038C11.9013 10.9605 11.8418 11.2544 11.6372 11.4591L10.0001 13.1035Z" fill="#EE620F" />
    </svg>
  )
  const Menu = (
    <svg width="30" height="20" viewBox="0 0 30 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 2H42" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 10H43" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
      <path d="M2 18H43" stroke="#EE620F" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" strokeDasharray="28" />
    </svg>
  )
  const Logo = (
    <svg width="112" height="22" viewBox="0 0 112 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Buy Sell Rent Real Estate</title>
      <path fillRule="evenodd" clipRule="evenodd" d="M5.82822 17.4561C9.12854 17.4561 11.6564 14.921 11.6564 11.3718C11.6564 7.75027 9.12854 5.2876 5.82822 5.2876C2.5279 5.2876 0 7.89513 0 11.3718C0 14.8486 2.5279 17.4561 5.82822 17.4561ZM5.8282 8.11227C7.30281 8.11227 8.63698 9.19875 8.63698 11.3717C8.63698 13.5446 7.30281 14.6311 5.8282 14.6311C4.35359 14.6311 3.01942 13.5446 3.01942 11.3717C3.01942 9.19875 4.35359 8.11227 5.8282 8.11227Z" fill="#EE620F" />
      <path d="M16.4312 17.094H13.4819V0.14502H16.5014V6.51899C17.2036 5.64981 18.4675 5.21522 19.5208 5.21522C22.3296 5.21522 23.6638 7.24331 23.6638 9.77841V17.0216H20.6443V10.2854C20.6443 8.98166 20.0123 7.96762 18.608 7.96762C17.344 7.96762 16.5716 8.98166 16.5014 10.213L16.4312 17.094Z" fill="#EE620F" />
      <path d="M26.0513 17.0941V5.57749H28.9303V6.95369C29.5622 5.79478 30.9666 5.21533 32.1604 5.21533C33.7052 5.21533 34.8989 5.86722 35.4607 7.09855C36.3735 5.72235 37.497 5.21533 38.9717 5.21533C41.008 5.21533 42.9742 6.5191 42.9742 9.56122V17.0217H40.0952V10.2131C40.0952 8.98177 39.5334 8.04016 38.129 8.04016C36.8651 8.04016 36.0927 9.0542 36.0927 10.2855V17.0217H33.1434V10.2131C33.1434 8.98177 32.5115 8.04016 31.1773 8.04016C29.8431 8.04016 29.0707 9.0542 29.0707 10.2855V17.0217H26.0513V17.0941Z" fill="#EE620F" />
      <path d="M45.9235 21.6569L48.5216 15.6451L43.8169 5.57715H47.1874L50.1366 12.3133L52.8752 5.57715H56.0351L49.0834 21.6569H45.9235Z" fill="#EE620F "/>
      <path d="M60.3188 17.094H57.2993V0.14502H60.3188V6.51899C61.0209 5.64981 62.2849 5.21522 63.3382 5.21522C66.147 5.21522 67.4811 7.24331 67.4811 9.77841V17.0216H64.4617V10.2854C64.4617 8.98166 63.8297 7.96762 62.4253 7.96762C61.1614 7.96762 60.389 8.98166 60.3188 10.213V17.094Z" fill="#EE620F" />
      <path fillRule="evenodd" clipRule="evenodd" d="M74.9244 17.4561C78.2247 17.4561 80.7526 14.921 80.7526 11.3719C80.7526 7.75032 78.2247 5.21521 74.9244 5.28765C71.6241 5.28765 69.0962 7.89518 69.0962 11.3719C69.0962 14.8486 71.6241 17.4561 74.9244 17.4561ZM74.9244 8.11227C76.3991 8.11227 77.7332 9.19875 77.7332 11.3717C77.7332 13.5446 76.3991 14.6311 74.9244 14.6311C73.4498 14.6311 72.1157 13.5446 72.1157 11.3717C72.1157 9.19875 73.4498 8.11227 74.9244 8.11227Z" fill="#EE620F" />
      <path d="M82.5078 17.0941V5.57749H85.3868V6.95369C86.0188 5.79478 87.4232 5.21533 88.6169 5.21533C90.1617 5.21533 91.3555 5.86722 91.9172 7.09855C92.8301 5.72235 93.9536 5.21533 95.4282 5.21533C97.4646 5.21533 99.4307 6.5191 99.4307 9.56122V17.0217H96.4815V10.2131C96.4815 8.98177 95.9197 8.04016 94.5153 8.04016C93.2514 8.04016 92.479 9.0542 92.479 10.2855V17.0217H89.5298V10.2131C89.5298 8.98177 88.8978 8.04016 87.5636 8.04016C86.2294 8.04016 85.457 9.0542 85.457 10.2855V17.0217H82.5078V17.0941Z" fill="#EE620F" />
      <path fillRule="evenodd" clipRule="evenodd" d="M106.804 17.5285C109.542 17.5285 111.298 15.7902 111.86 13.8345L109.402 13.0378C109.051 14.0518 108.278 14.7761 106.874 14.7761C105.4 14.7761 104.136 13.6897 104.065 12.1686H111.93C111.93 12.1686 112 11.6616 112 11.227C112 7.53297 109.964 5.2876 106.523 5.2876C103.644 5.2876 101.046 7.67784 101.046 11.3718C101.046 15.2831 103.784 17.5285 106.804 17.5285ZM106.593 7.82237C108.279 7.82237 108.981 8.90884 109.051 10.0677H104.136C104.206 8.76398 105.329 7.74993 106.593 7.82237Z" fill="#EE620F" />
      <path d="M2.24707 19.6291L7.86463 17.094H4.143L2.24707 19.6291Z" fill="#EE620F" />
      <path d="M11.6564 2.82483L5.82822 0L0 2.82483V5.72209L5.82822 2.89726L11.6564 5.72209V2.82483Z" fill="#EE620F" />
    </svg>
  )
  const LogoMobile = (
    <svg width="156" height="30" viewBox="0 0 156 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <title>Buy Sell Rent Real Estate</title>
      <path fillRule="evenodd" clipRule="evenodd" d="M8.06965 24.1811C12.6392 24.1811 16.1393 20.6696 16.1393 15.7534C16.1393 10.7369 12.6392 7.32564 8.06965 7.32564C3.50009 7.32564 0 10.9375 0 15.7534C0 20.5692 3.50009 24.1811 8.06965 24.1811ZM8.07114 11.2373C10.1132 11.2373 11.9607 12.7422 11.9607 15.7521C11.9607 18.762 10.1132 20.267 8.07114 20.267C6.02912 20.267 4.18158 18.762 4.18158 15.7521C4.18158 12.7422 6.02912 11.2373 8.07114 11.2373Z" fill="#EE620F"/>
      <path d="M22.7509 23.6783H18.6675V0.200943H22.8481V9.03002C23.8204 7.82605 25.5704 7.22407 27.0288 7.22407C30.9178 7.22407 32.765 10.0333 32.765 13.5449V23.5779H28.5844V14.2472C28.5844 12.4412 27.7094 11.0366 25.7649 11.0366C24.0148 11.0366 22.9454 12.4412 22.8481 14.1469L22.7509 23.6783Z" fill="#EE620F"/>
      <path d="M36.0708 23.6793V7.72675H40.057V9.63303C40.932 8.02774 42.8765 7.2251 44.5293 7.2251C46.6683 7.2251 48.3211 8.12807 49.0989 9.83369C50.3628 7.92741 51.9184 7.2251 53.9601 7.2251C56.7797 7.2251 59.5019 9.03104 59.5019 13.2449V23.5789H55.5157V14.1479C55.5157 12.4423 54.7379 11.138 52.7934 11.138C51.0434 11.138 49.9739 12.5426 49.9739 14.2482V23.5789H45.8905V14.1479C45.8905 12.4423 45.0155 11.138 43.1682 11.138C41.3209 11.138 40.2515 12.5426 40.2515 14.2482V23.5789H36.0708V23.6793Z" fill="#EE620F"/>
      <path d="M63.5854 30L67.1828 21.6726L60.6687 7.72668H65.3355L69.4189 17.0574L73.2107 7.72668H77.5858L67.9605 30H63.5854Z" fill="#EE620F"/>
      <path d="M83.5161 23.6783H79.3354V0.200943H83.5161V9.03002C84.4884 7.82605 86.2384 7.22407 87.6968 7.22407C91.5858 7.22407 93.433 10.0333 93.433 13.5449V23.5779H89.2524V14.2472C89.2524 12.4412 88.3773 11.0366 86.4328 11.0366C84.6828 11.0366 83.6133 12.4412 83.5161 14.1469V23.6783Z" fill="#EE620F"/>
      <path fillRule="evenodd" clipRule="evenodd" d="M103.739 24.1809C108.309 24.1809 111.809 20.6694 111.809 15.7532C111.809 10.7367 108.309 7.2251 103.739 7.32543C99.1695 7.32543 95.6694 10.9373 95.6694 15.7532C95.6694 20.569 99.1695 24.1809 103.739 24.1809ZM103.741 11.2373C105.783 11.2373 107.63 12.7422 107.63 15.7521C107.63 18.762 105.783 20.267 103.741 20.267C101.699 20.267 99.8511 18.762 99.8511 15.7521C99.8511 12.7422 101.699 11.2373 103.741 11.2373Z" fill="#EE620F"/>
      <path d="M114.239 23.6793V7.72675H118.225V9.63303C119.1 8.02774 121.045 7.2251 122.698 7.2251C124.837 7.2251 126.489 8.12807 127.267 9.83369C128.531 7.92741 130.087 7.2251 132.128 7.2251C134.948 7.2251 137.67 9.03104 137.67 13.2449V23.5789H133.587V14.1479C133.587 12.4423 132.809 11.138 130.864 11.138C129.114 11.138 128.045 12.5426 128.045 14.2482V23.5789H123.961V14.1479C123.961 12.4423 123.086 11.138 121.239 11.138C119.392 11.138 118.322 12.5426 118.322 14.2482V23.5789H114.239V23.6793Z" fill="#EE620F"/>
      <path fillRule="evenodd" clipRule="evenodd" d="M147.878 24.2815C151.669 24.2815 154.1 21.8735 154.878 19.1646L151.475 18.061C150.989 19.4656 149.919 20.4689 147.975 20.4689C145.933 20.4689 144.183 18.964 144.086 16.857H154.975C154.975 16.857 155.072 16.1547 155.072 15.5527C155.072 10.4359 152.253 7.32564 147.489 7.32564C143.503 7.32564 139.905 10.6365 139.905 15.7534C139.905 21.1712 143.697 24.2815 147.878 24.2815ZM147.588 10.836C149.922 10.836 150.894 12.3411 150.991 13.9467H144.185C144.282 12.1405 145.838 10.7356 147.588 10.836Z" fill="#EE620F"/>
      <path d="M3.11157 27.1903L10.8907 23.6782H5.73703L3.11157 27.1903Z" fill="#EE620F"/>
      <path d="M16.14 3.91288L8.07038 0L0.000732422 3.91288V7.9261L8.07038 4.01321L16.14 7.9261V3.91288Z" fill="#EE620F"/>
    </svg>
  )
  const MYS = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 10H46V37H2V10Z" fill="#D50000"/>
      <path d="M2 35.9999H46V37.9999H2V35.9999ZM2 31.9999H46V33.9999H2V31.9999ZM2 27.9999H46V29.9999H2V27.9999ZM2 23.9999H46V25.9999H2V23.9999ZM2 19.9999H46V21.9999H2V19.9999ZM2 15.9999H46V17.9999H2V15.9999ZM2 12.1429H46V13.9999H2V12.1429Z" fill="#ECEFF1"/>
      <path d="M2 10H28V26H2V10Z" fill="#311B92"/>
      <path d="M12.02 12.071C8.697 12.071 6 14.759 6 18.071C6 21.383 8.697 24.071 12.02 24.071C13.221 24.071 14.339 23.72 15.279 23.115C14.732 23.304 14.145 23.407 13.534 23.407C10.587 23.408 8 20.937 8 18C8 15.063 10.587 12.767 13.533 12.767C14.184 12.767 14.808 12.883 15.385 13.096C14.424 12.45 13.266 12.071 12.02 12.071Z" fill="#FFD600"/>
      <path d="M23.003 21.123L20.468 19.8L21.218 22.508L19.522 20.243L18.992 23L18.472 20.242L16.769 22.502L17.527 19.797L14.987 21.112L16.874 18.995L14 19.105L16.642 17.996L14.004 16.879L16.877 16.998L14.997 14.876L17.533 16.199L16.783 13.491L18.479 15.756L19.008 13L19.528 15.758L21.231 13.498L20.473 16.203L23.013 14.888L21.126 17.005L24 16.895L21.358 18.004L23.996 19.121L21.123 19.002L23.003 21.123Z" fill="#FFD600"/>
    </svg>
  )
  const PHL = (
    <svg width="32" height="32" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 24H46V39H2V24Z" fill="#FF3D00"/>
      <path d="M2 9H46V24H2V9Z" fill="#3F51B5"/>
      <path d="M2 9V39L25 24.002L2 9Z" fill="#ECEFF1"/>
      <path d="M4.5 13L4.963 13.988L6 14.146L5.25 14.916L5.428 16L4.5 15.488L3.572 16L3.75 14.916L3 14.146L4.037 13.988L4.5 13ZM4.787 32.072L5.25 33.061L6.287 33.218L5.537 33.988L5.715 35.072L4.787 34.561L3.859 35.072L4.037 33.988L3.287 33.218L4.324 33.061L4.787 32.072ZM19.463 22.324L19.926 23.313L20.963 23.47L20.213 24.24L20.391 25.324L19.463 24.813L18.535 25.324L18.713 24.24L17.963 23.47L19 23.313L19.463 22.324ZM10 21C9.20435 21 8.44129 21.3161 7.87868 21.8787C7.31607 22.4413 7 23.2044 7 24C7 24.7956 7.31607 25.5587 7.87868 26.1213C8.44129 26.6839 9.20435 27 10 27C10.7956 27 11.5587 26.6839 12.1213 26.1213C12.6839 25.5587 13 24.7956 13 24C13 23.2044 12.6839 22.4413 12.1213 21.8787C11.5587 21.3161 10.7956 21 10 21Z" fill="#FFA000"/>
      <path d="M11 29C11 29.553 10.553 30 10 30C9.447 30 9 29.553 9 29L10 27.002V21L9 19C9 18.447 9.447 18 10 18C10.553 18 11 18.447 11 19L10 21V27.002L11 29Z" fill="#FFA000"/>
      <path d="M15 23C15.553 23 16 23.447 16 24C16 24.553 15.553 25 15 25L13.002 24H7L5 25C4.447 25 4 24.553 4 24C4 23.447 4.447 23 5 23L7 24H13.002L15 23Z" fill="#FFA000"/>
      <path d="M5.75701 21.173C5.36501 20.781 5.36501 20.15 5.75701 19.758C6.14801 19.367 6.78001 19.367 7.17101 19.758L7.87601 21.878L12.12 26.122L14.241 26.829C14.632 27.22 14.632 27.852 14.241 28.243C13.85 28.634 13.218 28.634 12.827 28.243L12.12 26.122L7.87601 21.878L5.75701 21.173Z" fill="#FFA000"/>
      <path d="M12.828 19.758C13.22 19.366 13.851 19.366 14.243 19.758C14.634 20.149 14.634 20.781 14.243 21.172L12.123 21.877L7.87897 26.121L7.17197 28.242C6.78097 28.633 6.14897 28.633 5.75797 28.242C5.36697 27.851 5.36697 27.219 5.75797 26.828L7.87897 26.121L12.123 21.877L12.828 19.758Z" fill="#FFA000"/>
    </svg>
  )
  const SGP = (
    <svg width="32" height="32"viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 9H46V24H2V9Z" fill="#FF3D00"/>
      <path d="M2 24H46V39H2V24ZM13.621 21.688C13.023 21.893 12.385 22 11.716 22C8.564 22 6 19.543 6 16.5C6 13.457 8.564 11 11.716 11C12.385 11 13.023 11.107 13.621 11.312C10.733 11.409 8.432 13.701 8.432 16.5C8.432 19.299 10.733 21.59 13.621 21.688Z" fill="#ECEFF1"/>
      <path d="M12.427 16.6329L11.65 16.0599L10.874 16.6329L11.173 15.7089L10.394 15.1389L11.355 15.1409L11.65 14.2149L11.945 15.1409L12.906 15.1389L12.127 15.7089L12.427 16.6329ZM18.52 16.6329L17.743 16.0599L16.967 16.6329L17.266 15.7089L16.487 15.1389L17.448 15.1409L17.743 14.2149L18.038 15.1409L19 15.1379L18.221 15.7079L18.52 16.6329ZM15.477 14.4569L14.7 13.8839L13.924 14.4569L14.223 13.5329L13.444 12.9629L14.405 12.9649L14.7 12.0389L14.995 12.9649L15.956 12.9629L15.177 13.5329L15.477 14.4569ZM13.567 20.1139L12.79 19.5409L12.014 20.1139L12.313 19.1899L11.534 18.6199L12.495 18.6219L12.79 17.6959L13.085 18.6219L14.046 18.6199L13.267 19.1899L13.567 20.1139ZM17.38 20.1139L16.603 19.5409L15.827 20.1139L16.126 19.1899L15.347 18.6199L16.308 18.6219L16.603 17.6959L16.898 18.6219L17.859 18.6199L17.08 19.1899L17.38 20.1139Z" fill="#ECEFF1"/>
    </svg>
  )
  const close = (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14 2.33337C7.54831 2.33337 2.33331 7.54837 2.33331 14C2.33331 20.4517 7.54831 25.6667 14 25.6667C20.4516 25.6667 25.6666 20.4517 25.6666 14C25.6666 7.54837 20.4516 2.33337 14 2.33337ZM19.8333 18.1884L18.1883 19.8334L14 15.645L9.81165 19.8334L8.16665 18.1884L12.355 14L8.16665 9.81171L9.81165 8.16671L14 12.355L18.1883 8.16671L19.8333 9.81171L15.645 14L19.8333 18.1884Z" fill="#5F5F5F"/>
    </svg>
  )

  const navigationSG = [,
    {
      title: 'Find a Home',
      url: 'https://omh.sg',
      code: 'menu-1'
    },
    {
      title: 'Post a Property',
      url: 'https://omh.sg/post-a-property',
      code: 'menu-2'
    },
    {
      title: 'Agent Service',
      url: 'https://ohmyhome.com/en-sg/property-agent-services',
      code: 'menu-4'
    },
    {
      title: 'Renovation',
      url: 'https://renovation.ohmyhome.com/en-sg',
      code: 'menu-5'
    },
    {
      title: 'Home Services',
      url: 'https://omh.sg/services',
      code: 'menu-6'
    },
  ]

  const navigationPH = [,
    {
      title: 'New Properties',
      url: 'https://ohmyhome.com/en-ph/property-investments',
      code: 'menu-1'
    },
    {
      title: 'Agent Services',
      url: 'https://omh.sg/blog/condo',
      code: 'menu-2'
    },
    {
      title: 'Resale Properties',
      url: 'https://ohmyhome.com/en-ph/resale-properties',
      code: 'menu-3'
    },
    {
      title: 'About Us',
      url: 'https://ohmyhome.com/en-ph/about-us',
      code: 'menu-4'
    },
    {
      title: 'Blogs',
      url: '/en-ph/',
      code: 'menu-5'
    },
  ]

  const navigationMY = [,
    {
      title: 'New Launches',
      url: 'https://ohmyhome.com.my/new-launches',
      code: 'menu-1'
    },
    {
      title: 'Find a Home',
      url: 'https://ohmyhome.com.my/find-a-home',
      code: 'menu-2'
    },
    {
      title: 'Post a Property',
      url: 'https://ohmyhome.com.my/post-a-property',
      code: 'menu-4'
    },
    {
      title: 'Get an Agent',
      url: 'https://ohmyhome.com.my/get-an-agent',
      code: 'menu-5'
    },
  ]

  const router = useRouter()

  return (
    <div className="mobile-wrapper">
      <div className="tabs">
        <div className="tab">
          <input type="radio" id="mobile-sidebar_wrapper" name="mobile-navigation" />
          <label className="tab-label">
            <label htmlFor="mobile-sidebar_wrapper" className="tab-open_mobile">
              {Menu}
            </label>
            <div className="logo-wrapper">
              <a href={'https://ohmyhome.com'}>
                {Logo}
              </a>
            </div>
            <div className="phone-wrapper">
              <a href={'tel:6329677933'}>
                {Phone}
              </a>
            </div>
          </label>
          
          <div className="tab-content_mobile">
            <div className="tab tab-close_wrapper">
              <input type="radio" id="mobile-content" name="mobile-navigation" />
              <label htmlFor="mobile-content" className="tab-close">
                {close}
              </label>
            </div>
            <div className="tab-content_mobile_wrapper">
              { router.query.lang_country === 'en-sg' ?
                <div className="header-container_middle"> 
                  <div className="header-image-wrapper">
                    <a href="https://omh.sg">{Logo}</a>
                  </div>
                  <div className="header-navigation-wrapper">
                    {
                      navigationSG && navigationSG.map( item => {
                        const { title, url, code } = item
                        return(
                          <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                            
                            <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                              <a className={`link-item-container h-full relative block  ${code}`}>
                                <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                                <span className="border-item" />
                              </a>
                            </Link>
                          </span>
                        )
                      })
                    }
                  </div> 
                </div> : ''
              }
              { router.query.lang_country === 'en-ph' ?
                <div className="header-container_middle"> 
                  <div className="header-image-wrapper">
                    <a href="https://ohmyhome.com/en-ph">{Logo}</a>
                  </div>  
                  <div className="header-navigation-wrapper">
                    {
                      navigationPH && navigationPH.map( item => {
                        const { title, url, code } = item
                        return(
                          // {`https://api.omh.app/store/cms/media/${item.developer.logo}`}
                          <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                            <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                              <a className={`link-item-container h-full relative block  ${code}`}>
                                <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                                <span className="border-item" />
                              </a>
                            </Link>
                          </span>
                        )
                      })
                    }
                  </div> 
                </div> : ''
              }
              { router.query.lang_country === 'en-my' ?
                <div className="header-container_middle"> 
                  <div className="header-image-wrapper">
                    <a href="https://ohmyhome.com.my">{Logo}</a>
                  </div>
                  <div className="header-navigation-wrapper">
                    {
                      navigationMY && navigationMY.map( item => {
                        const { title, url, code } = item
                        return(
                          <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                            <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                              <a className={`link-item-container h-full relative block  ${code}`}>
                                <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                                <span className="border-item" />
                              </a>
                            </Link>
                          </span>
                        )
                      })
                    }
                  </div> 
                </div> : ''
              }
            <div className="country-mobile_wrapper">
              <CountryPicker />
            </div>
            </div>
            
          </div>
        </div>
      </div>

      <style jsx>{`

        label.tab-label {
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 0 15px;
          height: 56px;
          border-bottom: 1px solid #EBEBEB;
        }

        input#mobile-sidebar_wrapper, input#mobile-content {
          position: absolute;
          opacity: 0;
          z-index: -1;
        }
        .tab-open_mobile {
          font-size: 14px;
          line-height: 14px;
          color: #E86225;
          border-radius: 5px;
          text-decoration: none;
          display: inline-block;
          font-variation-settings: "wght" 550;
          transition: all 100ms ease 0s;
          cursor: pointer;
        }
        .tab-open_mobile:hover {
          border: 1px solid rgba(232, 98, 37, 0.6);
        }
        input#mobile-sidebar_wrapper:checked + .tab-open_mobile {
          position: absolute;
          left:0;
          right: 0;
          top: 0;
          z-index: -1;
        } 
        .tab-close_wrapper {
          border-bottom:1px solid #E0E0E0;
        }
        .tab-close {
          display: -webkit-box;
          display: flex;
          justify-content: flex-end;
          padding: 13px 19px 17px;
          font-size: 0.75em;
          cursor: pointer;
          color: #1C1C1C;
          background: #FFF;
        }
        .tab-content_mobile {
          left: -100%;
          background: white;
          position: fixed;
          height: 100vh;
          width: 100%;
          max-width: 400px;
          top: 0;
          bottom: 0;
          background: #FFF;
          border-right: 1px solid #E0E0E0;
          z-index: 2;
          transition: all 100ms ease 0s;
          box-shadow: 1px 0px 3px rgba(0, 0, 0, 0.2);
        }
        input#mobile-sidebar_wrapper:checked ~ .tab-content_mobile {
          left: 0;
          z-index: 4;
        }
        .tab-content_mobile_wrapper {
          height: 100%;
          background: #FFF;
          overflow: auto;
          padding-bottom: 40px;
        }
        input#mobile-sidebar_wrapper:checked ~ .tab-content_mobile .tab-close:after {
          content: " ";
          position: fixed;
          left: 0;
          right: 0;
          background: rgba(0,0,0,0.4);
          top: 0;
          bottom: 0;
          z-index: -999;
          text-indent: -9999px;
          overflow: hidden;
        }
        .header-image-wrapper a {
          padding: 44px 36px 31px;
          border-bottom: 1px solid rgba(224, 224, 224, 0.7);
          display: block;
        }
        .header-navigation-wrapper a{
          font-size: 14px;
          line-height: 14px;
          color: rgb(28, 28, 28);
          padding: 21px 36px;
          display: block;
          font-variation-settings: "wght" 520;
        }
        .header-navigation-wrapper {
          border-bottom: 1px solid rgba(224, 224, 224, 0.7);
          display: block;
        }
        .country-mobile_wrapper {
          padding: 24px 36px;
        }
      `}</style>
    </div>
  )
}

export default Header