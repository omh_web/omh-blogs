import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"
import CountryPicker from '../../countryPicker'

function BlogHeader({info, rootUrl}) {

  const navigationSG = [,
    {
      title: 'Find a Home',
      url: 'https://omh.sg',
      code: 'menu-1'
    },
    {
      title: 'Post a Property',
      url: 'https://omh.sg/post-a-property',
      code: 'menu-2'
    },
    {
      title: 'Agent Service',
      url: 'https://ohmyhome.com/en-sg/property-agent-services',
      code: 'menu-4'
    },
    {
      title: 'Renovation',
      url: 'https://renovation.ohmyhome.com/en-sg',
      code: 'menu-5'
    },
    {
      title: 'Home Services',
      url: 'https://omh.sg/services',
      code: 'menu-6'
    },
  ]

  const navigationPH = [,
    {
      title: 'New Properties',
      url: 'https://ohmyhome.com/en-ph/property-investments',
      code: 'menu-1'
    },
    {
      title: 'Agent Services',
      url: 'https://omh.sg/blog/condo',
      code: 'menu-2'
    },
    {
      title: 'Resale Properties',
      url: 'https://ohmyhome.com/en-ph/resale-properties',
      code: 'menu-3'
    },
    {
      title: 'About Us',
      url: 'https://ohmyhome.com/en-ph/about-us',
      code: 'menu-4'
    },
    {
      title: 'Blogs',
      url: '/',
      code: 'menu-5'
    },
  ]

  const navigationMY = [,
    {
      title: 'New Launches',
      url: 'https://ohmyhome.com.my/new-launches',
      code: 'menu-1'
    },
    {
      title: 'Find a Home',
      url: 'https://ohmyhome.com.my/find-a-home',
      code: 'menu-2'
    },
    {
      title: 'Post a Property',
      url: 'https://ohmyhome.com.my/post-a-property',
      code: 'menu-4'
    },
    {
      title: 'Get an Agent',
      url: 'https://ohmyhome.com.my/get-an-agent',
      code: 'menu-5'
    },
  ]

  const categoryLinksSG = [,
    {
      title: 'Featured',
      url: '/en-sg/category/featured',
      code: 'menu-1'
    },
    {
      title: 'HDB',
      url: '/en-sg/category/hdb-blog',
      code: 'menu-2'
    },
    {
      title: 'Condo',
      url: '/en-sg/category/condo',
      code: 'menu-3'
    },
    {
      title: 'Financing',
      url: '/en-sg/category/financing',
      code: 'menu-4'
    },
    {
      title: 'Town',
      url: '/en-sg/category/hot-towns',
      code: 'menu-5'
    },
    {
      title: 'Tips & Guide',
      url: '/en-sg/category/hdb-tips-and-tricks',
      code: 'menu-6'
    },
    {
      title: 'Home Renovation',
      url: '/en-sg/category/home-renovation',
      code: 'menu-7'
    }
  ]

  const categoryLinksMY = [,
    {
      title: 'Featured',
      url: '/en-my/category/featured',
      code: 'menu-1'
    },
    {
      title: 'Investor Guides',
      url: '/en-my/category/investor-guides',
      code: 'menu-2'
    },
    {
      title: 'Property Guides',
      url: '/en-my/category/property-guides',
      code: 'menu-3'
    },
    {
      title: 'User Guides',
      url: '/en-my/category/user-guides',
      code: 'menu-4'
    },
    {
      title: 'Financial',
      url: '/en-my/category/financial-planning-MY',
      code: 'menu-5'
    },
    {
      title: 'Lifestyle',
      url: '/en-my/category/lifestyle',
      code: 'menu-7'
    }
  ]
  const categoryLinksPH = [,
    {
      title: 'Property Guides',
      url: '/en-ph/category/property-guides-Philippines',
      code: 'menu-1'
    },
    {
      title: 'Lifestyle',
      url: '/en-ph/category/lifestyle-Philippines',
      code: 'menu-2'
    },
    {
      title: 'Featured',
      url: '/en-ph/category/featured-Philippines',
      code: 'menu-3'
    },
    {
      title: 'Investing',
      url: '/en-ph/category/finance-and-investments',
      code: 'menu-4'
    },
    {
      title: 'Location Guides',
      url: '/en-ph/category/location-guides',
      code: 'menu-5'
    },
    {
      title: 'OMH News',
      url: '/en-ph/category/Philippines-property-news',
      code: 'menu-6'
    },
  ]


  const logo = (
    <svg width="162" height="31" viewBox="0 0 162 31" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
      <path d="M16.8171 16.241C16.8171 21.3074 13.204 25.0062 8.40857 25.0062C3.61314 25.0062 0 21.3074 0 16.241C0 11.1746 3.61823 7.48877 8.40857 7.48877C13.1989 7.48877 16.8171 11.1355 16.8171 16.241ZM12.4854 16.241C12.4854 13.1152 10.5285 11.5523 8.40857 11.5523C6.28859 11.5523 4.33169 13.1282 4.33169 16.241C4.33169 19.3537 6.28859 20.9297 8.40857 20.9297C10.5285 20.9297 12.4854 19.3537 12.4854 16.241" fill="#EC6423"/>
      <path d="M23.7682 24.4853H19.4365V0.210938H23.7682V9.37731C24.8104 8.10355 26.5711 7.55394 28.0999 7.55394C32.1768 7.55394 34.0674 10.4713 34.0674 14.0946V24.4853H29.7357V14.8474C29.7357 13.0032 28.8567 11.5602 26.7698 11.5602C24.9429 11.5602 23.8676 12.9694 23.7708 14.7797L23.7682 24.4853Z" fill="#EC6423"/>
      <path d="M37.6552 24.4853V7.99157H41.7881V10.0051C42.6672 8.39532 44.7234 7.52271 46.4841 7.52271C48.6678 7.52271 50.4285 8.49691 51.2439 10.2734C52.5179 8.25987 54.2098 7.52271 56.34 7.52271C59.3085 7.52271 62.1445 9.36692 62.1445 13.7951V24.4853H57.9351V14.699C57.9351 12.9199 57.0866 11.5732 55.0991 11.5732C53.2416 11.5732 52.1332 13.0501 52.1332 14.8266V24.4853H47.8295V14.699C47.8295 12.9199 46.9504 11.5732 44.9935 11.5732C43.1029 11.5732 41.9945 13.0163 41.9945 14.8266V24.4853H37.6552Z" fill="#EC6423"/>
      <path d="M66.3768 31.0001L70.1606 22.412L63.3446 7.9917H68.2037L72.4743 17.6504L76.4212 7.9917H81.051L70.976 31.0001H66.3768Z" fill="#EC6423"/>
      <path d="M87.2122 24.4853H82.8805V0.210938H87.2122V9.37731C88.2543 8.10355 90.015 7.55394 91.5439 7.55394C95.6207 7.55394 97.5114 10.4713 97.5114 14.0946V24.4853H93.1797V14.8474C93.1797 13.0032 92.3006 11.5602 90.2138 11.5602C88.3868 11.5602 87.3116 12.9694 87.2147 14.7797L87.2122 24.4853Z" fill="#EC6423"/>
      <path d="M116.716 16.241C116.716 21.3048 113.098 24.9958 108.307 24.9958C103.517 24.9958 99.8835 21.3074 99.8835 16.241C99.8835 11.1746 103.502 7.48877 108.292 7.48877C113.082 7.48877 116.701 11.1355 116.701 16.241H116.716ZM112.369 16.241C112.369 13.1152 110.412 11.5523 108.292 11.5523C106.172 11.5523 104.215 13.1282 104.215 16.241C104.215 19.3537 106.172 20.9297 108.292 20.9297C110.412 20.9297 112.369 19.3537 112.369 16.241" fill="#EC6423"/>
      <path d="M119.323 24.4853V7.99157H123.463V10.0051C124.342 8.39532 126.399 7.52271 128.159 7.52271C130.343 7.52271 132.104 8.49691 132.919 10.2734C134.193 8.25987 135.885 7.52271 138.015 7.52271C140.984 7.52271 143.82 9.36692 143.82 13.7951V24.4853H139.605V14.699C139.605 12.9199 138.757 11.5732 136.769 11.5732C134.912 11.5732 133.803 13.0501 133.803 14.8266V24.4853H129.497V14.699C129.497 12.9199 128.618 11.5732 126.661 11.5732C124.77 11.5732 123.662 13.0163 123.662 14.8266V24.4853H119.323Z" fill="#EC6423"/>
      <path d="M161.773 19.7628C160.958 22.6151 158.415 25.0063 154.468 25.0063C150.067 25.0063 146.187 21.7529 146.187 16.1864C146.187 10.922 149.968 7.49927 154.086 7.49927C159.042 7.49927 162.01 10.7527 162.01 16.0509C162.01 16.6891 161.944 17.3533 161.944 17.4262H150.478C150.577 19.6065 152.368 21.1824 154.521 21.1824C156.542 21.1824 157.65 20.1405 158.173 18.6661L161.773 19.7628ZM157.724 14.363C157.658 12.7193 156.616 11.1096 154.137 11.1096C153.245 11.0782 152.376 11.3985 151.708 12.004C151.041 12.6095 150.627 13.4541 150.552 14.363H157.724Z" fill="#EC6423"/>
      <path d="M3.2666 28.0644L11.3567 24.4932H5.91403L3.2666 28.0644Z" fill="#EC6423"/>
      <path d="M16.8248 4.06613L8.41621 0L0 4.03487V8.14528L8.40857 4.11562L16.8248 8.18436V4.06613Z" fill="#EC6423"/>
      </g>
      <defs>
      <clipPath id="clip0">
      <rect width="162" height="31" fill="white"/>
      </clipPath>
      </defs>
    </svg>
  )

  const router = useRouter()

  return (
    <React.Fragment>
      <div className="header-wrapper">
        <div className="header-top">
          <div className="header-container_top"> 
            <div className="country-container"><CountryPicker /></div>
            <div className="number-container">
              { router.query.lang_country === 'en-sg' ? <a href="tel:+6597272131">+65 9727 2131</a> : '' }
              { router.query.lang_country === 'en-my' ? <a href="tel:+60192665593">+60 19-266 5593</a> : '' }
              { router.query.lang_country === 'en-ph' ? <a href="tel:+639177006497">+63 917 700 6497</a> : '' }
            </div>
          </div>
        </div>
        <div className="header-middle">
          { router.query.lang_country === 'en-sg' ?
            <div className="header-container_middle"> 
              <div className="header-image-wrapper">
                <a href="https://omh.sg">{logo}</a>
              </div>
              <div className="header-navigation-wrapper">
                {
                  navigationSG && navigationSG.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                        <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
          }
          { router.query.lang_country === 'en-ph' ?
            <div className="header-container_middle"> 
              <div className="header-image-wrapper">
                <a href="https://ohmyhome.com/en-ph">{logo}</a>
              </div>
              <div className="header-navigation-wrapper">
                {
                  navigationPH && navigationPH.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                        <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
          }
          { router.query.lang_country === 'en-my' ?
            <div className="header-container_middle"> 
              <div className="header-image-wrapper">
                <a href="https://ohmyhome.com.my">{logo}</a>
              </div>
              <div className="header-navigation-wrapper">
                {
                  navigationMY && navigationMY.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null} navigation-item`}>
                        <Link prefetch={false} href={`${url}?utm_source=blog&utm_medium=${info.slug}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
          }
        </div>
        <div className="header-bottom">
          { router.query.lang_country === 'en-sg' ?
            <div className="header-container_bottom">
              <div className="header-container-title">
                <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
              </div>
              <div className="header-container_bottom">
                {
                  categoryLinksSG && categoryLinksSG.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                        <Link prefetch={false} href={url}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
          }
          { router.query.lang_country === 'en-my' ?
            <div className="header-container_bottom">    
              <div className="header-container-title">
                <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
              </div>
              <div className="header-container_bottom">    
                {
                  categoryLinksMY && categoryLinksMY.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                        <Link prefetch={false} href={url}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
          }
          { router.query.lang_country === 'en-ph' ?
            <div className="header-container_bottom">  
              <div className="header-container-title">
                <a href={`${rootUrl}/${router.query.lang_country}`}>Blogs</a>
              </div> 
              <div className="header-container_bottom">         
                {
                  categoryLinksPH && categoryLinksPH.map( item => {
                    const { title, url, code } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                        <Link prefetch={false} href={url}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                            <span className="border-item" />
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
        }
        </div>
      </div>
      <style jsx>{`

        .header-bottom {
          background: #F7F6F7;
        }
        @media screen and (min-width: 768px) {
          header {
            margin: 0 0 24px;
          }
          .header-top {
            padding: 10px;
            z-index: 12;
            position: relative;
          }
          .header-middle {
            border-bottom: 1px solid #F2F2F2;
          }
          .header-container_top {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            margin: 0 auto;
            max-width: 1500px;
          }
          .number-container a {
            text-decoration: none;
            margin-left: 30px;
            font-size: 14px;
          }
          .header-container_middle {
            display: flex;
            justify-content: flex-start;
            align-items: flex;
            margin: 0 auto;
            max-width: 1500px;
          }
          .header-navigation-wrapper {
            display: flex;
            justify-content: flex-start;
          }
          .header-image-wrapper a {
            padding: 0;
          }
          .header-navigation-wrapper a {         
            color: #1C1C1C;
            font-weight: 600;
            font-size: 14px;
            line-height: 14px;
            text-decoration: none;
            padding: 30px 15px;
            transition: all .2s ease-out .1s;
            display: block;
          } 
          .header-navigation-wrapper .navigation-item {         
            transition: all .2s ease-out .1s;
            border-bottom: 5px solid #FFF;
          } 
          .header-navigation-wrapper .navigation-item:hover {         
            border-bottom: 5px solid #D8571D;
          }
          .header-navigation-wrapper .active {         
            border-bottom: 5px solid #D8571D;
          } 
          
          .header-image-wrapper {
            padding: 24px 40px 19px;
          }
          .header-container_bottom {
            max-width: 1016px;
            margin: 0 auto;
            display: flex;
            justify-content: flex-start;
            align-items: center;
          }
          .header-container_bottom a {         
            color: #1C1C1C;
            font-weight: 700;
            font-size: 14px;
            line-height: 14px;
            text-decoration: none;
            padding: 21px 15px;
            transition: all .2s ease-out .1s;
            display: block;
          }

          
          .header-container_bottom a:hover {         
            color: #EC6423;
          }
        }
        @media screen and (min-width: 1025px) {
          .header-wrapper {
            height: 100%;
          }
          .header-bottom {
            position: sticky;
            top: 0;
            z-index: 11;
          } 
          .header-container-title a {
            padding: 24px 40px 24px 40px;
            color: #1C1C1C;
            font-size: 20px;
            line-height: 23px;
            letter-spacing: -0.250037px;
            font-weight: 700;
            margin-right: 0px;
            display: block;
          }
        }
        @media screen and (min-width: 1301px) { 
          .header-container-title a {
            padding: 24px 120px 19px 40px;
            margin-right: 108px;
          }
        }
      `}</style>    
    </React.Fragment>
  )
}

export default BlogHeader
