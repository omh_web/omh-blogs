const path = require('path')
require('dotenv').config()

module.exports = {
    env: {
        ENV_VAR: process.env.ENV_VAR,
        OMH_PUBLIC_API_KEY: process.env.OMH_PUBLIC_API_KEY,
        omhweb_auth: process.env.omhweb_auth,
        INTERCOM_ID: process.env.INTERCOM_ID,
        host_cms: process.env.host_cms
    },
    serverRuntimeConfig: {
        ENV_VAR: process.env.ENV_VAR,
        OMH_PUBLIC_API_KEY: process.env.OMH_PUBLIC_API_KEY,
        omhweb_auth: process.env.omhweb_auth,
        INTERCOM_ID: process.env.INTERCOM_ID,
        host_cms: process.env.host_cms
    },
    publicRuntimeConfig: {
        ENV_VAR: process.env.ENV_VAR,
        OMH_PUBLIC_API_KEY: process.env.OMH_PUBLIC_API_KEY,
        omhweb_auth: process.env.omhweb_auth,
        INTERCOM_ID: process.env.INTERCOM_ID,
        host_cms: process.env.host_cms
    },
    amp: {
        validator: './custom_validator.js',
    },

    webpack: config => {
        config.resolve.alias['components'] = path.join(__dirname, 'components')
        config.resolve.alias['public'] = path.join(__dirname, 'public')
        config.resolve.alias['styles'] = path.join(__dirname, 'styles')
        config.resolve.alias['modules'] = path.join(__dirname, 'modules')
        config.resolve.alias['static'] = path.join(__dirname, 'static')
        config.resolve.alias['services'] = path.join(__dirname, 'services')
        return config
    }
}