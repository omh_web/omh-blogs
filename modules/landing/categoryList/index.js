import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

function CategoryList({category, Desktop, Mobile, title, description, slug}) {

  const router = useRouter()

  return (
    <div className="category-container">
      <div className="header-wrapper">
        <h2>{title}</h2>
        <span>{description}</span>
      </div>
      <div className="category-content_wrapper">
        {
          category && category.data && category.data.map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))            
            return (
              <React.Fragment key={item.id}>
                <div className="category-card">
                  <div className="category-card-image">
                    <amp-img
                      width="256"
                      height="225"
                      src={item.featuredImages[0].small}
                      alt={item.title}
                      layout="responsive"
                    />  
                  </div>
                  <div className="category-content">
                  <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                    <div className="learn-more">
                      Learn more
                      <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5036 6.42969L9.0036 7.92969L13.5739 12.5L9.0036 17.0703L10.5036 18.5703L16.5739 12.5L10.5036 6.42969Z" fill="#E86225"/>
                      </svg>
                    </div>
                  </div>
                  <a href={`/${slug}/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          }
          )
        }
      </div>
      <div className="category-button">
        <a href={`/${slug}`}> View All {title}</a>
      </div>
      <style jsx>{`
          .category-container {
            padding: 0 20px;
            margin: 0 0 37px;
          }
          .header-wrapper {
            margin: 0 0 32px;
          }
          h2 {
            font-size: 24px;
            line-height: 28px;
            color: #1C1C1C;
            margin: 0 0 8px;
            
          }
          .category-content_wrapper {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
          }
          .category-card {
            
            box-sizing: border-box;
            border-radius: 5px;
            background: #FFF;
            max-width: 392px;
            overflow: hidden;
            position: relative;
            background: #FFF;
            width: 49%;
            margin: 0 0 38px;
          }
          .category-card:last-child {
          
          }
          .category-card-image {
            overflow: hidden;
          }
          .category-card-image img {
            object-fit: cover;
            width: 100%;
            height: 100%;
          }
          .category-content {
            padding: 16px 16px 14px;
          }
          span {
            margin: 0 0 8px;
            display: block;
            color: #5F5F5F;
            font-size: 13px;
            line-height: 15px;
            letter-spacing: 0.1em;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            color: #1C1C1C;
            margin: 0 0 4px;
            font-family: Tinos;
            font-weight: 600;

          }
          a {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          a span {
            display: none;
          }
          p {
            margin: 0;
            font-size: 16px;
            line-height: 24px;
            color: #343434;
          }
          .learn-more {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            color: #e86225;
            font-size: 14px;
            line-height: 14px;
            font-weight: 600;
            margin: 20px 0 0;
          }
          .header-wrapper span {
            font-size: 14px;
            line-height: 18px;
            font-weight: 400;
            letter-spacing: 0;
          }
          .category-button {
            display: flex;
            justify-content: center;
          }
          .category-button a {
            font-size: 16px;
            line-height: 14px;
            border: 1px solid #E86225;
            color: #E86225;
            padding: 16px 25px;
            border-radius: 5px;
            text-decoration: none;
            display: inline-block;
            font-variation-settings: "wght" 550;
            transition: all 100ms ease 0s;
            cursor: pointer;
            position: relative;
            margin: 32px 0 0;
            max-width: 300px;
            width: 100%;
            box-sizing: border-box;
            text-align: center;
          }
          .category-button a:hover {
            border: 1px solid rgba(232, 98, 37, 0.6);
          }
        @media only screen and (min-width: 768px) { 
          .category-container {
            padding: 0 40px;
            margin: 0 0 80px;
          }
          h2 {
            font-size: 40px;
            line-height: 47px;
            margin: 0 0 12px;
          }
          .category-content_wrapper {
            flex-wrap: nowrap;
          }
          .category-card {
            min-width: 220px;
            width: 32%;
            margin: 0;
            border: 1px solid #E0E0E0;
          }
          .category-card:last-child {
            display: none;
          }
          .category-content {
            padding: 16px 16px 26px;
          }
          h3 {
            font-size: 20px;
            line-height: 26px;
            margin: 0 0 8px;
          }
          p {
            font-size: 18px;
            line-height: 27px;
          }
          .header-wrapper span {
            font-size: 18px;
            line-height: 22px;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .category-container {
            margin: 0 0 121px;
            padding: 0 16px;
          }
        }
        @media screen and (min-width: 1441px) {
          .category-card {
            max-width: 100%;
          }
        }
      `}</style>    
    </div>
  )
}

export default CategoryList
