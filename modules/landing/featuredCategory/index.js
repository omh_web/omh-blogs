import React from 'react'
import Link from 'next/link'
import { useRouter, withRouter } from "next/router"

function FeaturedCategory({rootUrl}) {

  const categoryLinksSG = [,
    {
      title: 'Featured',
      description: 'Highlighting unique property views for every Singaporean',
      url: 'featured',
      code: 'menu-1',
    },
    {
      title: 'HDB',
      description: 'Your one-stop guide for must-know HDB tips',
      url: 'hdb-blog',
      code: 'menu-2'
    },
    {
      title: 'Condo',
      description: 'Your one-stop guide for must-know condo tips',
      url: 'condo',
      code: 'menu-3'
    },
    {
      title: 'Financing',
      description: 'Your financial guide for property investments',
      url: 'financing',
      code: 'menu-4'
    },
    {
      title: 'Town',
      description: 'Find out which town fits your lifestyle the best',
      url: 'hot-towns',
      code: 'menu-5'
    },
    {
      title: 'Tips & Guide',
      description: 'For home DIY tricks, home-staging, and more',
      url: 'hdb-tips-and-tricks',
      code: 'menu-6'
    },
    {
      title: 'Home Renovation',
      description: 'Your one-stop guide for must-know reno tips',
      url: 'home-renovation',
      code: 'menu-7'
    }
  ]

  const categoryLinksMY = [,
    {
      title: 'Featured',
      description: 'Highlighting unique property views for every Malaysian',
      url: 'featured',
      code: 'menu-1'
    },
    {
      title: 'Investor Guides',
      description: 'Your one-stop guide for property investments',
      url: 'investor-guides',
      code: 'menu-2'
    },
    {
      title: 'Property Guides',
      description: 'A must-know resource for all things property',
      url: 'property-guides',
      code: 'menu-3'
    },
    {
      title: 'User Guides',
      description: 'Walking you through each stage of your home journey',
      url: 'user-guides',
      code: 'menu-4'
    },
    {
      title: 'Financial',
      description: 'Your financial guide for property investments',
      url: 'financial-planning-MY',
      code: 'menu-5'
    },
    {
      title: 'Lifestyle',
      description: 'A lifestyle catalogue for all your home needs',
      url: 'lifestyle',
      code: 'menu-7'
    }
  ]
  const categoryLinksPH = [,
    {
      title: 'Property Guides',
      description: 'Your guide to the different types of property in the Philippines',
      url: 'property-guides-Philippines',
      code: 'menu-1'
    },
    {
      title: 'Lifestyle',
      description: 'There`s more to property than buying and selling',
      url: 'lifestyle-Philippines',
      code: 'menu-2'
    },
    {
      title: 'Featured',
      description: 'Highlighting real estate developers and developments in the Philippines',
      url: 'featured-Philippines',
      code: 'menu-3'
    },
    {
      title: 'Investing',
      description: 'Your guide to real estate financing and investments',
      url: 'finance-and-investments',
      code: 'menu-4'
    },
    {
      title: 'Location Guides',
      description: 'Get to know the different places to live and invest in the Philippines',
      url: 'location-guides',
      code: 'menu-5'
    },
    {
      title: 'OMH News',
      description: 'The latest about Ohmyhome and Philippine real estate',
      url: 'Philippines-property-news',
      code: 'menu-6'
    },
  ]

  const chevronRight = (
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.84456 19.1973C9.64929 19.0021 9.64929 18.6855 9.84456 18.4902L14.9137 13.4211L9.84456 8.35201C9.64929 8.15675 9.64929 7.84017 9.84456 7.64491L10.343 7.14645C10.5383 6.95119 10.8549 6.95119 11.0501 7.14645L16.8536 12.9499C16.9813 13.0777 17.0255 13.2574 16.986 13.4211C17.0255 13.5848 16.9813 13.7646 16.8536 13.8924L11.0501 19.6958C10.8549 19.8911 10.5383 19.8911 10.343 19.6958L9.84456 19.1973Z" fill="#151515"/>
    </svg>
  )

  const chevronRightHover = (
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.84456 19.1973C9.64929 19.0021 9.64929 18.6855 9.84456 18.4902L14.9137 13.4211L9.84456 8.35201C9.64929 8.15675 9.64929 7.84017 9.84456 7.64491L10.343 7.14645C10.5383 6.95119 10.8549 6.95119 11.0501 7.14645L16.8536 12.9499C16.9813 13.0777 17.0255 13.2574 16.986 13.4211C17.0255 13.5848 16.9813 13.7646 16.8536 13.8924L11.0501 19.6958C10.8549 19.8911 10.5383 19.8911 10.343 19.6958L9.84456 19.1973Z" fill="#E55710"/>
    </svg>
  )

  const router = useRouter()

  return (
    <div className="featured-container">
      <div className="featured-container_wrapper">
        <h3>TOP CATEGORIES</h3>
        { router.query.lang_country === 'en-sg' ?
            <div className="header-bottom-container">
              <div className="header-container_bottom">
                {
                  categoryLinksSG && categoryLinksSG.map( item => {
                    const { title, url, code, description } = item
                    return(
                      <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                         <Link prefetch={false} href={`/${router.query.lang_country}/category/${url}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <div className="data">
                              <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                              <span className="icon">
                                <div className="normal">{chevronRight}</div>
                                <div className="hover">{chevronRightHover}</div>
                              </span>
                            </div>
                            <p>{description}</p>
                          </a>
                        </Link>
                      </span>
                    )
                  })
                }
              </div>
            </div> : ''
      }
      { router.query.lang_country === 'en-my' ?
          <div className="header-bottom-container">
            <div className="header-container_bottom">
              {
                categoryLinksMY && categoryLinksMY.map( item => {
                  const { title, url, code, description } = item
                  return(
                    <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                      <Link prefetch={false} href={`/${router.query.lang_country}/category/${url}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <div className="data">
                              <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                              <span className="icon">
                                <div className="normal">{chevronRight}</div>
                                <div className="hover">{chevronRightHover}</div>
                              </span>
                            </div>
                            <p>{description}</p>
                          </a>
                      </Link>
                    </span>
                  )
                })
              }
            </div>
          </div> : ''
      }
      { router.query.lang_country === 'en-ph' ?
          <div className="header-bottom-container">
            <div className="header-container_bottom">
              {
                categoryLinksPH && categoryLinksPH.map( item => {
                  const { title, url, code, description } = item
                  return(
                    <span className={`${router.asPath === url || router.asPath === code ? `active` : null}`}>
                       <Link prefetch={false} href={`/${router.query.lang_country}/category/${url}`}>
                          <a className={`link-item-container h-full relative block  ${code}`}>
                            <div className="data">
                              <span className="flex items-center px-headerNav h-full link-item">{title}</span>
                              <span className="icon">
                                <div className="normal">{chevronRight}</div>
                                <div className="hover">{chevronRightHover}</div>
                              </span>
                            </div>
                            <p>{description}</p>
                          </a>
                      </Link>
                    </span>
                  )
                })
              }
            </div>
          </div> : ''
        }
      </div>
      <style jsx>{`
        .header-container_bottom {
          background: #FEF4EE;
          border-radius: 10px;
          padding: 16px;
        }
        
        a {
          font-size: 16px;
          line-height: 21px;
          color: #1C1C1C;
          font-family: Tinos;
          font-weight: 600;
        }
        .data {
          display: flex;
          align-items: center;
          justify-content: flex-start;
          flex-wrap: nowrap;
        }
        .data span {
          font-size: 18px;
        }
        .icon {
          margin-left: 15px;
          position: relative;
          height: 27px;
        }
        .icon .hover {
          position: absolute;
          left: 0;
          right: 0;
          top:0;
          bottom: 0;
          opacity: 0;
          transition: all .2s ease 0s;
        }
        .header-container_bottom a:hover .icon .hover {
          opacity: 1;
        }
        p {
          margin: 4px 0 24px;
          color: #343434;
          font-size: 14px;
          line-height: 21px;
          font-family: Public Sans VF,sans-serif;
          font-weight: 300;
          transition: all .2s ease 0s;
        }
        .header-container_bottom a:hover p {
          color: #E55710;
        }
       
        h3 {
          font-weight: 600;
          font-size: 13px;
          line-height: 15px;
          display: flex;
          align-items: center;
          letter-spacing: 0.1em;
          color: #5F5F5F;
          margin: 0 0 24px;
          padding: 0 16px;
        }
        @media only screen and (min-width: 768px) { 
          h3 {
            margin: 0 0 16px;
            padding: 32px 0 0;
            border-top: 1px solid rgba(0,0,0,0.2);
          }
        }
        @media only screen and (min-width: 1025px) { 
         
        }
      `}</style>    
    </div>
  )
}

export default FeaturedCategory
