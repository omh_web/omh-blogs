import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

const learnMore = (
  <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.365 5.92969L8.84119 7.42969L13.484 12L8.84119 16.5703L10.365 18.0703L16.5317 12L10.365 5.92969Z" fill="#E86225"/>
  </svg>
)

function ListBlogs({list, Desktop, Mobile, title, description}) {

  const router = useRouter()

  return (
    <div className="list-container">
      <div className="list-wrapper">
        {
          list && list.data && list.data.slice(2, 6).map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))
            return (
              <React.Fragment key={item.id}>
                <div className="list-card">
                  <div className="list-card-image">
                    <amp-img
                      width="600"
                      height="334"
                      src={item.featuredImages && item.featuredImages[0].medium}
                      alt={item.title}
                      layout="fill"
                    />
                  </div>
                  <div className="list-content">
                  <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                    <div className="learn-more">
                      Learn More
                      {learnMore}
                    </div>
                  </div>
                  <a href={`en-sg/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          })
        }
        <div className="divider">&nbsp;</div>
      </div>
      <style jsx>{`
        .list-wrapper {
          display: grid;
          grid-template-areas:
            'featured featured featured'
            'list-1 list-1 list-1'
            'list-2 list-2 list-2'
            'list-3 list-3 list-3';
          grid-gap: 8px;
          box-sizing: border-box;
          padding: 0 16px;
        }
        .list-card:first-child { 
          grid-area: featured; 
          padding: 16px 0 21px;
          position: relative;
        }
        .list-card:nth-child(2n) { 
          grid-area: list-1; 
        }
        .list-card:nth-child(3n) { 
          grid-area: list-2; 
        }
        .list-card:nth-child(4n) { 
          grid-area: list-3; 
        }
        .list-card:nth-child(n+2) {
          padding: 16px 0 21px;
          position: relative;
          display: flex;
          flex-wrap: nowrap;
          flex-flow: row-reverse;
          justify-content: space-between;
          margin: 0 0 21px;
        }
        .list-card:first-child .list-card-image { 
          height: 224px;
          overflow: hidden;
          border-radius: 10px;
          margin: 0 0 16px;
          position: relative;
        }
        .list-card:nth-child(n+2) .list-card-image { 
          height: 104px;
          overflow: hidden;
          border-radius: 10px;
          margin: 0;
          margin-left: 18px;
          min-width: 104px;
          position: relative;
        }
        .list-card:first-child .list-card-image img { 
          object-position: center;
          object-fit: cover;
          margin: 0;
          width: 100%;
          height: 224px;
        }
        .list-card:nth-child(n+2) .list-card-image img { 
          object-position: center;
          object-fit: cover;
          margin: 0;
          width: 100%;
          height: 104px;
          max-width: 104px;
        }
        .list-card .list-content span { 
          font-size: 12px;
          line-height: 14px;
          font-variation-settings: 'wght' 450;
          display: block;
          letter-spacing: 0.1em;
           margin: 0 0 8px;
        }
        .list-card:first-child .list-content h3 { 
          font-size: 18px;
          line-height: 27px;
          color: #1C1C1C;
          margin: 0 0 8px;
          font-family: Tinos;
            font-weight: 600;
        }
        .list-card:nth-child(n+2) .list-content h3 {
          font-size: 14px;
          line-height: 18px;
          color: #1C1C1C;
          margin: 0 0 8px;
          font-family: Tinos;
            font-weight: 600;
        }
        .list-card:first-child .list-content p { 
          font-size: 14px;
          line-height: 21px;
          color: #343434;
          margin: 0 0 16px;
        }
        .list-card:nth-child(n+2) .list-content p {
          display: none;
        }
        .learn-more {
          display: flex;
          align-content: center;
          flex-wrap: nowrap;
          font-size: 14px;
          line-height: 14px;
          color: #E86225;
          align-items: center;
          font-variation-settings: 'wght' 450;
        }
        a {
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          opacity: 0;
        }
        .divider {
          display: none;
        }
        @media only screen and (min-width: 768px) { 
          .list-wrapper {
            grid-template-areas:
              'featured divider list-1'
              'featured divider list-2'
              'featured divider list-3';
            column-gap: 24px;
            row-gap: 8px;
            grid-template-columns: 50%;
            padding: 0 40px;
          }
          .list-card:first-child { 
            padding: 0;
          }
          .list-card:nth-child(n+2) {
            padding: 0;
          }
          .list-card:first-child .list-card-image { 
            height: 278px;
            margin: 0 0 24px;
            
          }
          .list-card:nth-child(n+2) .list-card-image { 
            height: 108px;
            margin-left: 24px;
            min-width: 108px;
            position: relative;
          }
          .list-card:first-child .list-card-image img { 
            height: 278px;
          }
          .list-card:nth-child(n+2) .list-card-image img { 
            height: 108px;
            max-width: 108px;
          }
          .list-card .list-content span { 
            font-size: 13px;
            line-height: 15px;
            
          }
          .list-card:first-child .list-content h3 { 
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 8px;
          }
          .list-card:first-child .list-content p { 
            margin: 0 0 24px;
          }
          .list-card:nth-child(n+2) .list-content h3 { 
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 8px;
          }
          .list-card:nth-child(n+2) .list-content p { 
            font-size: 12px;
            line-height: 16px;
            margin: 0 0 8px;
            display: block;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .list-wrapper {
            grid-template-areas:
              'featured divider list-1'
              'featured divider list-2'
              'featured divider list-3';
            column-gap: 29px;
            row-gap: 32px;
            grid-template-columns: 50% 1px auto;
            padding: 0 16px;
          }
          
          .list-card:first-child .list-card-image { 
            height: 320px;
            margin: 0 0 16px;
          }
          .list-card:nth-child(n+2) {
            margin: 0 8px 0 0;
          }
          .list-card:nth-child(n+2) .list-card-image { 
            height: 177px;
            margin-left: 44px;
            min-width: 177px;
          }
          .list-card:first-child .list-card-image img { 
            height: 320px;
          }
          .list-card:nth-child(n+2) .list-card-image img { 
            height: 177px;
            max-width: 177px;
          }
          .list-card .list-content span { 
            font-size: 13px;
            line-height: 15px;
 
           
          }
          .list-card:first-child .list-content h3 { 
            font-size: 38px;
            line-height: 44px;
            margin: 0 0 8px;
          }
          .list-card:first-child .list-content p { 
            font-size: 16px;
            line-height: 24px;
            margin: 0 0 24px;
          }
          .list-card:nth-child(n+2) .list-content h3 { 
            font-size: 20px;
            line-height: 26px;
            margin: 0 0 8px;
          }
          .list-card:nth-child(n+2) .list-content p { 
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 14px;
            display: block;
          }
          .divider {
            display: block;
            grid-area: divider; 
            background: #000;
            opacity: 0.2;
            width: 1px;
          }
        }
      `}</style>    
    </div>
  )
}

export default ListBlogs
