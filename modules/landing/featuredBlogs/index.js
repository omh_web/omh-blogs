import React from 'react'
import moment from 'moment'

const learnMore = (
  <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.365 5.92969L8.84119 7.42969L13.484 12L8.84119 16.5703L10.365 18.0703L16.5317 12L10.365 5.92969Z" fill="#E86225"/>
  </svg>
)

function FeaturedBlogs({featured, Desktop, Mobile, title, description}) {
  return (
    <div className="featured-container">
      <div className="featured-wrapper">
        {
          featured && featured.data && featured.data[0]?.posts && featured.data[0].posts.slice(0, 3).map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))
            return (
              <React.Fragment key={item.id}>
                <div className="featured-card">
                  <div className="featured-card-image">
                    <amp-img
                      width="568"
                      height="320"
                      src={item.featuredImages && item.featuredImages[0].large}
                      alt={item.title}
                      layout="fill"
                    />
                  </div>
                  <div className="featured-content">
                  <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                    <div className="learn-more">
                      Learn More
                      {learnMore}
                    </div>
                  </div>
                  <a href={`en-sg/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          }
          )
        }
      </div>
      <style jsx>{`
          .featured-container {
 
          }
          .header-wrapper {
            margin: 0 0 32px;
          }
          h2 {
            font-size: 24px;
            line-height: 28px;
            color: #1C1C1C;
            margin: 0 0 8px;
          }
          .featured-wrapper {
            display: block;
          }
          .featured-card {
            box-sizing: border-box;
            border-radius: 5px;
            background: #FFF;
            overflow: hidden;
            position: relative;
            background: #FFF;
          }
          .featured-card:last-child {
          
          }
          .featured-card-image {
            overflow: hidden;
            height: 208px;
            border-radius: 5px;
            position: relative;
          }
          .featured-card-image img {
            object-fit: cover;
            width: 100%;
            height: 100%;
          }
          .featured-content {
            padding: 16px 16px 14px;
          }
          span {
            margin: 0 0 8px;
            display: block;
            color: #5F5F5F;
            font-size: 14px;
            font-variation-settings: 'wght' 550;
            letter-spacing: 0.1em;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            color: #1C1C1C;
            margin: 0 0 4px;
            font-family: Tinos;
            font-weight: 600;
          }
          .featured-card:first-child h3 {
            font-size: 24px;
            line-height: 28px;
          }
          a {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          a span {
            display: none;
          }
          p {
            color: #343434;
            font-size: 12px;
            line-height: 16px;
            margin: 0 0 11px;
          }
          .learn-more {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            font-size: 14px;
            line-height: 14px;
            color: #E86225;
          }
          .featured-card:nth-child(+n+2) {
            display: flex;
            flex-direction: row-reverse;
            justify-content: space-between;
          }
          .featured-card:nth-child(+n+2) .featured-card-image {
            overflow: hidden;
            height: 104px;
            border-radius: 5px;
            min-width: 104px;
            max-width: 104px;
            padding: 16px 0;
          }
          .featured-card:nth-child(+n+2):after {
            display: none;
          }
        @media only screen and (min-width: 768px) { 
          .featured-container {

          }
          h2 {
            font-size: 40px;
            line-height: 47px;
            margin: 0 0 12px;
          }
          .featured-content {
            padding: 16px 16px 0px;
          }
          span {
            font-size: 13px;
            line-height: 15px;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 8px;
            font-weight: 700;
          }
          
          p {
            font-size: 16px;
            line-height: 21px;
          }
          .featured-card:first-child p {
            font-size: 18px;
            line-height: 27px;
          }
          .featured-card {
            padding: 0 0 37px;
            border-bottom: 1px solid #E0E0E0;
            margin: 0 0 8px;
          }
          .featured-card:nth-child(+n+2) {
            padding: 16px 16px 21px;
            border-bottom: 0px solid #E0E0E0;
          }
          .featured-card:nth-child(+n+2) .featured-card-image {
            height: 147px;
            min-width: 147px;
            max-width: 147px;
            padding: 0;
          }
          .featured-card:nth-child(+n+2) .featured-content {
            padding: 0 32px 0 0;
          }
          .featured-card-image {
            height: 284px;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .featured-card {
            border: 0px solid #E0E0E0;
            padding: 16px 16px 20px;
            position: relative;
            margin: 0 0 32px;
          }
          .featured-card:after {
            content: " ";
            position: absolute;
            opacity: .2;
            left: 0;
            background: #000;
            right: 0;
            bottom: 0;
            display: block;
            width: 100%;
            height: 1px;
          }
          .featured-card-image {
            height: 320px;
          }
          .featured-card:nth-child(+n+2) {
            margin: 0;
            padding: 0px 16px 32px;
            
          }
          .featured-card:nth-child(+n+2) .featured-card-image {
            height: 177px;
            min-width: 177px;
            max-width: 177px;
          }
          h3 {
            font-size: 20px;
            line-height: 26px;
          }
          .featured-card:first-child h3 {
            font-size: 38px;
            line-height: 44px;
            font-weight: 700;
          }
          .featured-card:nth-child(+n+2) .featured-content {
            padding: 0px 16px 0px;
          }
          p {
            margin: 0 0 14px;
          }
        }
        @media screen and (min-width: 1441px) {
          .featured-card-image {
            height: 400px
          }
          .featured-card:nth-child(+n+2) .featured-card-image {
            min-width: 240px;
            max-width: 240px;
          }
        }
      `}</style>    
    </div>
  )
}

export default FeaturedBlogs
