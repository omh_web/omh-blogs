import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

const learnMore = (
  <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.365 5.92969L8.84119 7.42969L13.484 12L8.84119 16.5703L10.365 18.0703L16.5317 12L10.365 5.92969Z" fill="#E86225"/>
  </svg>
)

function PopularBlogs({popular, Desktop, Mobile, title, description}) {

  const router = useRouter()

  return (
    <div className="popular-container">
      <div className="popular-wrapper">
        <div className="popular-content">
          <amp-carousel id="carousel-with-preview"
            width="256"
            height="348"
            layout="responsive"
            type="slides"
            loop=""
            autoplay=""
            delay="3000">
              {
                popular && popular.data && popular.data[0].posts && popular.data[0].posts.map((item) => {
                  const duration = moment.duration(item.readingTime, 'seconds')
                  const hours = duration._data.hours
                  const minutes = duration._data.minutes
                  const seconds =   duration._data.seconds
                  const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
                  const finalMinutes = Math.round(moment.duration(minutes + secondsRound))
                  return (
                    <div className="popular-image">
                      <amp-img src={item.featuredImages && item.featuredImages[0].medium}
                        width="256"
                        height="348"
                        layout="fill"
                        alt="apples">
                          <div className="popular-title">
                            <div className="read-time">
                            <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                            </div>
                            <h3>{item.title}</h3>
                          </div>
                          <div className="popular-link">
                            <a href={`en-sg/${item.slug}`}><span>{item.title}</span></a>
                          </div>
                      </amp-img>
                    </div>
                  )
                })
              }
          </amp-carousel>
          
        </div>   
            
      </div>
      <style jsx>{`
        .popular-wrapper {
          display: block;
          width: 100%;
          padding: 16px 0;
        }
        .popular-content {
          position: relative;
          border-radius: 5px;
          overflow: hidden;
        }
        .popular-container {
          margin: 0 0 16px;
          display: block;
        }
        .tab-panels {
          position: relative;
        }
        .popular-image {
          border-radius: 5px;
          overflow: hidden;
        }
        .popular-image img {
          object-fit: cover;
          margin: 0;
          padding: 0;
          width: 100%;
          height: 418px;
        }
        .popular-title {
          position: absolute;
          bottom: 0;
          padding: 0 16px;
          z-index: 2;
        }
        .read-time {
          font-size: 12px;
          line-height: 14px;
          color: #FFF;
          margin: 0 0 8px;
          letter-spacing: 0.1em;
        }
        h3 {
          color: #FFF;
          font-size: 24px;
          line-height: 29px;
          font-family: Tinos;
            font-weight: 600;
        }
        .popular-link a {
          position: absolute;
          bottom: 0;
          top: 0;
          left: 0;
          right: 0;
          z-index: 3;
        }
        .popular-link a span {
          opacity: 0;
        }
        .popular-image:before {
          content: "1";
          background: linear-gradient(180deg, rgba(85, 85, 85, 0) 0%, #000000 100%);
          text-indent: -999999px;
          overflow: hidden;
          position: absolute;
          z-index: 1;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
        }
        
        @media only screen and (min-width: 768px) { 
          .popular-wrapper {

            padding: 16px 28px;
          }
     
          h3 {
            font-size: 16px;
            line-height: 21px;
            padding: 0 0 16px;
            margin: 0;
          }
          .popular-image img {
            width: 100%;
            height: 349px;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .read-time {
            font-size: 13px;
            line-height: 15px;
          }
          h3 {
            font-size: 20px;
            line-height: 26px;
            padding: 0 0 16px;
            margin: 0;
          }
        }
      `}</style>    
    </div>
  )
}

export default PopularBlogs
