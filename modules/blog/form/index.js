import React from 'react'
import * as icons from './icon'

function Form({country}) {

  const COUNTRY = {
    sg: 'SGP',
    my: 'MYS',
    ph: 'PHL'
  }
  return (
    <div className="form-container">
      <div className="content-wrapper">
        <div className="content-background"></div>
        <div className="form-wrapper">
          <div>
          <form className="sample-form hide-inputs"
              method="post"
              action-xhr="https://cms.staging.ohmyhome.io/public/amp/newsletter-signup"
              target="_top">
                <div className="subscribe-form">
                  <h3>Subscribe to Us</h3>
                  <p>Be updated with the latest news and blogs for your property transaction needs.</p> 
                  <div className="subscribe-form-wrapper">
                    <div className="ampstart-input">
                      <input type="hidden"
                        className="hidden"
                        name="countryCode"
                        value={`${COUNTRY[country]}`} />
                      <input type="hidden"
                        className="hidden"
                        name="amp_source_origin"
                        value={`https://blog.ohmyhome.com`} />
                      <input type="email"
                        className="block border-none p0 m0"
                        name="email"
                        placeholder="Enter email address"
                        required />
                    </div>
                    <div className="ampstart-submit">
                      <input type="submit"
                        value="Subscribe"
                        className="ampstart-btn caps" />
                    </div>
                  </div>
                </div>
                <div className="submit-success">
                You will receive an email confirmation of your subscription. Thank you!

                  {/* <div className="success-image_wrapper">
                   <icons.success />
                  </div> */}
                </div>
                <div className="submit-error">
                  Error!
                </div>
            </form>
          </div>
        </div>
      </div>
      <style jsx>{`
        .content-wrapper {
          padding: 0 16px;
          min-height: 271px;
        }
        .content-background {
          background-image: url("https://api.omh.app/store/cms/media/philippines/prelaunch/Ohmyhome-Philippines-footer.jpg");
          position: absolute;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          z-index: -1;
          background-size: cover;
          background-position: center top;
        }
        h3 {
          font-size: 24px;
          line-height: 28px;
          color: #FFF;
          margin: 0 0 8px;
          text-align: center;
        }
        p {
          font-size: 16px;
          line-height: 21px;
          text-align: center;
          letter-spacing: -0.25px;
          color: #Fff;
        }
        
        .subscribe-form-wrapper {
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }
        .subscribe-form-wrapper  > div:first-child {
          width: 100%;
        }
        .subscribe-form-wrapper  > div {
          width: auto;
        }
        .subscribe-form input {
          font-size: 16px;
          padding: 20px 18px;
          border-radius: 3px 0 0 3px;
          border: 0px solid transparent;
          color: #858585;
          outline: none;
          display: block;
          width: 100%;
          max-width: 100%;
        }
        .subscribe-form input[type="submit"] {
          border-radius: 0 3px 3px 0;
          background: #EE620F;
          color: #FFF;
          cursor: pointer;
          -webkit-appearance: none;
        }
        .form-wrapper > div {
          display: flex;
          justify-content: center;
          position: absolute;
          transform: translate(-50%, -50%);
          top: 50%;
          left: 50%;
          width: 100%;
        }
  
        @media only screen and (min-width: 768px) {
          .content-wrapper {
            padding: 0 40px;
          }
          .content-wrapper {
            min-height: 271px;
          }
          h3 {
            font-size: 24px;
            line-height: 28px;
            text-align: left;
          }
          p {
            letter-spacing: -0.25px;
            text-align: left;
          }
          .form-wrapper > div {
            left: auto;
            position: absolute;
            transform: translate(-24px, -50%);
            max-width: 321px;
            top: 50%;
            right: 0;
          }
          .subscribe-form input {
            max-width: 313px;
          }
          .subscribe-form-wrapper  > div:first-child {
            width: 59%;
          }

        }
        @media only screen and (min-width: 1025px) {
          
          h3 {
            font-size: 40px;
            line-height: 47px;
            margin: 0 0 10px;
          }
          p {
            letter-spacing: -0.25px;
            max-width: 510px;
          }
          .form-wrapper > div {
            transform: translate(-50%, -50%);
            max-width: 514px;
          }
        }
      `}</style>    
    </div>
  )
}

export default Form
