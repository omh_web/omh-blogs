import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

const learnMore = (
  <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.365 5.92969L8.84119 7.42969L13.484 12L8.84119 16.5703L10.365 18.0703L16.5317 12L10.365 5.92969Z" fill="#E86225"/>
  </svg>
)

function RelatedBlogs({latest, Desktop, Mobile, title, description}) {

  const router = useRouter()

  return (
    <div className="latest-container">
      <div className="latest-wrapper">
        {
          latest && latest.data && latest.data.slice(0, 3).map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))
            return (
              <React.Fragment key={item.id}>
                <div className="latest-card">
                  <div className="latest-card-image">
                    <amp-img
                      width="256"
                      height="225"
                      src={item.featuredImages && item.featuredImages[0].thumbnail}
                      alt={item.title}
                      layout="responsive"
                    />
                  </div>
                  <div className="latest-content">
                    <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                    <div className="learn-more">
                      Learn More
                      {learnMore}
                    </div>
                  </div>
                  <a href={`${router.asPath}/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          }
          
          )
        }
      </div>
      <style jsx>{`
          .latest-container {
 
          }
          .header-wrapper {
            margin: 0 0 32px;
          }
          h2 {
            font-size: 24px;
            line-height: 28px;
            color: #1C1C1C;
            margin: 0 0 8px;
          }
          .latest-wrapper {
            display: flex;
            justify-content: space-between;
          }
          .latest-card {
            box-sizing: border-box;
            border-radius: 5px;
            background: #FFF;
            overflow: hidden;
            position: relative;
            background: #FFF;
            width: 48%;
          }
          .latest-card:last-child {
            display: none;
          }
          .latest-card-image {
            overflow: hidden;
            border-radius: 5px;
          }
          .latest-content {
            padding: 8px 16px 14px;
            margin: 0 0 16px;
          }
          span {
            margin: 0 0 16px;
            display: block;
            color: #5F5F5F;
            font-size: 12px;
            line-height: 14px;
            font-variation-settings: 'wght' 550;
            letter-spacing: 0.1em;
          }
          h3 {
            font-size: 14px;
            line-height: 18px;
            color: #1C1C1C;
            margin: 0 0 8px;
            font-family: Tinos;
            font-weight: 600;
          }
          a {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          a span {
            display: none;
          }
          .learn-more {
            display: flex;
            justify-content: flex-start;
            align-items: center;
          }
          
          p {
            color: #343434;
            font-size: 12px;
            line-height: 16px;
            margin: 0 0 11px;
            display: none;
          }
          .learn-more {
            font-size: 14px;
            line-height: 14px;
            color: #E86225;
          }
          .latest-card:last-child {
            display: none;
          }
        @media only screen and (min-width: 768px) { 
          .latest-container {

          }
          h2 {
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 8px;
          }
          .latest-card {
            border: 1px solid #E0E0E0;
          }
          
          .latest-content {
            padding: 16px 16px 26px;
          }
          span {
            font-size: 13px;
            line-height: 15px;
          }
          h3 {
            font-size: 22px;
            line-height: 26px;
            color: #1C1C1C;

          }
          p {
            font-size: 14px;
            line-height: 21px;
            display: block;
          }
          
          .latest-card {
            width: 32%;
          }
          .latest-card:last-child {
            display: block;
          }
          .latest-card:last-child {
            display: block;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .latest-card {
            border: 0px solid #E0E0E0;
            padding: 16px 16px 0px;
            position: relative;
            margin: 0 0 16px;
          }
          .latest-card:first-child:after {
            content: " ";
            position: absolute;
            opacity: .2;
            left: 0;
            background: #000;
            right: 0;
            bottom: 0;
            display: block;
            width: 100%;
            height: 1px;
          }
          .latest-card:last-child {
            display: none;
          }
          .latest-card:nth-child(2) {
            margin: 0;
            padding: 16px 16px 0;
          }
          h3 {
            font-size: 20px;
            line-height: 26px;
          }
          .latest-wrapper {
            display: block;
          }
          p {
            margin: 0 0 16px;
            font-size: 16px;
          }
          span {
            margin: 0 0 16px;
          }
          .latest-card {
            width: 100%;
          }
        }
      `}</style>    
    </div>
  )
}

export default RelatedBlogs
