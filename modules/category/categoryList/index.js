import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

function CategoryList({category, Desktop, Mobile, title, description, rootUrl}) {

  const router = useRouter()

  return (
    <div className="category-container">
      <div className="category-content_wrapper">
        {
          category.data && category.data.map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))

            return (
              <React.Fragment key={item.id}>
                <div className="category-card">
                  
                  <div className="category-content">
                    <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                    <div className="learn-more">
                      Learn more
                      <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5036 6.42969L9.0036 7.92969L13.5739 12.5L9.0036 17.0703L10.5036 18.5703L16.5739 12.5L10.5036 6.42969Z" fill="#E86225"/>
                      </svg>
                    </div>
                  </div>
                  <div className="category-card-image">
                    <amp-img
                      width="177"
                      height="177"
                      src={item.featuredImages[0]?.large}
                      alt={item.title}
                      layout="fill"
                    />  
                  </div>
                  <a href={`${rootUrl}/${router.query.lang_country}/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          }
          )
        }
      </div>
      <style jsx>{`

          .header-wrapper {
            margin: 0 0 32px;
          }
          h2 {
            font-size: 24px;
            line-height: 28px;
            color: #1C1C1C;
            margin: 0 0 8px;
            
          }
          .category-content_wrapper {
            display: block;
          }
          .category-card {
            position: relative;
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin: 0 0 45px;
          }
          .category-content {
            max-width: 344px;
            margin-right: 44px;
          }
          .category-card-image {
            overflow: hidden;
            border: 1px solid #E0E0E0;
            box-sizing: border-box;
            border-radius: 5px;
            min-width: 104px;
            min-height: 104px;
            position: relative;
          }
         
          span {
            margin: 0 0 8px;
            display: block;
            color: #5F5F5F;
            font-size: 12px;
            line-height: 14px;
            font-variation-settings: "wght" 550;
            letter-spacing: .1em;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            color: #1C1C1C;
            margin: 0 0 8px;
            font-family: Tinos;
            font-weight: 600;
          }
          a {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          a span {
            display: none;
          }
          p {
            margin: 0;
            font-size: 16px;
            line-height: 24px;
            color: #343434;
            displaY: none;
          }
          .learn-more {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            color: #e86225;
            font-size: 14px;
            line-height: 14px;
            margin: 8px 0 0;
          }
          .category-card:nth-child(4n) {
            display: flex;
            flex-direction: column-reverse;
            align-items: flex-start;
          }
          .category-card:nth-child(4n) .category-card-image {
            width: 100%;
            min-height: 320px;
            margin: 0 0 16px;
          }
          .category-card:nth-child(4n) .category-content {
            max-width: 100%;
            margin: 0;
            padding: 0 8px;
          }
          .category-card:nth-child(4n) .category-content h3 {
            font-size: 24px;
            line-height: 21px;
            font-weight: 700;
            margin: 0 0 8px;
          }
          .category-card:nth-child(4n) .category-content p {
            font-size: 14px;
            line-height: 21px;
            margin: 0 0 14px;
            display: block
          }
          
        @media only screen and (min-width: 768px) { 

          h2 {
            font-size: 40px;
            line-height: 47px;
            margin: 0 0 12px;
          }
          .category-content_wrapper {
            flex-wrap: nowrap;
          }
          .category-card {
            margin: 0 0 32px;
          }
  
          .category-content {
          
          }
          span {
            margin: 0 0 16px;
            font-size: 13px;
            line-height: 15px;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 8px;
          }
          p {
            displaY: block;
          }
          .category-card:nth-child(4n) .category-content h3 {
            font-size: 24px;
            line-height: 28px;
            font-weight: 700;
            margin: 0 0 8px;
          }
          .category-card-image {
            min-width: 147px;
            min-height: 147px;
          }
          .learn-more {
            margin: 20px 0 0;
          }
        }
        @media only screen and (min-width: 1025px) { 
          h3 {
            font-size: 20px;
            line-height: 26px;
            margin: 0 0 8px;
          }
          .category-card:nth-child(4n) .category-content h3 {
            font-size: 38px;
            line-height: 44px;
            font-weight: 700;
            margin: 0 0 8px;
          }
          .category-card:nth-child(4n) .category-content p {
            font-size: 18px;
            line-height: 27px;
            margin: 0 0 14px;
          }
          .category-card {
            margin: 0 0 48px;
          }
          .category-card-image {
            min-width: 177px;
            min-height: 177px;
          }
        }
      `}</style>    
    </div>
  )
}

export default CategoryList
