import React from 'react'
import Form from 'modules/post/form'

function Subscribe({post}) {
  
  const close = (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14 2.33337C7.54831 2.33337 2.33331 7.54837 2.33331 14C2.33331 20.4517 7.54831 25.6667 14 25.6667C20.4516 25.6667 25.6666 20.4517 25.6666 14C25.6666 7.54837 20.4516 2.33337 14 2.33337ZM19.8333 18.1884L18.1883 19.8334L14 15.645L9.81165 19.8334L8.16665 18.1884L12.355 14L8.16665 9.81171L9.81165 8.16671L14 12.355L18.1883 8.16671L19.8333 9.81171L15.645 14L19.8333 18.1884Z" fill="#5F5F5F"/>
    </svg>
  )
  
  return (
    <div className={`subscribe-container ${post.marketing && post.marketing.isActive === true ? '' : 'full'}`}>
      <div className="marketing-content">
        <div className="header-wrapper">
          Subscribe Now
        </div>
        <div className="content-wrapper">
          <div className="description-wrapper">
            Get a notification every time we upload a new blog post.
          </div>
        </div>
      </div>
      <div className="marketing-button">
        <div className="tabs">
          <div className="tab">
            <input type="radio" id="sidebar-wrapper" name="rd" />
            <label className="tab-label">
              <label htmlFor="sidebar-wrapper" className="tab-open">
                Subscribe Now
              </label>
            </label>
            
            <div className="tab-content">
              
              <div className="tab tab-close_wrapper">
                <input type="radio" id="rd3" name="rd" />
                <label htmlFor="rd3" className="tab-close">
                  {close}
                </label>
              </div>
              <div className="tab-content_wrapper">
                <Form post={post}/>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
      
      <style jsx>{`
        .subscribe-container {
          background: #FEF4EE;
          border-radius: 10px;
          padding: 24px 16px;
          display: flex; 
          justify-content: space-between;
          align-items: center;
        }
        .subscribe-container.full {
          display: flex; 
          justify-content: space-between;
          align-items: center;
          padding: 24px 32px;
        }
        .marketing-content {
          width: 50%;
        }
        .header-wrapper {
          font-size: 14px;
          line-height: 21px;
          color: #1C1C1C;
          margin: 0 0 4px;
          font-variation-settings: "wght" 550;
        }
        .description-wrapper {
          font-size: 12px;
          color: #343434;
          line-height: 18px;
        }

        // SideBar

        input {
          position: absolute;
          opacity: 0;
          z-index: -1;
        }
        .tab-open {
          font-size: 14px;
          line-height: 14px;
          border: 1px solid #E86225;
          color: #E86225;
          padding: 10px 25px;
          border-radius: 5px;
          text-decoration: none;
          display: inline-block;
          font-variation-settings: "wght" 550;
          transition: all 100ms ease 0s;
          cursor: pointer;
        }
        .tab-open:hover {
          border: 1px solid rgba(232, 98, 37, 0.6);
        }
        input:checked + .tab-open {
          position: absolute;
          left:0;
          right: 0;
          top: 0;
          z-index: -1;
        } 
        .tab-close_wrapper {
          border-bottom:1px solid #E0E0E0;
        }
        .tab-close {
          display: -webkit-box;
          display: flex;
          justify-content: flex-end;
          padding: 13px 19px 17px;
          font-size: 0.75em;
          cursor: pointer;
          color: #1C1C1C;
          background: #FFF;
        }
        .tab-content {
          right: 0;
          bottom: 0;
          background: white;
          position: absolute;
          height: auto
          width: 100%;
          max-width: 600px;
          top: auto;
          left: -100%;
          background: #FFF;
          border-right: 1px solid #E0E0E0;
          z-index: 2;
          transition: all .5s ease-in;
          box-shadow: 1px 0px 3px rgba(0, 0, 0, 0.2);
          margin: 0 auto;
          opacity: 0;
          z-index: -1;
        }
        input:checked ~ .tab-content {
          opacity: 1;
          left: 0;
          z-index: 1;
        }
        .tab-content_wrapper {
          padding: 20px;
          height: 100%;
        }
        @media only screen and (min-width: 768px) {
          .subscribe-container {
            display: block;
          }
          .subscribe-container {
            display: block;
          }
          .subscribe-container.full .marketing-content {
            width: 55%;
          }
          .marketing-content {
            width: 100%;
          }
          .header-wrapper {
            font-size: 18px;
            line-height: 27px;
            margin: 0 0 4px;
          }
          .subscribe-container.full .description-wrapper {
            font-size: 16px;
            line-height: 21px;
            margin: 0;
          }
          .description-wrapper {
            font-size: 16px;
            line-height: 21px;
            margin: 0 0 14px;
          }
          .tab-open {
            padding: 16px 25px;
          }
        }
      `}</style>    
    </div>
  )
}

export default Subscribe
