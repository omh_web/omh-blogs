import React from 'react'

function Content({post}) {

  return (
    <div className="content-wrapper">

      <div className="content-container" dangerouslySetInnerHTML={{ __html: post.contentHtml }} />      

      <style jsx>{`
        .content-container {
          padding: 0 16px 0;
        }
        .content-container p {
          line-height: 32px;
          font-size: 18px;
          color: #1c1c1c;
        }
        @media only screen and (min-width: 768px) {
          .content-container {
            padding: 0;
          }
        }
        @media only screen and (min-width: 1025px) {
          
        }
      `}</style>    
    </div>
  )
}

export default Content
