import React from 'react'

function PreviousPost({post, Desktop, Mobile}) {

  const chevron = (
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
     <path fillRule="evenodd" clipRule="evenodd" d="M16.8536 7.6449C17.0488 7.84017 17.0488 8.15675 16.8536 8.35201L11.7844 13.4211L16.8536 18.4902C17.0488 18.6855 17.0488 19.0021 16.8536 19.1973L16.3551 19.6958C16.1598 19.8911 15.8433 19.8911 15.648 19.6958L9.84455 13.8924C9.71677 13.7646 9.67261 13.5848 9.71208 13.4211C9.67261 13.2574 9.71677 13.0777 9.84455 12.9499L15.648 7.14645C15.8433 6.95118 16.1598 6.95118 16.3551 7.14645L16.8536 7.6449Z" fill="black"/>
    </svg>
  )
    

  return (
    <div className="previous-container">
      <div className="header-wrapper">
        <div className="mobile">
          <div className="icon-wrapper">
            {chevron}
          </div>
        </div>
        Previous Post
      </div>
      <div className="content-wrapper">
        <div className="desktop">
          <div className="icon-wrapper">
            {chevron}
          </div>
        </div>
        <div className="title-wrapper">
          {post.previousPost && post.previousPost.title}
        </div>
      </div>
      <a href={post.previousPost && post.previousPost.slug} ></a>
      <style jsx>{`
        .header-wrapper {
          margin: 0 0 8px;
          font-size: 12px;
          line-height: 18px;
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }
        .content-wrapper {
          display: flex;
          ustify-content: flex-start;
          align-items: center;
        }
        .icon-wrapper {
          margin-right: 8px;
        }
        .title-wrapper {
          font-size: 16px;
          line-height: 21px;
          font-family: 'Tinos';
          color:#5F5F5F;
          font-weight: bold;
        }
        a {
          position: absolute;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          display: block;
          opacity: 0;
        }
        .desktop {
          display: none;
        }
        .mobile {
          display: block;
        }
        @media only screen and (min-width: 500px) {
          .header-wrapper {
            font-size: 16px;
          }
        }
        @media only screen and (min-width: 768px) {
          .desktop {
            display: block;
          }
          .mobile {
            display: none;
          }
          .header-wrapper {
            padding-left: 35px;
          }
        }
      `}</style>    
    </div>
  )
}

export default PreviousPost
