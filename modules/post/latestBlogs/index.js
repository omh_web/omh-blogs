import React from 'react'
import moment from 'moment'
import { useRouter, withRouter } from "next/router"

function LatestBlogs({latest, Desktop, Mobile, title, description, rootUrl}) {

  const router = useRouter()

  return (
    <div className="latest-container">
      <div className="header-wrapper">
        <h2>{title}</h2>
        <span>{description}</span>
      </div>
      <div className="latest-content_wrapper">
        {
          latest && latest.data && latest.data.length && latest.data.map((item) => {
            const duration = moment.duration(item.readingTime, 'seconds')
            const hours = duration._data.hours
            const minutes = duration._data.minutes
            const seconds =   duration._data.seconds
            const secondsRound = Math.round(moment.duration(seconds) * 10) / 1000
            const finalMinutes = Math.round(moment.duration(minutes + secondsRound))

            return (
              <React.Fragment key={item.id}>
                <div className="latest-card">
                  <div className="latest-card-image">
                    <amp-img
                      width="364"
                      height="208"
                      src={item.featuredImages[0].small}
                      alt={item.title}
                      layout="responsive"
                    />  
                  </div>
                  <div className="latest-content">
                    <span>{hours === 0 ? null : hours} {finalMinutes} MIN READ</span>
                    <h3>{item.title}</h3>
                    <p>{item.excerpt}</p>
                  </div>
                  <a href={`${rootUrl}/${router.query.lang_country}/${item.slug}`}><span>{item.title}</span></a>
                </div>
              </React.Fragment>
            )
          }
          )
        }
      </div>
      <style jsx>{`
          .latest-container {
            padding: 0 20px;
            margin: 0 0 37px;
          }
          .header-wrapper {
            margin: 0 0 32px;
          }
          h2 {
            font-size: 24px;
            line-height: 28px;
            color: #1C1C1C;
            margin: 0 0 8px;
            
          }
          .latest-content_wrapper {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
          }
          .latest-card {
            border: 1px solid #E0E0E0;
            box-sizing: border-box;
            border-radius: 5px;
            background: #FFF;
            max-width: 392px;
            overflow: hidden;
            position: relative;
            background: #FFF;
            width: 49%;
            margin: 0 0 38px;
          }
          .latest-card:last-child {
          
          }
          .latest-card-image {
            overflow: hidden;
          }
          .latest-card-image img {
            object-fit: cover;
            width: 100%;
            height: 100%;
          }
          .latest-content {
            padding: 16px 16px 14px;
          }
          span {
            margin: 0 0 8px;
            display: block;
            color: #1c1c1c;
            font-size: 14px;
          }
          h3 {
            font-size: 16px;
            line-height: 21px;
            color: #1C1C1C;
            margin: 0 0 4px;
            font-family: Tinos;
          }
          a {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
          }
          a span {
            display: none;
          }
          p {
            margin: 0;
          }
          .latest-card:nth-child(+n+5) {
            display: none;
          }
        @media only screen and (min-width: 768px) { 
          .latest-container {
            padding: 0 28px;
            margin: 0 0 80px;
          }
          h2 {
            font-size: 40px;
            line-height: 47px;
            margin: 0 0 12px;
          }
          .header-wrapper span {
            margin: 0 0 16px;
            font-size: 18px;
            line-height: 24px;
          } 
          .latest-content_wrapper {
            flex-wrap: nowrap;
          }
          .latest-card {
            min-width: 220px;
            width: 32%;
            margin: 0;
          }
          .latest-card:nth-child(+n+4) {
            display: none;
          }
          .latest-content {
            padding: 16px 16px 26px;
          }
          span {
            margin: 0 0 16px;
          }
          h3 {
            font-size: 18px;
            line-height: 23px;
            margin: 0 0 8px;
          }
          p {
            font-size: 18px;
            line-height: 24px;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .latest-container {
            margin: 0 0 121px;
            padding: 0 16px;
          }
          h3 {
            font-size: 22px;
            line-height: 29px;
          }
        }
        @media only screen and (min-width: 1441px) {
          .latest-card {
            max-width: 100%;
          }
        }
      `}</style>    
    </div>
  )
}

export default LatestBlogs
