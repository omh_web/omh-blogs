import React from 'react'
import * as icons from './icon'


import { useRouter, withRouter } from "next/router"


function Form() {
  const router = useRouter()
  const country  = router.query.lang_country.toString().split(/-/)[1]
  const COUNTRY = {
    sg: 'SGP',
    my: 'MYS',
    ph: 'PHL'
  }

  return (
    <div className="form-container">
      <div className="content-wrapper">
        <div className="content-background"></div>
        <div className="form-wrapper">
          <div>
          <form className="sample-form hide-inputs"
              method="post"
              action-xhr="https://cms.staging.ohmyhome.io/public/amp/newsletter-signup"
              target="_top">
                <div className="subscribe-form">
                  <h3>Subscribe to Us</h3>
                  <p>Be updated with the latest news and blogs for your property transaction needs.</p> 
                  <div className="subscribe-form-wrapper">
                    <div className="ampstart-input">
                      <input type="hidden"
                        className="hidden"
                        name="countryCode"
                        value={`${COUNTRY[country]}`} />
                      <input type="hidden"
                        className="hidden"
                        name="amp_source_origin"
                        value={`https://blog.ohmyhome.com`} />
                      <input type="email"
                        className="block border-none p0 m0"
                        name="email"
                        placeholder="Enter email address"
                        required />
                    </div>
                    <div className="ampstart-submit">
                      <input type="submit"
                        value="Subscribe"
                        className="ampstart-btn caps" />
                    </div>
                  </div>
                </div>
                <div className="submit-success">
                You will receive an email confirmation of your subscription. Thank you!

                  {/* <div className="success-image_wrapper">
                   <icons.success />
                  </div> */}
                </div>
                <div className="submit-error">
                  Error!
                </div>
            </form>
          </div>
        </div>
      </div>
      

    <div id="subscribe-result"></div>
      <style jsx>{`
          h3 {
            font-family: Tinos;
            font-size: 22px;
            margin: 0 0 15px;
          }
          p {
            line-height: 18px;
            font-size: 16px;
            margin: 0 0 40px;
          }
          .subscribe-form-wrapper {
            display: block;
          }
          .subscribe-form-wrapper input[type="email"] {
            border: 1px solid #e55710;
            color: #000;
            padding: 16px 25px;
            border-radius: 5px;
            box-sizing: border-box;
            outline: none;
            margin: 0 0 18px;
            display: block;
            width: 100%;
            font-size: 14px;
          }
          .subscribe-form-wrapper input[type=submit] {
            padding: 16px 25px;
            font-size: 14px;
            border-radius: 5px;
            border: 1px solid #e86225;
            background: #e86225;
            color: #FFF;
            font-variation-settings: "wght" 550;
            cursor: pointer;
          }

      `}</style>    
    </div>
  )
}

export default Form
