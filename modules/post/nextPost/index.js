import React from 'react'

function nextPost({post, Desktop, Mobile}) {

  const chevronOrange = (
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M9.84457 19.1973C9.6493 19.0021 9.6493 18.6855 9.84457 18.4902L14.9137 13.4211L9.84457 8.352C9.6493 8.15674 9.6493 7.84015 9.84457 7.64489L10.343 7.14643C10.5383 6.95117 10.8549 6.95117 11.0501 7.14643L16.8536 12.9499C16.9814 13.0777 17.0255 13.2574 16.986 13.4211C17.0255 13.5848 16.9814 13.7646 16.8536 13.8923L11.0501 19.6958C10.8549 19.891 10.5383 19.891 10.343 19.6958L9.84457 19.1973Z" fill="#D8571D"/>
    </svg>
  )

  const chevron = (
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M9.84444 19.1973C9.64918 19.0021 9.64918 18.6855 9.84444 18.4902L14.9136 13.4211L9.84444 8.352C9.64918 8.15674 9.64918 7.84015 9.84444 7.64489L10.3429 7.14643C10.5382 6.95117 10.8547 6.95117 11.05 7.14643L16.8534 12.9499C16.9812 13.0777 17.0254 13.2574 16.9859 13.4211C17.0254 13.5848 16.9812 13.7646 16.8534 13.8923L11.05 19.6958C10.8547 19.891 10.5382 19.891 10.3429 19.6958L9.84444 19.1973Z" fill="#1C1C1C"/>
    </svg>
  )
    

  return (
    <div className="next-container">

      
          <React.Fragment>
            <div className="header-wrapper">
              Continue Reading
              <div className="mobile">
                <div className="icon-wrapper">
                  {chevron}
                </div>
              </div>
            </div>
            <div className="content-wrapper">
              <div className="title-wrapper">
                {post.nextPost && post.nextPost.title}
              </div>
              <div className="desktop">
                <div className="icon-wrapper">
                  {chevronOrange}
                </div>
              </div>
            </div>
            <a href={post.nextPost && post.nextPost.slug} ></a>
          </React.Fragment>
 
      <style jsx>{`
        .header-wrapper {
          margin: 0 0 8px;
          font-size: 12px;
          line-height: 18px;
          display: flex;
          justify-content: flex-end;
          align-items: center;
        }
        .content-wrapper {
          display: flex;
          justify-content: flex-end;
          align-items: center;
        }
        .title-wrapper {
          font-size: 16px;
          line-height: 21px;
          font-family: 'Tinos';
          color: #D8571D;
          font-weight: bold;
        }
        .icon-wrapper {
          margin-left: 8px;
        }
        a {
          position: absolute;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          display: block;
          opacity: 0;
        }
        .desktop {
          display: none;
        }
        .mobile {
          display: block;
        }
        @media only screen and (min-width: 500px) {
          .header-wrapper {
            font-size: 16px;
          }
        }
        @media only screen and (min-width: 768px) {
          .desktop {
            display: block;
          }
          .mobile {
            display: none;
          }
          .header-wrapper {
            padding-right: 35px;
          }
        }
      `}</style>    
    </div>
  )
}

export default nextPost
