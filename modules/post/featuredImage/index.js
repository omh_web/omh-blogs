import React from 'react'

function FeaturedImage({post}) {
  return (
    <div className="featured-image_wrapper">
      { post.featuredImages && post.featuredImages[0] && post.featuredImages[0] === undefined ? '' :
        <amp-img
          width="700"
          height="400"
          src={post.featuredImages && post.featuredImages[0] && post.featuredImages[0].medium || ''}
          alt={post.title}
          layout="nodisplay"
        />
      }
      <style jsx>{`
        .featured-image_wrapper {
          min-height: 400px;
          width: 100%;
          position: relative;
          background-image: url(${post.featuredImages && post.featuredImages[0] && post.featuredImages[0].xlarge})
          background-size: cover;
          background-position: center;
          
        }
        .featured-image_wrapper amp-img {
          border-radius: 10px;
        }
        @media only screen and (min-width: 768px) { 
          .featured-image_wrapper {
            width: 97.5vw;
            margin-left: -50vw;
            left: 52%;
            border-radius: 10px;
            margin-top: 68px;
          }
        }
        @media only screen and (min-width: 1025px) { 
          .featured-image_wrapper {
            width: 100%;
            margin-left: 0;
            left: 0;
            margin: 98px 0 0;
          }
        }
      `}</style>    
    </div>

    
  )
}

export default FeaturedImage
