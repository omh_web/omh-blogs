import React from 'react'

function Marketing({post}) {

  return (
    <div className={`marketing-container ${post && post.marketing && post.marketing.isActive === true ? '' : 'full'}`}>
      <div className="marketing-content">
        <div className="header-wrapper">
          {post.marketing.title}
        </div>
        <div className="content-wrapper">
          <div className="description-wrapper">
            {post.marketing.category}
          </div>
        </div>
      </div>
      <div className="marketing-button">
        <a href={`${post && post.marketing && post.marketing.primaryButtonUrl}`}>{ post && post.marketing &&  post.marketing.primaryButtonLabel}</a>
      </div>
      
      <style jsx>{`
        .marketing-container {
          background: #FEF4EE;
          border-radius: 10px;
          padding: 24px 16px;
          display: flex; 
          justify-content: space-between;
          align-items: center;
        }
        .marketing-content {
          width: 50%;
        }
        .header-wrapper {
          font-size: 14px;
          line-height: 21px;
          color: #1C1C1C;
          margin: 0 0 4px;
          font-variation-settings: "wght" 550;
        }
        .description-wrapper {
          font-size: 12px;
          color: #343434;
          line-height: 18px;
        }
        a {
          font-size: 14px;
          line-height: 14px;
          border: 1px solid #E86225;
          color: #FFF;
          padding: 10px 25px;
          border-radius: 5px;
          text-decoration: none;
          display: inline-block;
          background-color: #E86225;
          font-variation-settings: "wght" 550;
          transition: all 100ms ease 0s;
          text-align: center;
        }
        a:hover {
          background-color: rgba(232, 98, 37, 0.9);
          border: 1px solid rgba(232, 98, 37, 0.9);
          color: #FFF;
        }
        @media only screen and (min-width: 768px) {
          .marketing-container {
            display: block; 
          }
          .marketing-container.full {
            display: flex; 
            justify-content: space-between;
            align-items: center;
          }
         
          .marketing-content {
            width: 100%;
          }
          .marketing-container.full .marketing-content {
            width: 50%;
          }
          .header-wrapper {
            font-size: 18px;
            line-height: 27px;
            margin: 0 0 4px;
          }
          .description-wrapper {
            font-size: 14px;
            line-height: 21px;
            margin: 0 0 14px;
          }
          a {
            padding: 16px 25px;
          }
        }
      `}</style>    
    </div>
  )
}

export default Marketing
